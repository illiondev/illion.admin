import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

import { emailFormat, required, composeValidators } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class FormUserAdd extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  form = null;

  handleSubmit = ({ email }) => {
    this.props.onSave(email);
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  render() {
    const { open } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="xs" fullWidth>
        <DialogTitle>Добавить пользователя</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                <Field
                  label="Email пользователя*"
                  name="email"
                  component={FormFieldText}
                  validate={composeValidators(required, emailFormat)}
                  fullWidth
                />
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Добавить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormUserAdd;
