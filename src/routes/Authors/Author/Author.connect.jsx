import { connect } from 'react-redux';

import {
  authorRead, authorUpdate, authorUserCreate, authorUserDelete
} from 'lib/authors/actions';
import { getAuthorsById } from 'lib/authors/selectors';

import Author from './Author';

function mapStateToProps(state, ownProps) {
  return {
    entry: getAuthorsById(state)[ownProps.match.params.id],
    isFetching: state.authors.isFetchingEntry,
  };
}

export default connect(mapStateToProps, {
  authorRead,
  authorUpdate,
  authorUserCreate,
  authorUserDelete,
})(Author);
