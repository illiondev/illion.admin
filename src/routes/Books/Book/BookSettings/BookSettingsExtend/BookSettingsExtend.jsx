import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import BookSettingsQuotes from './BookSettingsQuotes';
import BookSettingsAdvantages from './BookSettingsAdvantages';
import BookSettingsFeatures from './BookSettingsFeatures';
import BookSettingsImages from './BookSettingsImages';
import BookSettingsOther from './BookSettingsOther';
import BookSettingsReviews from './BookSettingsReviews';

class BookSettingsExtend extends Component {
  static propTypes = {
    isReadOnlyRoute: PropTypes.bool.isRequired,
    entry: PropTypes.shape({
      id: PropTypes.number,
      quotes: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
      features: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
      advantages: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
      photos: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
      reviews: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
    }).isRequired,
  }

  render() {
    const {
      entry: {
        quotes, features, advantages, photos, reviews, id
      },
      entry,
      isReadOnlyRoute,
    } = this.props;

    return (
      <Fragment>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Цитаты(quotes)</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <BookSettingsQuotes entries={quotes.data} bookId={id} isReadOnly={isReadOnlyRoute} />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Список преимуществ(advantages)</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <BookSettingsAdvantages entries={advantages.data} bookId={id} isReadOnly={isReadOnlyRoute} />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Особенности(features)</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <BookSettingsFeatures entries={features.data} bookId={id} isReadOnly={isReadOnlyRoute} />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Отзывы(reviews)</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <BookSettingsReviews entries={reviews.data} bookId={id} isReadOnly={isReadOnlyRoute} />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Фотографии книги</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <BookSettingsImages entries={photos.data} bookId={id} isReadOnly={isReadOnlyRoute} />
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <BookSettingsOther entry={entry} isReadOnly={isReadOnlyRoute} />
      </Fragment>
    );
  }
}

export default BookSettingsExtend;
