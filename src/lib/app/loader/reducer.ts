import types from './actionTypes';
import { AppLoaderAllActions } from './actions';

export interface AppLoaderState {
  readonly counter: number;
}

const initialState: AppLoaderState = {
  counter: 0,
};

const loader = (state = initialState, action: AppLoaderAllActions): AppLoaderState => {
  switch (action.type) {
    case types.APP_LOADER_SHOW:
      return {
        ...state,
        counter: state.counter + 1,
      };

    case types.APP_LOADER_HIDE:
      return {
        ...state,
        counter: state.counter - 1,
      };

    default:
      return state;
  }
};

export default loader;
