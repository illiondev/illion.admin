import { connect } from 'react-redux';

import {
  bookPublish, bookRead, bookUnpublish, bookClear, bookInit
} from 'lib/books/actions';
import { bookChapterInit } from 'lib/chapters/actions';
import { getBooksById } from 'lib/books/selectors';

import Book from './Book';

function mapStateToProps(state, ownProps) {
  return {
    entry: getBooksById(state)[ownProps.match.params.id],
    isFetching: state.books.isFetchingEntry,
    chapters: Number(ownProps.match.params.id) === state.chapters.bookId ? state.chapters.entries : undefined,
    chapterDraftId: state.chapterDraft.id,
  };
}

export default connect(mapStateToProps, {
  bookInit,
  bookClear,
  bookRead,
  bookPublish,
  bookUnpublish,
  bookChapterInit,
})(Book);
