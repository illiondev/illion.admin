import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';

import { LOG_TYPES_LIST } from 'enums/LogsEnums';
import { LogClass, LogsFilterQuery, LogType } from 'types/logs';

interface Props {
  current: LogsFilterQuery;
  logClasses: LogClass[];
  onSubmit(query?: LogsFilterQuery): void;
}

interface State {
  current: LogsFilterQuery;
  filter: LogsFilterQuery;
}

class LogsFilter extends Component<Props, State> {
  public static getDerivedStateFromProps(nextProps: Props, prevState: State): State|null {
    if (nextProps.current !== prevState.current) {
      return {
        filter: {
          ...LogsFilter.initialFilterValues(),
          ...nextProps.current,
        },
        current: nextProps.current,
      };
    }

    return null;
  }

  private static initialFilterValues(): LogsFilterQuery {
    return {
      subject_id: undefined,
      email: '',
      subject_label: '',
      description: '',
    };
  }

  public state: State = {
    current: {},
    filter: LogsFilter.initialFilterValues(),
  };

  public componentDidMount(): void {
    window.addEventListener('keyup', this.handleKeyUp);
  }

  public componentWillUnmount(): void {
    window.removeEventListener('keyup', this.handleKeyUp);
  }

  private handleKeyUp = (e: KeyboardEvent): void => {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement|HTMLSelectElement>): void => {
    const { name, value } = e.target;

    this.setState((prevState: State): Pick<State, 'filter'> => ({
      filter: {
        ...prevState.filter,
        [name]: value,
      }
    }));
  };

  private handleSubmit = (): void => {
    let params: LogsFilterQuery|undefined = Object.keys(this.state.filter)
      .reduce((mem: LogsFilterQuery, key: string): LogsFilterQuery => {
        // @ts-ignore
        let val = this.state.filter[key];

        if (val) {
          if (['subject_id'].includes(key)) {
            val = Number(val);
          }

          return {
            ...mem,
            [key]: val,
          };
        }

        return mem;
      }, {});

    if (Object.keys(params).length === 0) {
      params = undefined;
    }

    this.props.onSubmit(params);
  };

  private handleReset = (): void => {
    const isActiveFilter = Object.keys(this.props.current).length > 0;

    this.setState({ filter: LogsFilter.initialFilterValues() });

    if (isActiveFilter) {
      this.props.onSubmit();
    }
  };

  public render(): React.ReactNode {
    const {
      subject_id, email, subject_label, description,
    } = this.state.filter;
    const hasValues = [subject_id, email, subject_label, description]
      .some((v: any): boolean => Boolean(v));
    const isActive = Object.keys(this.props.current).length > 0;

    return (
      <Grid container spacing={24} className="b-paper">
        <Grid item xs={12} sm={6} md="auto">
          <TextField
            type="email"
            name="email"
            label="Пользователь"
            onChange={this.handleChange}
            value={email}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md="auto">
          <TextField
            type="number"
            name="subject_id"
            label="ID"
            onChange={this.handleChange}
            value={subject_id || ''}
            className="b-orders-filter__number"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md="auto">
          <FormControl fullWidth>
            <InputLabel>Тип&nbsp;данных</InputLabel>
            <NativeSelect
              name="subject_label"
              onChange={this.handleChange}
              value={subject_label}
            >
              <option value="" />
              {this.props.logClasses.map((o: LogClass): React.ReactNode => (
                <option value={o.subject_label} key={o.subject_label}>{o.subject_name}</option>
              ))}
            </NativeSelect>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md="auto">
          <FormControl fullWidth>
            <InputLabel>Действие</InputLabel>
            <NativeSelect
              name="description"
              onChange={this.handleChange}
              value={description}
            >
              <option value="" />
              {LOG_TYPES_LIST.map((o: LogType): React.ReactNode => (
                <option value={o.id} key={o.id}>{o.title}</option>
              ))}
            </NativeSelect>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md="auto">
          {(isActive || hasValues) && (
            <Button
              variant="outlined"
              color="secondary"
              onClick={this.handleReset}
              className="m-margin_right-normal"
            >
              Сбросить
            </Button>
          )}

          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
            disabled={!hasValues}
          >
            Искать
          </Button>
        </Grid>
      </Grid>
    );
  }
}

export default LogsFilter;
