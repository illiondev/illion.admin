import React, { Component, ComponentType, Fragment } from 'react';
import {
  Route, Redirect, RouteProps, RouteComponentProps
} from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { PERMISSION_PATHS } from 'enums/PermissionsEnums';

import router from 'services/RouterService';

import { RoutePrivateStoreProps } from './RoutePrivate.connect';

interface Props extends RouteProps {
  component: ComponentType<any>;
}

class RoutePrivate extends Component<RoutePrivateStoreProps & Props> {
  public render(): React.ReactNode {
    const {
      component: RouteComponent, isLogged, permissions, permissionsEdit, profileIsLoaded, ...rest
    } = this.props;
    const canRead = permissions.includes(PERMISSION_PATHS[rest.path as string]);
    const canEdit = permissionsEdit.includes(PERMISSION_PATHS[rest.path as string]);

    return (
      <Route
        {...rest}
        render={
          (props: RouteComponentProps): React.ReactNode => (isLogged ? (
            <Fragment>
              {canRead ? (
                <RouteComponent {...props} isReadOnlyRoute={!canEdit} />
              ) : (
                <div className="b-page__content">
                  {profileIsLoaded && (
                    <Paper className="b-form-content">
                      <Typography variant="h5" align="center" color="error">
                        У Вас нет прав для просмотра этой страницы
                      </Typography>
                    </Paper>
                  )}
                </div>
              )}
            </Fragment>
          ) : (
            <Redirect
              to={{
                pathname: router.url.LOGIN(),
                state: { from: props.location }
              }}
            />
          ))
        }
      />
    );
  }
}

export default RoutePrivate;
