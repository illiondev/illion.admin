import { connect } from 'react-redux';

import { RootState } from 'lib';

import { getRoutesAccess, getRoutesEditAccess, getProfileIsLoaded } from 'lib/profile/selectors';

import RoutePrivate from './RoutePrivate';

export interface RoutePrivateStoreProps {
  isLogged: boolean;
  permissions: string[];
  permissionsEdit: string[];
  profileIsLoaded: boolean;
}

function mapStateToProps(state: RootState): RoutePrivateStoreProps {
  return {
    isLogged: Boolean(state.auth.tokens),
    permissions: getRoutesAccess(state),
    permissionsEdit: getRoutesEditAccess(state),
    profileIsLoaded: getProfileIsLoaded(state),
  };
}

export default connect<RoutePrivateStoreProps, {}, {}, RootState>(mapStateToProps)(RoutePrivate);
