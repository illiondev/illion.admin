import { AutocompleteEntry } from 'types';
import { Action } from 'types/redux';

import types from './actionTypes';

type AppAutocompleteFetch = Action<types.APP_AUTOCOMPLETE_FETCH, boolean>;
type AppAutocompleteSetType = Action<types.APP_AUTOCOMPLETE_SET_TYPE, string>;
type AppAutocompleteResult = Action<types.APP_AUTOCOMPLETE_RESULT, AutocompleteEntry[]>;
type AppAutocompleteDrop = Action<types.APP_AUTOCOMPLETE_DROP, undefined>;

export type AppAutocompleteAllActions = AppAutocompleteFetch | AppAutocompleteSetType
| AppAutocompleteResult | AppAutocompleteDrop;

export function appAutocompleteFetch(payload: boolean): AppAutocompleteFetch {
  return {
    type: types.APP_AUTOCOMPLETE_FETCH,
    payload,
  };
}

export function appAutocompleteSetType(payload: string): AppAutocompleteSetType {
  return {
    type: types.APP_AUTOCOMPLETE_SET_TYPE,
    payload,
  };
}

export function appAutocompleteResult(payload: AutocompleteEntry[]): AppAutocompleteResult {
  return {
    type: types.APP_AUTOCOMPLETE_RESULT,
    payload,
  };
}

export function appAutocompleteDrop(): AppAutocompleteDrop {
  return {
    type: types.APP_AUTOCOMPLETE_DROP,
    payload: undefined,
  };
}
