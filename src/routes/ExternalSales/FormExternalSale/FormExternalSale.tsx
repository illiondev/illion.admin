import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';
import { ValidationErrors } from 'final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';

import { getCurrentDate } from 'utils';
import {
  composeValidators, required, requiredId, maxNumber
} from 'utils/validators';

import { AutocompleteEntry, ExternalSale } from 'types';

import FormFieldAutoComplete from 'components/FormFieldAutoComplete';
import FormFieldText from 'components/FormFieldText';
import FormFieldDate from 'components/FormFieldDate';
import FormFieldSwitch from 'components/FormFieldSwitch';
import FormFieldRadio from 'components/FormFieldRadio';

interface Props {
  entry: ExternalSale;
  open: boolean;
  onClose(): void;
  onSave(data: any): void;
  onBundleAutocomplete(query: string): void;
}

type ExternalSaleFormData = ExternalSale & {
  bundle: AutocompleteEntry;
  commission_type: 'flat' | 'percent';
  commission_value: number;
}

class FormExternalSale extends Component<Props> {
  public static defaultProps = {
    entry: {},
  };

  private form = React.createRef<HTMLFormElement>();

  private handleSubmit = (formData: object): void => {
    const {
      bundle, price, comment, sold_at, id, quantity, commission_type, commission_value, is_paid, author_fee
    } = formData as ExternalSaleFormData;
    const data = {
      bundle_id: bundle.id,
      quantity: Number(quantity),
      author_fee: Number(author_fee),
      price: Number(price),
      [commission_type === 'flat' ? 'commission_flat' : 'commission']: Number(commission_value),
      is_paid: Boolean(is_paid),
      sold_at,
      comment,
    };

    if (id) {
      data.id = id;
    }

    this.props.onSave(data);
  };

  private validate = (values: object): ValidationErrors => {
    const errors = {} as ValidationErrors;

    // @ts-ignore
    if (values.commission_type === 'percent' && values.commission_value > 100) {
      errors.commission_value = 'Процент не может быть более 100';
    }

    return errors;
  };

  private handleSave = (): void => {
    if (this.form.current) {
      this.form.current.dispatchEvent(new Event('submit', { cancelable: true }));
    }
  };

  public render(): React.ReactNode {
    const { open, entry } = this.props;
    const commission_value = entry.commission_flat || entry.commission;
    // eslint-disable-next-line no-nested-ternary
    const commission_type = typeof commission_value !== 'undefined' ? entry.commission_flat ? 'flat' : 'percent' : '';

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="md">
        <DialogTitle>{entry.id ? `Редактирование продажи #${entry.id}` : 'Новая продажа'}</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={{
              sold_at: getCurrentDate(),
              ...entry,
              bundle: entry.id ? {
                id: entry.bundle_id,
                title: `${entry.title}`,
              } : {},
              commission_value,
              commission_type,
            }}
            validate={this.validate}
            render={({ handleSubmit }): React.ReactNode => (
              <form onSubmit={handleSubmit} ref={this.form}>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Field
                      autocompleteHandler={this.props.onBundleAutocomplete}
                      label="Название книги*"
                      name="bundle"
                      component={FormFieldAutoComplete as any}
                      validate={requiredId}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={2}>
                    <Field
                      name="quantity"
                      label="Кол-во*"
                      type="number"
                      component={FormFieldText}
                      validate={required}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={3}>
                    <Field
                      name="price"
                      label="Цена (за шт.)*"
                      type="number"
                      component={FormFieldText}
                      validate={required}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={3}>
                    <Field
                      name="sold_at"
                      label="Дата продажи*"
                      fullWidth
                      type="date"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      component={FormFieldDate}
                      validate={required}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={3}>
                    <Field
                      name="is_paid"
                      type="checkbox"
                      label="Оплачено"
                      component={FormFieldSwitch as any}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={3}>
                    <Field
                      name="author_fee"
                      label="Процент автора, %*"
                      type="number"
                      component={FormFieldText}
                      validate={composeValidators(required, maxNumber(100))}
                      fullWidth
                    />
                  </Grid>
                  <Grid item sm={12} md={1} />
                  <Grid item sm={12} md={7}>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={5}>
                        <Field
                          name="commission_value"
                          label="Комиссия c продажи*"
                          type="number"
                          component={FormFieldText}
                          validate={required}
                          fullWidth
                        />
                      </Grid>
                      <Grid item xs={12} sm={7}>
                        <FormGroup row>
                          <Field
                            name="commission_type"
                            type="radio"
                            label="В процентаx"
                            value="percent"
                            component={FormFieldRadio as any}
                            validate={required}
                          />
                          <Field
                            name="commission_type"
                            type="radio"
                            label="В рублях, за ед."
                            value="flat"
                            component={FormFieldRadio as any}
                            validate={required}
                          />
                        </FormGroup>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={12}>
                    <Field
                      name="comment"
                      label="Комментарий"
                      component={FormFieldText}
                      fullWidth
                      multiline
                      rows={3}
                    />
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Сохранить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormExternalSale;
