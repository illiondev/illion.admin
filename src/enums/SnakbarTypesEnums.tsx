import { ComponentType } from 'react';

import { SvgIconProps } from '@material-ui/core/SvgIcon';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';

export enum SNACKBAR_TYPES {
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
  WARNING = 'warning',
}

export const SNACKBAR_TYPE_ICONS: Record<SNACKBAR_TYPES, ComponentType<SvgIconProps>> = {
  [SNACKBAR_TYPES.ERROR]: ErrorIcon,
  [SNACKBAR_TYPES.INFO]: InfoIcon,
  [SNACKBAR_TYPES.SUCCESS]: CheckCircleIcon,
  [SNACKBAR_TYPES.WARNING]: WarningIcon,
};
