import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Toolbar from '@material-ui/core/Toolbar';
import Button, { ButtonProps } from '@material-ui/core/Button';
import Typography, { TypographyProps } from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import { WithWidthProps } from '@material-ui/core/withWidth';

import router from 'services/RouterService';

// @ts-ignore
import LogoIcon from 'assets/logo.svg';

import Menu from './Menu';
import MenuMobile from './MenuMobile';
import { HeaderDispatchProps, HeaderStoreProps } from './Header.connect';

import './Header.scss';

type Props = HeaderStoreProps & HeaderDispatchProps & WithWidthProps;

class Header extends Component<Props> {
  public static defaultProps = {
    username: '',
  };

  public render(): React.ReactNode {
    const {
      isLoading, isLogged, menuItems, username, width
    } = this.props;

    return (
      <AppBar position="sticky">
        <Toolbar>
          <Link to="/">
            <LogoIcon className="b-header__logo" />
          </Link>

          {!isLogged && (
            <Typography
              variant="h6"
              className="b-header__logo-desc"
              color="inherit"
              noWrap
              component={(props: TypographyProps): any => <Link to="/" {...props} />}
            >
              Издательство Иллион
            </Typography>
          )}

          {isLogged && (
            ['lg', 'xl'].includes(width || '') ? <Menu items={menuItems} /> : <MenuMobile items={menuItems} />
          )}

          <div className="b-header__button-wrapper">
            {isLogged ? (
              <Fragment>
                {username && (
                  <Typography variant="button" className="b-header__username">
                    <AccountCircleIcon />
                    <span className="b-header__username-value">{username}</span>
                  </Typography>
                )}

                <Button color="inherit" onClick={this.props.authLogout} className="m-margin_left-normal">
                  Выйти
                </Button>
              </Fragment>
            ) : (
              <Fragment>
                <Button
                  component={(props: ButtonProps): any => <Link to={router.url.LOGIN()} {...props} />}
                  color="inherit"
                >
                  Войти
                </Button>
                <Button
                  component={(props: ButtonProps): any => <Link to={router.url.SIGNUP()} {...props} />}
                  color="inherit"
                >
                  Регистрация
                </Button>
              </Fragment>
            )}
          </div>
        </Toolbar>

        {isLoading && <LinearProgress color="secondary" className="b-header__progress" />}
      </AppBar>
    );
  }
}

export default Header;
