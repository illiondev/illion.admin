import { LOG_TYPES } from 'enums/LogsEnums';

export interface Log {
  readonly id: number;
  readonly email: string;
  readonly description: LOG_TYPES;
  readonly subject_id: number;
  readonly subject_name: string;
  readonly subject_label: string;
  readonly created_at: string;
  readonly properties: {
    readonly attributes: string;
    readonly old: string;
  };
}

export interface LogType {
  id: LOG_TYPES;
  title: string;
}

export interface LogClass {
  readonly subject_label: string;
  readonly subject_name: string;
}

export interface LogsFilterQuery {
  readonly subject_label?: string;
  readonly description?: string;
  readonly email?: string;
  readonly subject_id?: number;
  readonly page?: number;
}
