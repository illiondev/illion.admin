import { connect } from 'react-redux';

import { bundleRead } from 'lib/books/actions';
import { getOrders, ordersSpreadsheet, ordersSpreadsheetDrop } from 'lib/orders/actions';

import Orders from './Orders';

function mapStateToProps(state) {
  return {
    entries: state.orders.entries,
    pager: state.orders.pager,
    query: state.orders.query,
    exportUrl: state.orders.exportUrl,
    bundles: state.books.bundles,
  };
}

export default connect(mapStateToProps, {
  bundleRead,
  getOrders,
  ordersSpreadsheet,
  ordersSpreadsheetDrop,
})(Orders);
