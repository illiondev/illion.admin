import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import CommentIcon from '@material-ui/icons/Comment';

import UserPermissions from './UserPermissions';
import FormUserAdd from './FormUserAdd';
import FormComment from './FormComment';

class Users extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      comment: PropTypes.string,
      read: PropTypes.arrayOf(PropTypes.string).isRequired,
      edit: PropTypes.arrayOf(PropTypes.string).isRequired,
    })).isRequired,
    pager: PropTypes.shape({}).isRequired,
    userId: PropTypes.number,
    getUsers: PropTypes.func.isRequired,
    userUpdate: PropTypes.func.isRequired,
    userModeratorCreate: PropTypes.func.isRequired,
    userModeratorDelete: PropTypes.func.isRequired,
    userCommentCreate: PropTypes.func.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    userId: undefined,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.entries !== prevState.entriesProp) {
      return {
        modalPermissionsIsOpen: false,
        modalUserAddIsOpen: false,
        modalCommentIsOpen: false,
        entriesProp: nextProps.entries,
        entry: undefined,
      };
    }

    return null;
  }

  state = {
    modalPermissionsIsOpen: false,
    modalUserAddIsOpen: false,
    modalCommentIsOpen: false,
    entriesProp: undefined,
    entry: undefined,
  };

  componentDidMount() {
    this.props.getUsers();
  }

  handleChangePage = (e, page) => {
    this.props.getUsers({ page: page + 1 });
  };

  handleEditEntry = entry => () => this.setState({ entry, modalPermissionsIsOpen: true });

  handleCommentEntry = entry => () => this.setState({ entry, modalCommentIsOpen: true });

  handleOpenUserAddForm = () => this.setState({ modalUserAddIsOpen: true });

  handleModalClose = () => {
    this.setState({
      modalPermissionsIsOpen: false,
      modalUserAddIsOpen: false,
      modalCommentIsOpen: false,
      entry: undefined
    });
  };

  handleAddUser = (email) => {
    this.props.userModeratorCreate(email);
  };

  handleUpdateUser = (user) => {
    this.props.userUpdate(user, user.id === this.props.userId);
  };

  handleDeleteUser = entry => () => {
    if (window.confirm('Удалить?')) this.props.userModeratorDelete(entry.id);
  };


  handleAddComment = (comment) => {
    this.props.userCommentCreate(this.state.entry.id, comment);
  };


  render() {
    const { entries, pager, isReadOnlyRoute } = this.props;
    const {
      modalPermissionsIsOpen, modalUserAddIsOpen, entry, modalCommentIsOpen
    } = this.state;

    return (
      <div className="b-page__content">
        {!isReadOnlyRoute && (
          <Tooltip title="Добавить пользователя" placement="top">
            <Fab
              color="primary"
              className="b-page__actions"
              onClick={this.handleOpenUserAddForm}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}

        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Пользователи</Typography>

          <div className="b-table-responsive">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Email</TableCell>
                  <TableCell>Чтение</TableCell>
                  <TableCell>Запись</TableCell>
                  <TableCell>Комментарий</TableCell>
                  <TableCell align="right" />
                </TableRow>
              </TableHead>
              <TableBody>
                {entries.map(e => (
                  <TableRow key={e.id} hover>
                    <TableCell component="th" scope="row">
                      {e.email}
                    </TableCell>
                    <TableCell>{e.read.sort().join(', ')}</TableCell>
                    <TableCell>{e.edit.sort().join(', ')}</TableCell>
                    <TableCell>
                      {Boolean(e.comment) && (
                        <Fragment>
                          <Typography variant="caption">{e.comment.data.user.data.name}</Typography>
                          <Typography>{e.comment.data.text}</Typography>
                        </Fragment>
                      )}
                    </TableCell>
                    <TableCell align="right" className="m-nowrap">
                      {!isReadOnlyRoute && (
                        <Fragment>
                          <Tooltip title="Комментарий" placement="top">
                            <IconButton onClick={this.handleCommentEntry(e)}>
                              <CommentIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Редактировать" placement="top">
                            <IconButton onClick={this.handleEditEntry(e)}>
                              <EditIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Удалить" placement="top">
                            <IconButton onClick={this.handleDeleteUser(e)} color="secondary">
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip>
                        </Fragment>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>

          {pager.total_pages > 1 && (
            <TablePagination
              component="div"
              count={pager.total}
              rowsPerPage={pager.per_page}
              rowsPerPageOptions={[pager.per_page]}
              page={pager.current_page - 1}
              onChangePage={this.handleChangePage}
            />
          )}
        </Paper>

        <UserPermissions
          onClose={this.handleModalClose}
          onSave={this.handleUpdateUser}
          open={modalPermissionsIsOpen}
          entry={entry}
        />

        <FormUserAdd
          onClose={this.handleModalClose}
          onSave={this.handleAddUser}
          open={modalUserAddIsOpen}
        />

        <FormComment
          currentValue={entry && entry.comment ? entry.comment.data.text : ''}
          onClose={this.handleModalClose}
          onSave={this.handleAddComment}
          open={modalCommentIsOpen}
        />
      </div>
    );
  }
}

export default Users;
