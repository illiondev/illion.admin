require('dotenv').config();

const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CleanAfterEmitWebpackPlugin = require('clean-after-emit-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const DotenvPlugin = require('dotenv-webpack');
const SentryPlugin = require('@sentry/webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';
const isProduction = NODE_ENV === 'production';
const cssName = isProduction ? 'bundle-[hash].css' : 'bundle.css';
const jsName = isProduction ? 'bundle-[hash].js' : 'bundle.js';
const buildFolder = path.resolve(__dirname, 'dist/static');
// извлекаем последний хеш коммита в git
const commitHash = require('child_process').execSync('git rev-parse HEAD').toString().replace(/\s/g, '');

const config = {
  mode: NODE_ENV,
  entry: [
    '@babel/polyfill/dist/polyfill.js',
    path.resolve(__dirname, 'src/index.tsx'),
  ],
  devtool: isProduction ? 'source-map' : false,
  resolve: {
    extensions: ['.js', '.jsx', '.tsx', '.ts'],
    modules: [
      'node_modules',
      path.resolve(__dirname, 'src'),
    ],
    alias: {
      routes: path.resolve(__dirname, 'src/routes'),
      lib: path.resolve(__dirname, 'src/lib'),
      components: path.resolve(__dirname, 'src/components'),
      services: path.resolve(__dirname, 'src/services'),
      utils: path.resolve(__dirname, 'src/utils'),
      enums: path.resolve(__dirname, 'src/enums'),
      config: path.resolve(__dirname, 'src/config'),
      assets: path.resolve(__dirname, 'src/assets'),
      types: path.resolve(__dirname, 'src/types'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(jsx?|tsx?)$/,
        include: /node_modules/,
        use: ['react-hot-loader/webpack'],
      },
      {
        test: /\.(jsx?|tsx?)$/,
        loader: 'babel-loader',
        exclude: [path.resolve(__dirname, 'node_modules')],
        options: {
          cacheDirectory: true,
        },
      },
      {
        test: /\.(jsx?|tsx?)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/,
        options: {
          // для dev окружения разрешаем собрать бандл с ошибками линтера
          // на остальных окружений сборка закончится с фейлом
          emitWarning: process.env.APP_ENV === 'dev',
        },
      },
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
          },
          'postcss-loader',
          'resolve-url-loader',
          'sass-loader?sourceMap',
        ]
      },
      {
        test: /\.svg$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        loader: 'svg-react-loader',
        query: {
          uniqueIdPrefix: true,
        },
      },
    ],
  },
  plugins: [
    new DotenvPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
      'process.env.RELEASE': JSON.stringify(commitHash),
    }),
    new MiniCssExtractPlugin({
      filename: cssName,
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.ejs'),
      favicon: path.resolve(__dirname, 'src/assets/favicon.png'),
      filename: isProduction ? '../index.html' : 'index.html',
    }),
  ],
  output: {
    filename: jsName,
    path: buildFolder,
    publicPath: isProduction ? '/static/' : '/',
  },
};


if (isProduction) {
  config.plugins.push(
    new CleanWebpackPlugin(),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  );

  if (process.env.SENTRY_DSN) {
    config.plugins.push(
      new SentryPlugin({
        release: commitHash,
        include: './dist'
      })
    );
  }

  // удаляем файлы sourcemap из сборки
  if (process.env.APP_ENV === 'production') {
    config.plugins.push(
      new CleanAfterEmitWebpackPlugin({
        paths: [path.join(buildFolder, '*.js.map'), path.join(buildFolder, '*.css.map')],
      }),
    );
  }

  if (process.env.APP_ENV !== 'production') {
    config.plugins.push(
      new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        openAnalyzer: false,
      }),
    );
  }

  config.optimization = {
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        cache: true,
        parallel: true,
        terserOptions: {
          output: {
            comments: false,
          },
        },
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  };
} else {
  config.devServer = {
    contentBase: buildFolder,
    historyApiFallback: true,
    hot: true,
    inline: true,
    port: 8080,
  };
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
}


module.exports = config;
