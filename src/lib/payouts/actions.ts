import api from 'services/ApiService';

import {
  Payout, ListRes, PagerReq, PayoutReq
} from 'types';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

type PayoutsReadSuccess = Action<types.PAYOUTS_READ, ListRes<Payout>>;
type PayoutCreateSuccess = Action<types.PAYOUT_CREATE, Payout>;
type PayoutUpdateSuccess = Action<types.PAYOUT_UPDATE, Payout>;

export type StatisticsAllActions = PayoutsReadSuccess | PayoutCreateSuccess | PayoutUpdateSuccess | AuthLogout;

function payoutsReadSuccess(payload: ListRes<Payout>): PayoutsReadSuccess {
  return {
    type: types.PAYOUTS_READ,
    payload,
  };
}

export function payoutsRead(query: PagerReq = {} as PagerReq): ActionThunk {
  return (dispatch): Promise<void> => api.payoutsRead(query).then((res): void => {
    dispatch(payoutsReadSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
    }));
  });
}

function payoutCreateSuccess(payload: Payout): PayoutCreateSuccess {
  return {
    type: types.PAYOUT_CREATE,
    payload,
  };
}

export function payoutCreate(data: PayoutReq): ActionThunk {
  return (dispatch): Promise<void> => api.payoutCreate(data).then((res): void => {
    dispatch(payoutCreateSuccess(res.data.data));
  });
}

function payoutUpdateSuccess(payload: Payout): PayoutUpdateSuccess {
  return {
    type: types.PAYOUT_UPDATE,
    payload,
  };
}

export function payoutUpdate(data: PayoutReq): ActionThunk {
  return (dispatch): Promise<void> => api.payoutUpdate(data).then((res): void => {
    dispatch(payoutUpdateSuccess(res.data.data));
  });
}
