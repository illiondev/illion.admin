import router from 'services/RouterService';

const { path } = router;

enum PERMISSIONS {
  BOOKS = 'books',
  AUTHORS = 'authors',
  USERS = 'users',
  CONTENT = 'content',
  STATISTICS = 'statistics',
  ORDERS = 'orders',
  PAYOUTS = 'payouts',
  AUTHORS_STATISTICS = 'authors statistics',
  AUTHORS_PAYOUTS = 'authors payouts',
  LOGS = 'logs',
  VACANCIES = 'vacancies',
  EXTERNAL_SALES = 'external sales',
  SOON = 'soon',
}

interface PermissionForm {
  type: PERMISSIONS;
  readOnly?: boolean;
}

export const PERMISSION_LIST: PermissionForm[] = [
  { type: PERMISSIONS.BOOKS },
  { type: PERMISSIONS.AUTHORS },
  { type: PERMISSIONS.USERS },
  { type: PERMISSIONS.CONTENT },
  { type: PERMISSIONS.STATISTICS, readOnly: true },
  { type: PERMISSIONS.ORDERS },
  { type: PERMISSIONS.PAYOUTS },
  { type: PERMISSIONS.AUTHORS_STATISTICS, readOnly: true },
  { type: PERMISSIONS.AUTHORS_PAYOUTS, readOnly: true },
  { type: PERMISSIONS.LOGS, readOnly: true },
  { type: PERMISSIONS.VACANCIES },
  { type: PERMISSIONS.EXTERNAL_SALES },
  { type: PERMISSIONS.SOON },
].sort((a, b): 1|-1 => (a.type > b.type ? 1 : -1));

interface PermissionPaths { [key: string]: PERMISSIONS }

export const PERMISSION_PATHS: PermissionPaths = {
  [path.CONTENT]: PERMISSIONS.CONTENT,

  [path.BOOKS]: PERMISSIONS.BOOKS,
  [path.BOOK_NEW]: PERMISSIONS.BOOKS,
  [path.BOOK]: PERMISSIONS.BOOKS,

  [path.BOOK_EDIT]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_BASIC]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_EXTEND]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_PRODUCTS]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_PRODUCT]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_BUNDLES]: PERMISSIONS.BOOKS,
  [path.BOOK_EDIT_BUNDLE]: PERMISSIONS.BOOKS,
  [path.BOOK_PRODUCT_NEW]: PERMISSIONS.BOOKS,

  [path.BOOK_CHAPTER]: PERMISSIONS.BOOKS,
  [path.BOOK_CHAPTER_PREVIEW]: PERMISSIONS.BOOKS,

  [path.AUTHORS]: PERMISSIONS.AUTHORS,
  [path.AUTHOR_NEW]: PERMISSIONS.AUTHORS,
  [path.AUTHOR]: PERMISSIONS.AUTHORS,

  [path.ORDERS]: PERMISSIONS.ORDERS,
  [path.ORDER]: PERMISSIONS.ORDERS,

  [path.VACANCIES]: PERMISSIONS.VACANCIES,
  [path.VACANCY]: PERMISSIONS.VACANCIES,

  [path.USERS]: PERMISSIONS.USERS,
  [path.STATISTICS]: PERMISSIONS.STATISTICS,
  [path.STATISTICS_PROFIT]: PERMISSIONS.STATISTICS,
  [path.PAYOUTS]: PERMISSIONS.PAYOUTS,
  [path.LOGS]: PERMISSIONS.LOGS,
  [path.EXTERNAL_SALES]: PERMISSIONS.EXTERNAL_SALES,
  [path.SOON]: PERMISSIONS.SOON,
};
