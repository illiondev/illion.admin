import { LogType } from 'types/logs';

export enum LOG_TYPES {
  CREATED = 'created',
  UPDATED = 'updated',
  DELETED = 'deleted',
  PAID = 'paid',
}

export const LOG_TYPES_LIST: LogType[] = [
  { id: LOG_TYPES.CREATED, title: 'Создание' },
  { id: LOG_TYPES.UPDATED, title: 'Редактирование' },
  { id: LOG_TYPES.DELETED, title: 'Удаление' },
  { id: LOG_TYPES.PAID, title: 'Оплата' },
];

type LogTypesTitleById = { [key in LOG_TYPES]: string }

export const LOG_TYPES_TITLE_BY_ID: LogTypesTitleById = LOG_TYPES_LIST.reduce(
  (byId: LogTypesTitleById, s: LogType): LogTypesTitleById => ({
    ...byId, [s.id]: s.title,
  }),
  {} as LogTypesTitleById
);
