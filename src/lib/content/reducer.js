import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  announcement: undefined,
};

const content = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.ANNOUNCEMENT_GET:
      return {
        ...state,
        announcement: payload,
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default content;
