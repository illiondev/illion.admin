export const CHAPTER_DRAFT_CREATE = 'CHAPTER_DRAFT_CREATE';
export const CHAPTER_DRAFT_UPDATE = 'CHAPTER_DRAFT_UPDATE';
export const CHAPTER_DRAFT_DELETE = 'CHAPTER_DRAFT_DELETE';
