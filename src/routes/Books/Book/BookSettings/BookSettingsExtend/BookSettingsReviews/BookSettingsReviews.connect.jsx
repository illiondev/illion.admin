import { connect } from 'react-redux';

import { bookReviewsCreate, bookReviewsUpdate, bookReviewsDelete } from 'lib/books/actions';

import BookSettingsReviews from './BookSettingsReviews';

export default connect(null, {
  bookReviewsCreate,
  bookReviewsUpdate,
  bookReviewsDelete,
})(BookSettingsReviews);
