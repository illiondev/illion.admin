import React, { Component } from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';

import { PUBLIC_URL } from 'config';
import { copyToClipboard } from 'utils';

import OrderBillItem from './OrderBillItem';
import BillForm from './BillForm';

class OrderBills extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    orderId: PropTypes.number.isRequired,
    orderBillCreate: PropTypes.func.isRequired,
    snackbarShow: PropTypes.func.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
  };

  handleSubmitForm = (data) => {
    this.props.orderBillCreate({
      ...data,
      id: this.props.orderId,
    });
  };

  handleCopyUrl = (entry) => {
    copyToClipboard(`${PUBLIC_URL}/payment/bill/${entry.hash}`);
    this.props.snackbarShow({ type: 'info', message: 'Ссылка скопирована в буфер' });
  };

  render() {
    const { isReadOnly, orderId } = this.props;

    return (
      <div className="m-width-full">
        <List>
          {this.props.entries.map(e => (
            <OrderBillItem key={e.id} entry={e} onCopyUrl={this.handleCopyUrl} />
          ))}
        </List>

        {!isReadOnly && (
          <BillForm onSave={this.handleSubmitForm} orderId={orderId} />
        )}
      </div>
    );
  }
}

export default OrderBills;
