import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class BundlePrices extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Grid container spacing={24}>
            <Grid item sm={12} md={6}>
              <Typography variant="subtitle1" gutterBottom>Цена веб:</Typography>
              <Grid container spacing={24}>
                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_web_rub"
                    type="number"
                    label="RUB*"
                    fullWidth
                    component={FormFieldText}
                    validate={required}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_web_eur"
                    type="number"
                    label="EUR"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_web_uah"
                    type="number"
                    label="UAH"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnly}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item sm={12} md={6}>
              <Typography variant="subtitle1" gutterBottom>Цена мобильная:</Typography>
              <Grid container spacing={24}>
                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_mob_rub"
                    type="number"
                    label="RUB"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_mob_eur"
                    type="number"
                    label="EUR"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <Field
                    name="price_mob_uah"
                    type="number"
                    label="UAH"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnly}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default BundlePrices;
