import api from 'services/ApiService';

import { ListRes } from 'types';
import { Log, LogClass, LogsFilterQuery } from 'types/logs';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

interface LogsPayload extends ListRes<Log> {
  query: LogsFilterQuery;
}

type LogsSuccess = Action<types.LOGS_LIST, LogsPayload>;
type LogClassesSuccess = Action<types.LOG_CLASSES, LogClass[]>;

export type LogsAllActions = LogsSuccess | LogClassesSuccess | AuthLogout;

function getLogsSuccess(payload: LogsPayload): LogsSuccess {
  return {
    type: types.LOGS_LIST,
    payload,
  };
}

export function getLogs(query: LogsFilterQuery = {} as LogsFilterQuery): ActionThunk {
  return (dispatch): Promise<void> => api.logs(query).then((res): void => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { page, ...restParams } = query;

    dispatch(getLogsSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
      query: restParams,
    }));
  });
}

function getLogClassesSuccess(payload: LogClass[]): LogClassesSuccess {
  return {
    type: types.LOG_CLASSES,
    payload,
  };
}

export function getLogClasses(): ActionThunk {
  return (dispatch): Promise<void> => api.logClasses().then((res): void => {
    dispatch(getLogClassesSuccess(
      res.data.data.sort((a: LogClass, b: LogClass): number => (a.subject_name > b.subject_name ? 1 : -1))
    ));
  });
}
