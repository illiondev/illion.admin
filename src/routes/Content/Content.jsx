import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { required } from 'utils/validators';
import { HEADER_MESSAGE_VISIBILITY } from 'enums/ContentEnums';

import FormFieldSelect from 'components/FormFieldSelect';
import FormFieldSwitch from 'components/FormFieldSwitch';
import FormFieldEditor from 'components/FormFieldEditor';

import './Content.scss';

class Content extends Component {
  static propTypes = {
    announcement: PropTypes.shape({
      enabled: PropTypes.bool,
      content: PropTypes.string,
      page_type: PropTypes.number,
    }),
    announcementRead: PropTypes.func.isRequired,
    announcementCreate: PropTypes.func.isRequired,
    announcementUpdate: PropTypes.func.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    announcement: undefined,
  };

  componentDidMount() {
    this.props.announcementRead();
  }

  handleSubmit = (formData) => {
    // удаляем символы переносов (фикс ошибки парсинга JSON на клиенте)
    const data = { ...formData, content: formData.content.replace(/\n/g, '') };

    if (!this.props.announcement || (this.props.announcement && !this.props.announcement.page_type)) {
      this.props.announcementCreate(data);
    } else {
      this.props.announcementUpdate(data);
    }
  };

  render() {
    const { announcement = {}, isReadOnlyRoute } = this.props;
    const initialValues = announcement.page_type ? announcement : {
      ...announcement,
      page_type: HEADER_MESSAGE_VISIBILITY[0].id
    };


    return (
      <div className="b-page__content">
        <Paper className="b-form-content">
          <Typography variant="h5">Объявление в шапке</Typography>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={initialValues}
            initialValuesEqual={(o1, o2) => o1 === o2}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={24}>
                  <Grid item xs={12} md={6}>
                    <Field
                      name="enabled"
                      type="checkbox"
                      label="Сообщение включено"
                      component={FormFieldSwitch}
                      disabled={isReadOnlyRoute}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <Field
                      name="page_type"
                      label="Видимость*"
                      options={HEADER_MESSAGE_VISIBILITY}
                      fullWidth
                      component={FormFieldSelect}
                      validate={required}
                      skipEmpty
                      disabled={isReadOnlyRoute}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="caption" gutterBottom>Аннотация*</Typography>
                    <Field
                      name="content"
                      component={FormFieldEditor}
                      options={{
                        plugins: 'code lists textcolor colorpicker',
                        height: 300,
                        content_style: 'body {background-color: #0b0b0b; color: #fff; font-weight: 500;}'
                      }}
                      validate={required}
                      disabled={isReadOnlyRoute}
                    />
                  </Grid>
                </Grid>

                {!isReadOnlyRoute && (
                  <div className="b-form-content__actions">
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      className="m-margin_left-auto"
                    >
                      Сохранить
                    </Button>
                  </div>
                )}
              </form>
            )}
          />
        </Paper>
      </div>
    );
  }
}

export default Content;
