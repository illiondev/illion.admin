import { connect } from 'react-redux';

import { announcementCreate, announcementRead, announcementUpdate } from 'lib/content/actions';

import Content from './Content';

function mapStateToProps(state) {
  return {
    announcement: state.content.announcement,
  };
}

export default connect(mapStateToProps, {
  announcementCreate,
  announcementRead,
  announcementUpdate,
})(Content);
