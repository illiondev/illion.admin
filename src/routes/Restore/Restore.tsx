import React, { Component } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import router from 'services/RouterService';

import RestoreForm from './RestoreForm';
import { RestoreDispatchProps } from './Restore.connect';

interface RouteParams {
  status?: 'next';
}

type Props = RestoreDispatchProps & RouteComponentProps<RouteParams>;

class Restore extends Component<Props> {
  public render(): React.ReactNode {
    const isNextStep = this.props.match.params.status === 'next';

    return (
      <div className="b-auth">
        <Paper className="b-auth__paper">
          <Avatar className="b-auth__avatar">
            <LockIcon />
          </Avatar>
          <Typography variant="h5">Восстановление доступа</Typography>
          {isNextStep ? (
            <Typography variant="body1" align="center" className="m-margin_top-normal">
              Инструкция по восстановлению доступа отправлена на указанный email
            </Typography>
          ) : (
            <div className="b-auth__form">
              <RestoreForm onSave={this.props.authForgot} />

              <Typography variant="body1" align="center" className="m-margin_top-normal">
                <Link to={router.url.LOGIN()}>Войти</Link>
              </Typography>
            </div>
          )}
        </Paper>
      </div>
    );
  }
}

export default Restore;
