import { connect } from 'react-redux';

import { RootState } from 'lib';
import { authRegister } from 'lib/auth/actions';

import SignUp from './SignUp';

export interface SignUpDispatchProps {
  authRegister: typeof authRegister;
}

export default connect<{}, SignUpDispatchProps, {}, RootState>(null, { authRegister })(
  // @ts-ignore
  SignUp
);
