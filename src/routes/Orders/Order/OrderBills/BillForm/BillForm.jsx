import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

import { required, maxLength } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class FormPayout extends Component {
  static propTypes = {
    orderId: PropTypes.number.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  state = {
    isOpen: false,
  };

  form = null;

  handleSubmit = (data) => {
    this.props.onSave(data);
    this.handleCloseModal();
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  handleOpenModal = () => this.setState({ isOpen: true });

  handleCloseModal = () => this.setState({ isOpen: false });

  render() {
    const { orderId } = this.props;
    const { isOpen } = this.state;

    return (
      <Fragment>
        <Button
          fullWidth
          variant="outlined"
          onClick={this.handleOpenModal}
        >
          Выставить счёт
        </Button>

        <Dialog onClose={this.handleCloseModal} open={isOpen} fullWidth maxWidth="xs">
          <DialogTitle>{`Новый счет для заказа #${orderId}`}</DialogTitle>
          <DialogContent>
            <Form
              onSubmit={this.handleSubmit}
              initialValues={{
                description: `Доплата для заказа №${orderId}.`,
                price: '',
              }}
              subscription={{ submitting: true, pristine: true }}
              render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <Field
                        name="price"
                        label="Сумма*"
                        type="number"
                        component={FormFieldText}
                        validate={required}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="description"
                        label="Комментарий"
                        component={FormFieldText}
                        validate={maxLength(255)}
                        fullWidth
                        multiline
                        rows={3}
                      />
                    </Grid>
                  </Grid>
                </form>
              )}
            />
          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleCloseModal} color="default">
              Отмена
            </Button>
            <Button onClick={this.handleSave} color="primary" variant="contained">
              Создать
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

export default FormPayout;
