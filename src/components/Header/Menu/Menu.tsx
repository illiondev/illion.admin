import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import Button, { ButtonProps } from '@material-ui/core/Button';
import MenuMui from '@material-ui/core/Menu';
import MenuItemMui, { MenuItemProps } from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import { MenuItem, MouseEventCallback } from 'types';

interface Props {
  items: MenuItem[];
}

interface State {
  anchorEl: EventTarget|null;
  submenu: string;
}

class Menu extends Component<Props, State> {
  public state: State = {
    anchorEl: null,
    submenu: '',
  };

  private handleOpenSubmenu = (submenu: string): MouseEventCallback => (e: React.MouseEvent<HTMLElement>): void => {
    this.setState({ anchorEl: e.currentTarget, submenu });
  };

  private handleCloseSubmenu = (): void => this.setState({ anchorEl: null });

  public render(): React.ReactNode {
    const { anchorEl, submenu } = this.state;

    return (
      <Fragment>
        {this.props.items.map((m): React.ReactNode => (m.submenu ? (
          <Fragment key={m.title}>
            <Button
              onClick={this.handleOpenSubmenu(m.title)}
              color="inherit"
            >
              {m.title}
              <ArrowDropDownIcon />
            </Button>

            <MenuMui
              anchorEl={anchorEl as HTMLElement}
              open={Boolean(anchorEl) && submenu === m.title}
              onClose={this.handleCloseSubmenu}
            >
              {m.submenu.map((sm): React.ReactNode => (
                <MenuItemMui
                  component={(props: MenuItemProps): any => <Link to={sm.url} {...props} />}
                  key={sm.url}
                  onClick={this.handleCloseSubmenu}
                >
                  {sm.title}
                </MenuItemMui>
              ))}
            </MenuMui>
          </Fragment>
        ) : (
          <Button
            component={(props: ButtonProps): any => <Link to={m.url} {...props} />}
            color="inherit"
            key={m.url}
          >
            {m.title}
          </Button>
        )))}
      </Fragment>
    );
  }
}

export default Menu;
