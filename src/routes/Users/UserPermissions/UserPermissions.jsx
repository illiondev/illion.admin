import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import { PERMISSION_LIST } from 'enums/PermissionsEnums';

import './UserPermissions.scss';

class UserPermissions extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number,
      permissions: PropTypes.arrayOf(PropTypes.string),
    }),
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  static defaultProps = {
    entry: {
      permissions: [],
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.entry.id !== prevState.id) {
      return {
        permissions: nextProps.entry.permissions,
        id: nextProps.entry.id,
      };
    }

    return null;
  }

  state = {
    permissions: [],
    id: ''
  };

  handleSave = () => {
    this.props.onSave(this.state);
  };

  handleChange = (e) => {
    const { checked, name } = e.target;
    const values = [name];

    // если разрешаем запись, сразу добавляем разрешение на чтение
    if (/^edit/.test(name) && checked) {
      values.push(name.replace('edit', 'read'));
    }

    // если запрещаем чтение, сразу убираем разрешение на запись
    if (/^read/.test(name) && !checked) {
      values.push(name.replace('read', 'edit'));
    }

    this.setState(prevState => ({
      permissions: checked
        ? [...prevState.permissions, ...values].filter((p, i, c) => c.indexOf(p) === i)
        : prevState.permissions.filter(p => !values.includes(p))
    }));
  };

  render() {
    const { open, entry } = this.props;
    const { permissions } = this.state;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="md">
        <DialogTitle>{`Редактирование прав доступа для ${entry.email}`}</DialogTitle>
        <DialogContent>
          <ul className="b-user-permissions">
            {PERMISSION_LIST.map(p => (
              <li className="b-user-permissions__item" key={p.type}>
                <strong className="b-user-permissions__title">{p.type}</strong>
                <FormControlLabel
                  control={(
                    <Switch
                      checked={permissions.includes(`read ${p.type}`)}
                      name={`read ${p.type}`}
                      color="primary"
                      onChange={this.handleChange}
                    />
                  )}
                  label="Чтение"
                />
                <br />
                {!p.readOnly && (
                  <FormControlLabel
                    control={(
                      <Switch
                        checked={permissions.includes(`edit ${p.type}`)}
                        name={`edit ${p.type}`}
                        onChange={this.handleChange}
                      />
                    )}
                    label="Запись"
                  />
                )}
              </li>
            ))}
          </ul>
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Сохранить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default UserPermissions;
