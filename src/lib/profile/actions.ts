import api from 'services/ApiService';

import { UserProfile } from 'types';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

type ProfileMeFetch = Action<types.PROFILE_ME_FETCH, boolean>;
type ProfileMeSuccess = Action<types.PROFILE_ME, UserProfile>;

export type ProfileAllActions = ProfileMeFetch | ProfileMeSuccess | AuthLogout;

function profileMeFetch(payload: boolean): ProfileMeFetch {
  return {
    type: types.PROFILE_ME_FETCH,
    payload,
  };
}


export function profileMeSuccess(payload: UserProfile): ProfileMeSuccess {
  return {
    type: types.PROFILE_ME,
    payload,
  };
}

export function profileMe(): ActionThunk {
  return (dispatch): Promise<void> => {
    dispatch(profileMeFetch(true));

    return api.profileMe()
      .then((res): void => {
        dispatch(profileMeSuccess(res.data.data));
      })
      .catch((e): void => {
        dispatch(profileMeFetch(false));

        throw e;
      });
  };
}
