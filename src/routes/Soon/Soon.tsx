import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import { getAuthorName } from 'utils';
import { SoonBook, SoonBookReq, ProtectedRoute } from 'types';

import FormSoon from './FormSoon';
import { SoonStoreProps, SoonDispatchProps } from './Soon.connect';

import './Soon.scss';

type Props = SoonStoreProps & SoonDispatchProps & ProtectedRoute;

interface State {
  modalIsOpen: boolean;
  entry?: SoonBook;
  entries: SoonBook[];
}

class Soon extends Component<Props, State> {
  public static getDerivedStateFromProps(nextProps: Props, prevState: State): State|null {
    const { entries } = nextProps;

    if (entries !== prevState.entries) {
      return {
        modalIsOpen: false,
        entry: undefined,
        entries,
      };
    }

    return null;
  }

  public state: State = {
    modalIsOpen: false,
    entry: undefined,
    entries: [],
  };

  public componentDidMount(): void {
    this.props.soonRead();
  }

  private handleModalClose = (): void => this.setState({ modalIsOpen: false, entry: undefined });

  private handleEditEntry = (entry: SoonBook): void => this.setState({ entry, modalIsOpen: true });

  private handleAuthorAutocomplete = (query: string): void => {
    this.props.authorAutocomplete(query);
  };

  private handleCreate = (): void => {
    this.setState({ entry: {} as SoonBook, modalIsOpen: true });
  };

  private handleUpdateEntry = (data: Partial<SoonBookReq>): void => {
    if (data.id) {
      this.props.soonUpdate(data as SoonBookReq);
    } else {
      this.props.soonCreate(data);
    }
  };

  private handleToggleEnabled = (entry: SoonBook): void => {
    this.props.soonUpdate({
      id: entry.id,
      enabled: !entry.enabled,
    } as SoonBookReq);
  };

  public render(): React.ReactNode {
    const { entries, isReadOnlyRoute } = this.props;
    const { modalIsOpen, entry } = this.state;

    return (
      <div className="b-page__content">
        <Paper className="b-page__paper">
          <Typography variant="headline" className="b-page__paper-title">Скоро в продаже</Typography>

          <Grid container spacing={24} className="b-paper">
            {entries.map((e: SoonBook): React.ReactNode => (
              <Grid item xs={12} sm={6} md={3} key={e.id}>
                <Card className={e.enabled ? '' : 'b-soon-card-disabled'}>
                  <CardHeader
                    classes={{ content: 'm-zero-min-width' }}
                    title={getAuthorName(e.author)}
                    subheader={e.title}
                    subheaderTypographyProps={{ noWrap: true }}
                  />
                  {Boolean(e.cover) && (
                    <div className="m-align-center">
                      <CardMedia
                        component="img"
                        image={e.cover.data.url}
                        className="b-soon-card-cover"
                      />
                    </div>
                  )}
                  <CardActions disableActionSpacing>
                    <FormControlLabel
                      control={(
                        <Switch
                          name="enabled"
                          onChange={(): void => this.handleToggleEnabled(e)}
                          checked={e.enabled}
                          color="secondary"
                        />
                      )}
                      disabled={isReadOnlyRoute}
                      label="Активен"
                    />

                    {!isReadOnlyRoute && (
                      <Tooltip title="Редактировать" placement="top">
                        <IconButton
                          className="m-margin_left-auto"
                          onClick={(): void => this.handleEditEntry(e)}
                        >
                          <EditIcon />
                        </IconButton>
                      </Tooltip>
                    )}
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Paper>

        <FormSoon
          onClose={this.handleModalClose}
          onSave={this.handleUpdateEntry}
          onDelete={this.props.soonDelete}
          onAuthorAutocomplete={this.handleAuthorAutocomplete}
          open={modalIsOpen}
          entry={entry}
        />

        {!isReadOnlyRoute && (
          <Tooltip title="Добавить книгу">
            <Fab
              color="primary"
              className="b-page__actions"
              onClick={this.handleCreate}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}
      </div>
    );
  }
}

export default Soon;
