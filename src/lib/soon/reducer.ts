import { SoonBook } from 'types';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { SoonAllActions } from './actions';

export interface SoonState {
  readonly entries: SoonBook[];
}

const initialState: SoonState = {
  entries: [],
};

const soon = (state = initialState, action: SoonAllActions): SoonState => {
  switch (action.type) {
    case types.SOON_READ:
      return {
        ...state,
        entries: action.payload,
      };

    case types.SOON_CREATE:
      return {
        ...state,
        entries: [...state.entries, action.payload],
      };

    case types.SOON_UPDATE:
      return {
        ...state,
        entries: state.entries.map((e: SoonBook): SoonBook => (e.id !== action.payload.id ? e : action.payload)),
      };

    case types.SOON_DELETE:
      return {
        ...state,
        entries: state.entries.filter((e: SoonBook): boolean => (e.id !== action.payload)),
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default soon;
