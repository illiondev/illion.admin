import { createSelector } from 'reselect';

import { Vacancy, VacanciesById } from 'types';
import { RootState } from 'lib';

export const getEntries = (state: RootState): Vacancy[] => state.vacancies.entries;
export const getVacanciesById = createSelector(
  getEntries,
  (entries: Vacancy[]): VacanciesById => entries.reduce((byId: VacanciesById, entry: Vacancy): VacanciesById => ({
    ...byId,
    [entry.id]: entry,
  }), {})
);
