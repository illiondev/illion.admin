import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from '@material-ui/core/Avatar';
import AddIcon from '@material-ui/icons/Add';
import pink from '@material-ui/core/colors/pink';
import { ButtonProps } from '@material-ui/core/Button';

import { Vacancy, ProtectedRoute } from 'types';

import router from 'services/RouterService';

import { VacanciesStoreProps, VacanciesDispatchProps } from './Vacancies.connect';

type Props = VacanciesStoreProps & VacanciesDispatchProps & ProtectedRoute;

class Vacancies extends Component<Props> {
  public componentDidMount(): void {
    this.props.vacanciesRead();
  }

  public render(): React.ReactNode {
    const { entries, isReadOnlyRoute } = this.props;

    return (
      <div className="b-page__content">
        {!isReadOnlyRoute && (
          <Tooltip title="Добавить вакансию" placement="top">
            <Fab
              color="primary"
              className="b-page__actions"
              component={(props: ButtonProps): React.ReactElement => <Link to={router.url.VACANCY_NEW()} {...props} />}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}

        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Вакансии</Typography>
          {entries.length > 0 && (
            <List>
              {entries.map((e: Vacancy): React.ReactNode => (
                <ListItem
                  key={e.id}
                  dense
                  button
                  component={
                    (props: ListItemProps): React.ReactElement => <Link to={router.url.VACANCY(`${e.id}`)} {...props} />
                  }
                >
                  <ListItemAvatar>
                    <Avatar style={{ backgroundColor: e.enabled ? pink[500] : '' }}>
                      {e.title.charAt(0).toUpperCase()}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={e.title} />
                </ListItem>
              ))}
            </List>
          )}
        </Paper>
      </div>
    );
  }
}

export default Vacancies;
