import { Vacancy } from 'types';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { VacanciesAllActions } from './actions';

export interface VacanciesState {
  readonly entries: Vacancy[];
}

const initialState: VacanciesState = {
  entries: [],
};

const vacancies = (state = initialState, action: VacanciesAllActions): VacanciesState => {
  switch (action.type) {
    case types.VACANCIES_READ:
      return {
        ...state,
        entries: action.payload,
      };

    case types.VACANCY_CREATE:
    case types.VACANCY_READ:
      return {
        ...state,
        entries: [...state.entries, action.payload],
      };

    case types.VACANCY_UPDATED:
      return {
        ...state,
        entries: state.entries.map((e: Vacancy): Vacancy => (e.id !== action.payload.id ? e : action.payload)),
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default vacancies;
