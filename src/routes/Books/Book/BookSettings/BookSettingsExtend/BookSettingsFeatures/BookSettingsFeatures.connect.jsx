import { connect } from 'react-redux';

import { bookFeaturesCreate, bookFeaturesUpdate, bookFeaturesDelete } from 'lib/books/actions';

import BookSettingsFeatures from './BookSettingsFeatures';

export default connect(null, {
  bookFeaturesCreate,
  bookFeaturesUpdate,
  bookFeaturesDelete,
})(BookSettingsFeatures);
