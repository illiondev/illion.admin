import api from 'services/ApiService';

import {
  StatsSummary, StatsBundle, StatsProfit, StatsFilter
} from 'types/stats';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

interface AllStatistics {
  readonly stats: StatsBundle[];
  readonly summary: StatsSummary;
}

type StatisticsSuccess = Action<types.STATISTICS_LIST, AllStatistics>;
type StatisticsProfitSuccess = Action<types.STATISTICS_PROFIT, StatsProfit[]>;

export type StatisticsAllActions = StatisticsSuccess | StatisticsProfitSuccess | AuthLogout;

function getStatisticsSuccess(payload: AllStatistics): StatisticsSuccess {
  return {
    type: types.STATISTICS_LIST,
    payload,
  };
}

export function getStatistics(query: StatsFilter = {} as StatsFilter): ActionThunk {
  return (dispatch): Promise<void> => Promise.all([
    api.statistics(query),
    api.statisticsSummary(query)
  ]).then(([stats, summary]): void => {
    dispatch(getStatisticsSuccess({
      stats: stats.data.data || [],
      summary: summary.data.data,
    }));
  });
}

function statisticsProfitSuccess(payload: StatsProfit[]): StatisticsProfitSuccess {
  return {
    type: types.STATISTICS_PROFIT,
    payload,
  };
}

export function statisticsProfit(query: StatsFilter = {} as StatsFilter): ActionThunk {
  return (dispatch): Promise<void> => api.statisticsProfit(query).then((res): void => {
    dispatch(statisticsProfitSuccess(res.data.data));
  });
}
