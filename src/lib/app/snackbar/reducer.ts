import { SNACKBAR_TYPES } from 'enums/SnakbarTypesEnums';

import types from './actionTypes';
import { AppSnackbarAllActions } from './actions';

export interface AppSnackbarState {
  readonly message: string;
  readonly type: SNACKBAR_TYPES;
}

const initialState: AppSnackbarState = {
  message: '',
  type: SNACKBAR_TYPES.INFO,
};

const snackbar = (state = initialState, action: AppSnackbarAllActions): AppSnackbarState => {
  switch (action.type) {
    case types.APP_SNACKBAR_SHOW:
      return {
        ...state,
        ...action.payload,
      };

    case types.APP_SNACKBAR_HIDE:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default snackbar;
