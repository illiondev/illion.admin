import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import apiService from 'services/ApiService';
import router from 'services/RouterService';

import ContentEditor from 'components/ContentEditor';

class BookChapter extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number,
      book_id: PropTypes.number,
      counter: PropTypes.number,
      title: PropTypes.string,
      content: PropTypes.string,
      completed: PropTypes.bool,
    }),
    chapterDraft: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      content: PropTypes.string,
      completed: PropTypes.bool,
    }),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
        chapter: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    bookChapterCreate: PropTypes.func.isRequired,
    bookChapterUpdate: PropTypes.func.isRequired,
    bookChapterDelete: PropTypes.func.isRequired,
    chapterDraftCreate: PropTypes.func.isRequired,
    chapterDraftUpdate: PropTypes.func.isRequired,
    chapterDraftDelete: PropTypes.func.isRequired,
  };

  static defaultProps = {
    entry: {},
    chapterDraft: {
      title: '',
      content: '',
      completed: false,
    },
  }

  componentDidMount() {
    const { match: { params: { chapter, id } } } = this.props;

    if (chapter === 'new') {
      this.props.bookChapterCreate({
        book_id: Number(id),
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { match: { params: { chapter, id } } } = this.props;

    if (chapter === 'new' && prevProps.match.params.chapter !== chapter) {
      this.props.bookChapterCreate({
        book_id: Number(id),
      });
    }
  }

  handleChangeInput = (e) => {
    const { name, type, checked } = e.target;
    let { value } = e.target;

    if (type === 'checkbox') {
      value = Boolean(checked);
    }

    this.updateChapterDraft({ [name]: value });
  };

  handleChangeEditor = (content) => {
    if (this.props.entry.content !== content) {
      this.updateChapterDraft({ content });
    }
  };

  handleUploadImage = (blobInfo) => {
    const { entry: { id, book_id } } = this.props;

    return apiService.bookChapterImage({ id, book_id }, blobInfo);
  };

  handleSave = () => {
    const { title, completed, content } = this.props.chapterDraft;

    const data = {
      title: title || '',
      completed: Number(completed),
      content,
      book_id: Number(this.props.match.params.id),
      id: Number(this.props.match.params.chapter),
    };

    this.props.bookChapterUpdate(data);
  };

  handleDelete = () => {
    this.props.bookChapterDelete(this.props.entry);
  };

  handleClearDraft = () => {
    this.props.chapterDraftDelete(this.props.entry.id);
  };

  updateChapterDraft(data = {}) {
    const { chapterDraft, entry } = this.props;

    if (!chapterDraft.id || entry.id !== chapterDraft.id) {
      this.props.chapterDraftCreate({
        title: entry.title,
        content: entry.content,
        id: entry.id,
        completed: entry.completed,
        ...data,
      });
    } else {
      this.props.chapterDraftUpdate(data);
    }
  }

  render() {
    const { entry, chapterDraft } = this.props;
    const isNew = this.props.match.params.chapter === 'new';
    const hasDraft = Boolean(chapterDraft.id);
    const source = hasDraft ? chapterDraft : entry;


    if (!isNew && !entry.id) {
      return (
        <Typography variant="h4" align="center" className="m-margin_top-normal">
          Глава не найдена
        </Typography>
      );
    }

    return (
      <Paper className="b-paper">
        <Typography variant="h4" align="center">
          {isNew ? 'Новая глава' : `Глава #${entry.counter}`}
          &nbsp;
          {hasDraft && '(черновик)'}
        </Typography>

        <Grid container spacing={24}>
          <Grid item xs={12}>
            <TextField
              id="title"
              name="title"
              label="Название главы"
              fullWidth
              value={source.title || ''}
              onChange={this.handleChangeInput}
            />
          </Grid>
          {Boolean(entry.id) && (
            <Grid item xs={12}>
              <ContentEditor
                content={source.content}
                entryId={entry.id}
                draftId={chapterDraft.id}
                onChange={this.handleChangeEditor}
                onImageUpload={this.handleUploadImage}
              />
            </Grid>
          )}
          <Grid item xs={12}>
            <FormControlLabel
              margin="normal"
              control={(
                <Checkbox
                  name="completed"
                  id="completed"
                  onChange={this.handleChangeInput}
                  checked={Boolean(source.completed)}
                />
              )}
              label="Закончена"
            />
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={24}>
              {!isNew && (
                <Grid item>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={this.handleDelete}
                  >
                    Удалить
                  </Button>
                </Grid>
              )}

              <Grid item className="m-margin_left-auto">
                {hasDraft ? (
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.handleClearDraft}
                  >
                    Удалить черновик
                  </Button>
                ) : (
                  <Button
                    variant="outlined"
                    color="primary"
                    component={Link}
                    to={router.url.BOOK_CHAPTER_PREVIEW(entry.book_id, entry.id)}
                  >
                    превью главы
                  </Button>
                )}
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleSave}
                >
                  {isNew ? 'Добавить' : 'Сохранить'}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default BookChapter;
