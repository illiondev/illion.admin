import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import LinkIcon from '@material-ui/icons/Link';

import { formatPrice } from 'utils';

import { PAYMENT_BILL_STATUS, PAYMENT_BILL_STATUS_TITLES_BY_ID } from 'enums/PaymentBillEnums';

class OrderBillItem extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      description: PropTypes.string,
      hash: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      status: PropTypes.number.isRequired,
      id: PropTypes.number.isRequired,
      created_at: PropTypes.string.isRequired,
      moderator_email: PropTypes.string.isRequired,
    }).isRequired,
    onCopyUrl: PropTypes.func.isRequired,
  };

  handleCopy = () => this.props.onCopyUrl(this.props.entry);

  render() {
    const {
      description, price, hash, status, created_at, moderator_email
    } = this.props.entry;

    return (
      <ListItem disableGutters divider>
        <ListItemText
          disableTypography
          primary={(
            <Fragment>
              <Typography variant="body2">
                {description || ''}
              </Typography>
              <Typography variant="subtitle1">
                Сумма&nbsp;
                <b>{formatPrice(price)}</b>
              </Typography>
            </Fragment>
          )}
          secondary={(
            <Fragment>
              <Typography gutterBottom variant="caption" noWrap title={hash}>
                <b>Статус:&nbsp;</b>
                {`${PAYMENT_BILL_STATUS_TITLES_BY_ID[status]}. `}
                <b>Хэш:&nbsp;</b>
                {hash}
              </Typography>
              <Typography gutterBottom variant="caption" noWrap title={new Date(created_at).toLocaleString()}>
                <b>Автор:&nbsp;</b>
                {`${moderator_email}. `}
                <b>Создан:&nbsp;</b>
                {new Date(created_at).toLocaleString()}
              </Typography>
            </Fragment>
          )}
        />

        {[PAYMENT_BILL_STATUS.NEW, PAYMENT_BILL_STATUS.WAITING_FOR_PAYMENT].includes(status) && (
          <ListItemSecondaryAction>
            <Tooltip title="Копировать ссылку на оплату" placement="top">
              <IconButton
                onClick={this.handleCopy}
                color="primary"
              >
                <LinkIcon />
              </IconButton>
            </Tooltip>
          </ListItemSecondaryAction>
        )}
      </ListItem>
    );
  }
}

export default OrderBillItem;
