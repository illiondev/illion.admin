import api from 'services/ApiService';

import { SoonBook, SoonBookReq } from 'types';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

type SoonReadSuccess = Action<types.SOON_READ, SoonBook[]>;
type SoonCreateSuccess = Action<types.SOON_CREATE, SoonBook>;
type SoonUpdateSuccess = Action<types.SOON_UPDATE, SoonBook>;
type SoonDeleteSuccess = Action<types.SOON_DELETE, number>;

export type SoonAllActions = SoonReadSuccess | SoonCreateSuccess | SoonUpdateSuccess | SoonDeleteSuccess | AuthLogout;

function soonReadSuccess(payload: SoonBook[]): SoonReadSuccess {
  return {
    type: types.SOON_READ,
    payload,
  };
}

export function soonRead(): ActionThunk {
  return (dispatch): Promise<void> => api.soonRead().then((res): void => {
    dispatch(soonReadSuccess(res.data.data || []));
  });
}

function soonCreateSuccess(payload: SoonBook): SoonCreateSuccess {
  return {
    type: types.SOON_CREATE,
    payload,
  };
}

export function soonCreate({ cover, ...data }: Partial<SoonBookReq>): ActionThunk {
  return async (dispatch): Promise<void> => {
    try {
      const res = await api.soonCreate(data);
      const entry = res.data.data;

      if (cover) {
        const coverRes = await api.soonImageUpdate({ image: cover as File, id: entry.id });
        entry.cover = coverRes.data;
      }

      dispatch(soonCreateSuccess(res.data.data));
    } catch (e) {
      throw e;
    }
  };
}


function soonUpdateSuccess(payload: SoonBook): SoonUpdateSuccess {
  return {
    type: types.SOON_UPDATE,
    payload,
  };
}

export function soonUpdate({ cover, ...data }: SoonBookReq): ActionThunk {
  return async (dispatch): Promise<void> => {
    try {
      if (cover) await api.soonImageUpdate({ image: cover as File, id: data.id });

      const res = await api.soonUpdate(data);

      dispatch(soonUpdateSuccess(res.data.data));
    } catch (e) {
      throw e;
    }
  };
}


function soonDeleteSuccess(payload: number): SoonDeleteSuccess {
  return {
    type: types.SOON_DELETE,
    payload,
  };
}

export function soonDelete(id: number): ActionThunk {
  return (dispatch): Promise<void> => api.soonDelete(id).then((): void => {
    dispatch(soonDeleteSuccess(id));
  });
}
