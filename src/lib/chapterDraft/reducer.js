import * as types from './actionTypes';

const initialState = {
  content: '',
  title: '',
  id: undefined,
  completed: false,
};

const chapterDraft = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CHAPTER_DRAFT_CREATE:
      return {
        ...state,
        ...payload,
      };

    case types.CHAPTER_DRAFT_UPDATE:
      return {
        ...state,
        ...payload,
      };

    case types.CHAPTER_DRAFT_DELETE:
      return state.id === payload ? {
        ...initialState,
      } : state;


    default:
      return state;
  }
};

export default chapterDraft;
