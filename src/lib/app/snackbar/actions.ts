import { Action } from 'types/redux';
import { SNACKBAR_TYPES } from 'enums/SnakbarTypesEnums';

import types from './actionTypes';

interface SnackbarPayload {
  message: string;
  type: SNACKBAR_TYPES;
}

type SnackbarShow = Action<types.APP_SNACKBAR_SHOW, SnackbarPayload>;
type SnackbarHide = Action<types.APP_SNACKBAR_HIDE, undefined>;

export type AppSnackbarAllActions = SnackbarShow | SnackbarHide;

export function snackbarShow(payload: SnackbarPayload): SnackbarShow {
  return {
    type: types.APP_SNACKBAR_SHOW,
    payload
  };
}

export function snackbarHide(): SnackbarHide {
  return {
    type: types.APP_SNACKBAR_HIDE,
    payload: undefined,
  };
}
