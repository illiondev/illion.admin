import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';

import { RegisterReq } from 'types';

import { composeValidators, emailFormat, required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';


interface Props {
  onSave(req: RegisterReq): void;
}

interface FormValues {
  email: string;
  name: string;
  password: string;
  password_confirmation?: string;
}

interface FormErrors {
  password_confirmation?: string;
}

class SignUpForm extends Component<Props> {
  private handleSubmit = (values: object): void => {
    const { email, name, password } = values as FormValues;

    this.props.onSave({ email, name, password });
  };

  public render(): React.ReactNode {
    return (
      <Form
        onSubmit={this.handleSubmit}
        subscription={{ submitting: true, pristine: true }}
        initialValues={{ remember_me: false }}
        validate={(values: object): FormErrors => {
          const { password_confirmation, password } = values as FormValues;
          const errors: FormErrors = {};

          if (password && password !== password_confirmation) {
            errors.password_confirmation = 'Не совпадает значение';
          }

          return errors;
        }}
        render={({ handleSubmit }): React.ReactNode => (
          <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <Field
                label="Имя"
                name="name"
                autoFocus
                component={FormFieldText}
                validate={required}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <Field
                label="Email"
                name="email"
                type="email"
                autoFocus
                component={FormFieldText}
                validate={composeValidators(required, emailFormat)}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <Field
                name="password"
                label="Пароль"
                type="password"
                component={FormFieldText}
                validate={required}
                fullWidth
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <Field
                name="password_confirmation"
                label="Подтверждение пароля"
                type="password"
                component={FormFieldText}
                validate={required}
                fullWidth
              />
            </FormControl>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className="b-auth__form-submit"
            >
              Зарегистрироваться
            </Button>
          </form>
        )}
      />
    );
  }
}

export default SignUpForm;
