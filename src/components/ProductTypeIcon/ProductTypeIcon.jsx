import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Tooltip from '@material-ui/core/Tooltip';

import { BOOK_TYPES_TITLE_BY_ID, BOOK_TYPES_ICON_BY_ID } from 'enums/BookTypesEnum';

class ProductTypeIcon extends PureComponent {
  static propTypes = {
    type: PropTypes.number.isRequired,
  };

  render() {
    const { type } = this.props;
    if (!type) return null;

    return (
      <Tooltip title={BOOK_TYPES_TITLE_BY_ID[type]}>
        {BOOK_TYPES_ICON_BY_ID[type]}
      </Tooltip>
    );
  }
}

export default ProductTypeIcon;
