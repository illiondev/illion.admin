import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';

import router from 'services/RouterService';

import './BookMenu.scss';

class BookMenu extends Component {
  static propTypes = {
    chapters: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      book_id: PropTypes.number.isRequired,
      title: PropTypes.string,
    })).isRequired,
    draftId: PropTypes.number,
  };

  static defaultProps = {
    draftId: undefined,
  };

  render() {
    const { chapters, draftId } = this.props;

    if (chapters.length === 0) return null;

    return (
      <Drawer
        anchor="right"
        variant="permanent"
        classes={{
          paper: 'b-book-menu__drawer',
        }}
      >
        <List>
          {chapters.map((chapter, i) => (
            <ListItem
              key={chapter.id}
              dense
              button
              component={NavLink}
              to={router.url.BOOK_CHAPTER(chapter.book_id, chapter.id)}
              classes={{
                root: chapter.completed ? '' : 'b-book-menu__chapter-incomplete'
              }}
              activeClassName="b-book-menu__chapter-current"
            >
              <ListItemText
                primary={`Глава ${i + 1}${chapter.id === draftId ? ' (черновик)' : ''}`}
                secondary={chapter.title}
                secondaryTypographyProps={{ noWrap: true, title: chapter.title }}
              />
            </ListItem>
          ))}
        </List>
      </Drawer>
    );
  }
}

export default BookMenu;
