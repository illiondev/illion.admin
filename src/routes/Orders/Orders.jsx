import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconError from '@material-ui/icons/Error';

import { formatPrice } from 'utils';
import { getBundleType } from 'utils/bundles';

import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';
import { PAYMENT_STATUS_TITLE_BY_ID } from 'enums/PaymentEnums';
import { SHIPPING_CATEGORIES_TITLE_BY_ID } from 'enums/OrderEnums';

import router from 'services/RouterService';

import OrdersFilter from './OrdersFilter';
import './Orders.scss';

const ORDERS_COLS = [
  { name: 'ID', sort: 'id' },
  { name: 'Shiptor', sort: 'shiptor_id' },
  { name: 'YandexID', sort: 'payment_id' },
  { name: 'ФИО', sort: 'fio' },
  { name: 'Email', sort: 'email' },
  { name: 'Состав' },
  { name: 'Статус', sort: 'status', numeric: true },
  { name: 'Способ доставки', sort: 'shipping_method', numeric: true },
  { name: 'Сумма', sort: 'total_price', numeric: true },
  { name: 'Дата', sort: 'created_at', numeric: true },
];

class Orders extends Component {
  static propTypes = {
    bundles: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      shiptor_id: PropTypes.number,
      shiptor_error: PropTypes.string,
      tracking_number: PropTypes.string,
      payment_id: PropTypes.string,
      surname: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      patronimic: PropTypes.string,
      email: PropTypes.string.isRequired,
      status: PropTypes.number.isRequired,
      shipping_method: PropTypes.number,
      total_price: PropTypes.number.isRequired,
      created_at: PropTypes.string.isRequired,
    })).isRequired,
    pager: PropTypes.shape({
      page: PropTypes.number,
    }).isRequired,
    query: PropTypes.shape({
      sort_by: PropTypes.string,
      sort_dir: PropTypes.string,
    }).isRequired,
    exportUrl: PropTypes.string.isRequired,
    getOrders: PropTypes.func.isRequired,
    bundleRead: PropTypes.func.isRequired,
    ordersSpreadsheet: PropTypes.func.isRequired,
    ordersSpreadsheetDrop: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { bundles, query } = this.props;

    this.props.getOrders(query);

    if (bundles.length === 0) {
      this.props.bundleRead();
    }
  }

  componentDidUpdate(prevProps) {
    const { entries, exportUrl } = this.props;

    if (entries !== prevProps.entries) {
      window.scrollTo(0, 0);
    }

    if (exportUrl && exportUrl !== prevProps.exportUrl) {
      const newTab = window.open(exportUrl, '_blank');

      if (newTab) {
        newTab.focus();
        this.props.ordersSpreadsheetDrop();
      }
    }
  }

  getSortDir(sort_dir) {
    return sort_dir === 'asc' ? 'desc' : 'asc';
  }

  handleChangePage = (e, page) => {
    const params = {
      ...this.props.query,
      page: page + 1
    };

    this.props.getOrders(params);
  };

  handleSort = (sortId) => {
    const { pager, query: { sort_by, sort_dir } } = this.props;
    const params = { sort_by: sortId, sort_dir: sort_by !== sortId ? 'asc' : this.getSortDir(sort_dir) };

    this.props.getOrders({
      page: sort_by !== sortId ? 1 : pager.page || 1,
      ...this.props.query,
      ...params,
    });
  };

  handleChangeFilter = (filter) => {
    const { query } = this.props;
    let params;

    if (filter) {
      params = {
        page: 1,
        ...filter,
      };
    } else { // если фильтра нет, сбрасываем пагинацию, но сохраняем сортировку
      params = {
        page: 1,
      };

      if (query.sort_by) {
        params.sort_by = query.sort_by;
        params.sort_dir = query.sort_dir;
      }
    }

    this.props.getOrders(params);
  };

  handleExportData = (filter) => {
    const { query } = this.props;
    const params = {
      ...filter,
    };

    if (query.sort_by) {
      params.sort_by = query.sort_by;
      params.sort_dir = query.sort_dir;
    }

    this.props.ordersSpreadsheet(params);
  };

  render() {
    const {
      entries, pager, query: { sort_by, sort_dir, ...filter }, bundles
    } = this.props;
    const isActiveFilter = Object.keys(this.props.query).length > 0;

    return (
      <div>
        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Заказы</Typography>

          <OrdersFilter
            current={filter}
            onSubmit={this.handleChangeFilter}
            onExport={this.handleExportData}
            isActive={isActiveFilter}
            bundles={bundles}
          />

          {entries.length === 0 && isActiveFilter && (
            <Typography variant="h5" align="center" className="m-margin_top-medium">
              По данному критерию ничего не найдено
              <br />
              <br />
            </Typography>
          )}

          {entries.length > 0 && (
            <Fragment>
              <div className="b-table-responsive">
                <Table>
                  <TableHead>
                    <TableRow>
                      {ORDERS_COLS.map(col => (
                        <TableCell
                          padding="dense"
                          key={col.name}
                          align={col.numeric ? 'right' : 'left'}
                          sortDirection={sort_by === col.sort ? sort_dir : false}
                        >
                          {col.sort ? (
                            <Tooltip
                              title="Сортировать"
                              enterDelay={300}
                            >
                              <TableSortLabel
                                active={sort_by === col.sort}
                                direction={sort_dir}
                                onClick={() => this.handleSort(col.sort)}
                              >
                                {col.name}
                              </TableSortLabel>
                            </Tooltip>
                          ) : col.name}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {entries.map(e => (
                      <TableRow
                        key={e.id}
                        hover
                        onClick={() => router.push(router.url.ORDER(e.id))}
                        className="b-orders__row"
                      >
                        <TableCell padding="dense">{e.id}</TableCell>
                        <TableCell padding="dense">
                          {Boolean(e.shiptor_error) && (
                            <Tooltip title={e.shiptor_error}>
                              <Typography color="error" noWrap>
                                <IconError className="b-orders__icon-error" />
                                  &nbsp;
                                  Ошибка
                              </Typography>
                            </Tooltip>
                          )}
                          {Boolean(e.shiptor_id) && (
                            <Typography>
                              <b>ID:&nbsp;</b>
                              {e.shiptor_id}
                            </Typography>
                          )}
                          {Boolean(e.tracking_number) && (
                            <Typography>
                              <b>Tracking:&nbsp;</b>
                              <a
                                href={`https://gdeposylka.ru/courier/shiptor/tracking/${e.tracking_number}`}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {e.tracking_number}
                              </a>
                            </Typography>
                          )}
                        </TableCell>
                        <TableCell padding="dense">{e.payment_id}</TableCell>
                        <TableCell padding="dense">{[e.surname, e.name, e.patronimic].join(' ')}</TableCell>
                        <TableCell padding="dense">{e.email}</TableCell>
                        <TableCell padding="dense">
                          <ul className="b-orders__product-list">
                            {e.items.data.map(b => (
                              <li key={b.bundle_id}>
                                {/* eslint-disable-next-line max-len */}
                                {`${b.title} (${BOOK_TYPES_TITLE_BY_ID[getBundleType(b.products.data)]}) - ${b.quantity} шт.`}
                              </li>
                            ))}
                          </ul>
                        </TableCell>
                        <TableCell padding="dense" align="right">{PAYMENT_STATUS_TITLE_BY_ID[e.status]}</TableCell>
                        <TableCell padding="dense" align="right">
                          {SHIPPING_CATEGORIES_TITLE_BY_ID[e.shipping_category]}
                        </TableCell>
                        <TableCell padding="dense" align="right">{formatPrice(e.total_price)}</TableCell>
                        <TableCell padding="dense" align="right">{new Date(e.created_at).toLocaleString()}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>

              {pager.total_pages > 1 && (
                <TablePagination
                  colSpan={3}
                  component="div"
                  count={pager.total}
                  rowsPerPage={pager.per_page}
                  rowsPerPageOptions={[pager.per_page]}
                  page={pager.current_page - 1}
                  onChangePage={this.handleChangePage}
                />
              )}
            </Fragment>
          )}
        </Paper>
      </div>
    );
  }
}

export default Orders;
