import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { transformBundlePricesToFormPrices, transformFormPricesToBundlePrices } from 'utils/bundles';

import Divider from '@material-ui/core/Divider';
import BundleOptions from './BundleOptions';
import BundlePrices from './BundlePrices';
import BundleDetails from './BundleDetails';
import BundleIncludes from './BundleIncludes';
import BundleActions from './BundleActions';

class BundleSettings extends Component {
  static propTypes = {
    bookId: PropTypes.number.isRequired,
    bundle: PropTypes.shape({
      id: PropTypes.number,
      enabled: PropTypes.bool,
      pre_order: PropTypes.bool,
      cover: PropTypes.shape({
        data: PropTypes.shape({
          id: PropTypes.number.isRequired,
          url: PropTypes.string.isRequired,
        }),
      }),
      products: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
      }).isRequired,
    }).isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onDeleteCover: PropTypes.func.isRequired,
  };

  form = null;

  handleSubmit = ({ cover, ...formData }) => {
    const { bundle, bookId } = this.props;

    const data = {
      book_id: bookId,
      enabled: Number(formData.enabled || 0),
      pre_order: Number(formData.pre_order || 0),
      title: formData.title || '',
      description: formData.description || '',
      slug: formData.slug || '',
      notification: formData.notification || '',
      hidden: Boolean(formData.hidden),
      // если в значении есть поле data, то это родное значение, полученное с сервера
      cover: cover && !cover.data ? cover : undefined,
      prices: transformFormPricesToBundlePrices(formData)
    };

    if (bundle.id) {
      data.id = bundle.id;
      this.props.onUpdate(data);
    } else {
      this.props.onCreate(data);
    }
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  handleDelete = () => {
    if (window.confirm('Удалить из книги?')) {
      this.props.onDelete({
        book_id: this.props.bookId,
        id: this.props.bundle.id,
      });
    }
  };

  handleDeleteCover = () => {
    this.props.onDeleteCover({
      book_id: this.props.bookId,
      bundle_id: this.props.bundle.id,
      id: this.props.bundle.cover.data.id,
    });
  };

  render() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { bundle: { includes, prices, ...bundle }, isReadOnly, bookId } = this.props;
    const initialValues = {
      ...bundle,
      ...transformBundlePricesToFormPrices(prices)
    };

    return (
      <Fragment>
        <div className="b-product__dialog-title">
          <Typography variant="title" align="center">
            {bundle.id ? `Комплект #${bundle.id}` : 'Новый комплект'}
          </Typography>
        </div>
        <div className="m-margin_bottom-normal">
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={initialValues}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={8}>
                    <BundlePrices isReadOnly={isReadOnly} />
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <BundleOptions isReadOnly={isReadOnly} />
                  </Grid>
                </Grid>

                <BundleDetails isReadOnly={isReadOnly} onDeleteCover={this.handleDeleteCover} />
              </form>
            )}
          />
        </div>

        <BundleIncludes
          isReadOnly={isReadOnly}
          includes={bundle.id ? bundle.products.data : undefined}
          bookId={bookId}
          bundleId={bundle.id}
        />

        <Divider className="m-margin_top-big" />

        <BundleActions
          isReadOnly={isReadOnly}
          onClose={this.props.onClose}
          onSave={this.handleSave}
          onDelete={bundle.id ? this.handleDelete : undefined}
        />
      </Fragment>
    );
  }
}

export default BundleSettings;
