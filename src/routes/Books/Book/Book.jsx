import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Switch } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import SettingsIcon from '@material-ui/icons/Settings';
import PublishIcon from '@material-ui/icons/Publish';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import router from 'services/RouterService';

import RoutePrivate from 'components/RoutePrivate';

import BookSettings from './BookSettings';
import BookChapter from './BookChapter';
import BookChapterPreview from './BookChapterPreview';
import BookMenu from './BookMenu';

import './Book.scss';


class Book extends Component {
  static propTypes = {
    bookInit: PropTypes.func.isRequired,
    bookRead: PropTypes.func.isRequired,
    bookChapterInit: PropTypes.func.isRequired,
    bookPublish: PropTypes.func.isRequired,
    bookUnpublish: PropTypes.func.isRequired,
    bookClear: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    entry: PropTypes.shape({
      id: PropTypes.number,
      bookChapters: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({})),
      }),
    }),
    chapterDraftId: PropTypes.number,
    chapters: PropTypes.arrayOf(PropTypes.shape({})),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    entry: undefined,
    chapterDraftId: undefined,
    chapters: [],
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isFetching !== prevState.isFetching) {
      return {
        notFound: !nextProps.isFetching && !nextProps.entry,
        isFetching: nextProps.isFetching,
      };
    }

    return null;
  }

  state = {
    notFound: false,
    isFetching: false,
  };

  componentDidMount() {
    const bookId = Number(this.props.match.params.id);

    if (bookId && !this.props.entry && !this.props.isFetching) {
      this.props.bookRead(bookId);
    }

    if (this.props.entry) this.initBook();
  }

  componentDidUpdate(prevProps) {
    if (this.props.entry && !prevProps.entry) this.initBook();

    const bookId = Number(this.props.match.params.id);

    if (prevProps.entry && bookId && bookId !== prevProps.entry.id) {
      this.props.bookRead(bookId);
    }
  }

  componentWillUnmount() {
    this.props.bookClear();
  }

  initBook = () => {
    this.props.bookInit(this.props.entry);
    this.props.bookChapterInit({
      entries: this.props.entry.bookChapters.data,
      bookId: this.props.entry.id,
    });
  };

  handlePublish = () => {
    this.props.bookPublish(this.props.entry.id);
  };

  handleUnpublish = () => {
    this.props.bookUnpublish(this.props.entry.id);
  };

  render() {
    const {
      entry, chapters, match: { url }, chapterDraftId, location: { pathname }
    } = this.props;
    const isChapterPreview = /^\/books\/\d+\/preview\/\d+$/.test(pathname);

    if (this.state.notFound) {
      return (
        <Typography variant="h4" align="center" className="m-margin_top-normal">
          Книга не найдена
        </Typography>
      );
    }

    if (!entry) return null;

    return (
      <div className="b-page__content">
        <BookMenu chapters={chapters} draftId={chapterDraftId} />

        {false && !isChapterPreview && (
          <div className="b-page__actions">
            {Boolean(entry.completed_at) && (
              <Tooltip title="Скрыть" placement="top">
                <Fab
                  color="default"
                  onClick={this.handleUnpublish}
                >
                  <VisibilityOffIcon />
                </Fab>
              </Tooltip>
            )}

            <Tooltip title="Опубликовать" placement="top">
              <Fab
                className="m-margin_top-normal"
                color="default"
                onClick={this.handlePublish}
              >
                <PublishIcon />
              </Fab>
            </Tooltip>
            <Tooltip title="Настройки" placement="top">
              <Fab
                className="m-margin_top-normal"
                color="secondary"
                component={Link}
                to={router.url.BOOK_EDIT(entry.id)}
              >
                <SettingsIcon />
              </Fab>
            </Tooltip>

            {!entry.epub && (
              <Tooltip title="Новая глава" placement="top">
                <Fab
                  color="primary"
                  className="m-margin_top-normal"
                  component={Link}
                  to={`${url}/chapter/new`}
                >
                  <AddIcon />
                </Fab>
              </Tooltip>
            )}
          </div>
        )}

        <Switch>
          <RoutePrivate path={router.path.BOOK_EDIT} component={BookSettings} />
          <RoutePrivate path={router.path.BOOK_CHAPTER} component={BookChapter} exact />
          <RoutePrivate path={router.path.BOOK_CHAPTER_PREVIEW} component={BookChapterPreview} />
        </Switch>
      </div>
    );
  }
}

export default Book;
