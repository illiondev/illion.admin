import React, { Component } from 'react';
import { Editor } from '@tinymce/tinymce-react';

import { EDITOR_KEY } from 'config';
import { BlobInfo, EditorTypes } from 'types/tinymce';


export interface ContentEditorProps {
  content: string;
  editorStyles: {
    styles: string[];
    css: string;
  };
  onChange(arg: string): void;
  onImageUpload(arg: BlobInfo): Promise<void>;
  disabled: boolean;
  options: Record<string, any>;
}

const DEFAULT_OPTIONS = {
  height: 500,
  plugins: 'link lists image code media textcolor colorpicker imagetools paste',
  toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | outdent indent blockquote | removeformat bullist numlist link image media table forecolor backcolor', // eslint-disable-line max-len
  entity_encoding: 'raw',
  branding: false,
  style_formats: [] as string[],
  content_style: '',
};

class ContentEditor extends Component<ContentEditorProps> {
  public static defaultProps = {
    content: '',
    onImageUpload: undefined,
    editorStyles: {},
    options: {},
    disabled: false,
  };

  private editor: EditorTypes|null = null;

  public shouldComponentUpdate(nextProps: ContentEditorProps): boolean {
    const { content } = this.props;

    return !content && nextProps.content !== content;
  }

  public componentDidUpdate(): void {
    if (this.editor) this.editor.setContent(this.props.content);
  }

  private handleEditorChange = (content: string): void => {
    this.props.onChange(content);
  };

  private handleSetupEditor = (editor: EditorTypes): void => {
    this.editor = editor;

    if (this.props.onImageUpload) {
      this.editor.settings.images_upload_handler = (blobInfo, success, failure): Promise<void> => {
        this.props.onImageUpload(blobInfo).then((res: any): void => {
          success(res.data.data.url);
        }).catch((e: any): void => {
          failure(e);
        });

        return Promise.resolve();
      };
    } else {
      this.editor.settings.file_picker_callback = (cb): void => {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        input.onchange = function (): void {
          // @ts-ignore
          const file = this.files[0];
          const reader: FileReader = new FileReader();

          reader.onload = function (): void {
            const id = `blobid${(new Date()).getTime()}`;
            const blobCache = editor.editorUpload.blobCache;
            const base64 = (reader.result as string).split(',')[1];
            const blobInfo: BlobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            cb((): void => {}, blobInfo.blobUri(), { title: file.name });
          };

          reader.readAsDataURL(file);
        };

        input.click();
      };
    }
  };

  public render(): React.ReactNode {
    const { editorStyles, disabled } = this.props;
    const options = {
      ...DEFAULT_OPTIONS,
      ...this.props.options,
      setup: this.handleSetupEditor,
      readonly: disabled,
    };

    if (editorStyles.styles && editorStyles.styles.length > 0) {
      options.style_formats = editorStyles.styles;
      options.toolbar = `${options.toolbar} | styleselect`;
      options.content_style = editorStyles.css || '';
    }

    return (
      <Editor
        initialValue={this.props.content || ''}
        onEditorChange={this.handleEditorChange}
        apiKey={EDITOR_KEY}
        init={options}
      />
    );
  }
}

export default ContentEditor;
