import { connect } from 'react-redux';

import { snackbarShow } from 'lib/app/snackbar/actions';
import { bookBundleUpdate, bundleAutocomplete, bookBundleAttach } from 'lib/books/actions';
import { getBooksById } from 'lib/books/selectors';

import BookSettingsBundles from './BookSettingsBundles';

function mapStateToProps(state, ownProps) {
  return {
    book: getBooksById(state)[ownProps.match.params.id],
  };
}

export default connect(mapStateToProps, {
  bundleAutocomplete,
  bookBundleUpdate,
  bookBundleAttach,
  snackbarShow,
})(BookSettingsBundles);
