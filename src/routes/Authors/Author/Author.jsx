import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import AuthorSettings from './AuthorSettings';

class Author extends Component {
  static propTypes = {
    entry: PropTypes.shape({}),
    authorUpdate: PropTypes.func.isRequired,
    authorRead: PropTypes.func.isRequired,
    authorUserCreate: PropTypes.func.isRequired,
    authorUserDelete: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string,
      }),
    }).isRequired,
  };

  static defaultProps = {
    entry: undefined,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isFetching !== prevState.isFetching) {
      return {
        notFound: !nextProps.isFetching && !nextProps.entry,
        isFetching: nextProps.isFetching,
      };
    }

    return null;
  }

  state = {
    notFound: false,
    isFetching: false,
  };

  componentDidMount() {
    if (!this.props.entry && !this.props.isFetching) {
      this.props.authorRead(this.props.match.params.id);
    }
  }

  render() {
    const { isReadOnlyRoute, entry } = this.props;
    return (
      <div>
        {this.state.notFound && (
          <Typography variant="h4" align="center" className="m-margin_top-normal">
            Автор не найден
          </Typography>
        )}
        {entry && (
          <AuthorSettings
            onSubmit={this.props.authorUpdate}
            onAddUser={this.props.authorUserCreate}
            onDeleteUser={this.props.authorUserDelete}
            entry={entry}
            isReadOnly={isReadOnlyRoute}
          />
        )}
      </div>
    );
  }
}

export default Author;
