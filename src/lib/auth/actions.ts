import { AxiosError } from 'axios';
import { ITokensData } from 'illion-api';

import api from 'services/ApiService';
import router from 'services/RouterService';

import { Action, ActionThunk } from 'types/redux';
import { ForgotReq, LoginReq, RegisterReq } from 'types';

import types from './actionTypes';

const START_URL = router.url.BOOKS();

export type AuthLogin = Action<types.LOG_IN, ITokensData>;
export type AuthLogout = Action<types.LOG_OUT, undefined>;

export type AuthAllActions = AuthLogin | AuthLogout;

export function authLogin(credentials: LoginReq): ActionThunk {
  return (): Promise<void> => api.authLogin(credentials).then((): void => {
    router.push(START_URL);
  });
}

export function authRegister(credentials: RegisterReq): ActionThunk {
  return (): Promise<void> => api.authRegister(credentials).then((): void => {
    router.push(START_URL);
  });
}

export function authLogout(): ActionThunk {
  return (): Promise<void> => api.authLogout().then((): void => {
    router.push(START_URL);
  }).catch((e: AxiosError): void => {
    api.clearToken();

    throw e;
  });
}

export function authForgot(data: ForgotReq): ActionThunk {
  return (): Promise<void> => api.authForgot(data).then((): void => {
    router.push(router.url.RESTORE_STATUS('next'));
  });
}
