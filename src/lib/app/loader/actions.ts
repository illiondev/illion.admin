import { Action } from 'types/redux';

import types from './actionTypes';

type LoaderShow = Action<types.APP_LOADER_SHOW, undefined>;
type LoaderHide = Action<types.APP_LOADER_HIDE, undefined>;

export type AppLoaderAllActions = LoaderShow | LoaderHide;

export function loaderShow(): LoaderShow {
  return {
    type: types.APP_LOADER_SHOW,
    payload: undefined,
  };
}

export function loaderHide(): LoaderHide {
  return {
    type: types.APP_LOADER_HIDE,
    payload: undefined,
  };
}
