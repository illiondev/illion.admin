enum StatisticsActionTypes {
  STATISTICS_LIST = 'STATISTICS_LIST',
  STATISTICS_PROFIT = 'STATISTICS_PROFIT',
}

export default StatisticsActionTypes;
