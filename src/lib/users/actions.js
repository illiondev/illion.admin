import api from 'services/ApiService';

import { profileMeSuccess } from 'lib/profile/actions';
import * as types from './actionTypes';

function getUsersSuccess(payload) {
  return {
    type: types.USER_LIST,
    payload,
  };
}

export function getUsers(query = {}) {
  return dispatch => api.users(query).then((res) => {
    dispatch(getUsersSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
    }));
  });
}

function userModeratorCreateSuccess(payload) {
  return {
    type: types.USER_MODERATOR_CREATE,
    payload,
  };
}

export function userModeratorCreate(email) {
  return dispatch => api.userModeratorCreate({ email }).then((res) => {
    dispatch(userModeratorCreateSuccess(res.data.data));
  });
}

function userModeratorDeleteSuccess(payload) {
  return {
    type: types.USER_MODERATOR_UPDATE,
    payload,
  };
}

export function userModeratorDelete(id) {
  return dispatch => api.userModeratorDelete(id).then(() => {
    dispatch(userModeratorDeleteSuccess(id));
  });
}

function userUpdateSuccess(payload) {
  return {
    type: types.USER_UPDATE,
    payload,
  };
}

export function userUpdate(data, isMe = false) {
  return dispatch => api.userUpdate(data).then((res) => {
    const entry = res.data.data;

    if (isMe) {
      dispatch(profileMeSuccess(entry));
    }

    dispatch(userUpdateSuccess(entry));
  });
}

export function userCommentCreate(userId, comment) {
  return dispatch => api.userCommentCreate(userId, { comment }).then((res) => {
    dispatch(userUpdateSuccess(res.data.data));
  });
}
