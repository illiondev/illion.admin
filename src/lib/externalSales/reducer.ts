import { ExternalSale, Pager } from 'types';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { ExternalSalesAllActions } from './actions';

export interface ExternalSalesState {
  readonly entries: ExternalSale[];
  readonly pager: Pager;
}

const initialState: ExternalSalesState = {
  entries: [],
  pager: {} as Pager,
};

const externalSales = (state = initialState, action: ExternalSalesAllActions): ExternalSalesState => {
  switch (action.type) {
    case types.EXTERNAL_SALES_READ:
      return {
        ...state,
        entries: action.payload.entries,
        pager: action.payload.pager,
      };

    case types.EXTERNAL_SALE_CREATE:
      return {
        ...state,
        entries: [action.payload, ...state.entries],
      };

    case types.EXTERNAL_SALE_UPDATE:
      return {
        ...state,
        entries: state.entries.map(
          (e: ExternalSale): ExternalSale => (e.id !== action.payload.id ? e : action.payload)
        ),
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default externalSales;
