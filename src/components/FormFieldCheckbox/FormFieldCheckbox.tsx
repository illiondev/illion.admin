import React, { Component } from 'react';

import { FieldRenderProps } from 'react-final-form';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export interface Props extends FieldRenderProps<any> {
  label: string;
}

class FormFieldCheckbox extends Component<Props> {
  public render(): React.ReactNode {
    const {
      input: {
        name, onChange, value, checked, ...restInput
      },
      label,
      ...rest
    } = this.props;

    return (
      <FormControlLabel
        control={(
          <Checkbox
            {...rest}
            name={name}
            inputProps={restInput}
            onChange={onChange}
            checked={checked}
            value={String(value)}
          />
        )}
        label={label}
      />
    );
  }
}

export default FormFieldCheckbox;
