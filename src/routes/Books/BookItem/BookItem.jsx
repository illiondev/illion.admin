import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import LinkIcon from '@material-ui/icons/Link';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';

import { getAuthorName } from 'utils';

import router from 'services/RouterService';
import { PUBLIC_URL } from 'config';

class BookItem extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number.isRequired,
      author: PropTypes.shape({
        data: PropTypes.shape({
          id: PropTypes.number,
          name: PropTypes.string,
          surname: PropTypes.string,
        }).isRequired
      }).isRequired,
    }).isRequired,
  };

  render() {
    const {
      entry: {
        title, id, author, coverSmall
      }
    } = this.props;
    const bookCover = coverSmall ? coverSmall.data.url : '';

    return (
      <ListItem
        key={id}
        dense
        button
        component={Link}
        to={router.url.BOOK_EDIT_BASIC(id)}
      >
        <ListItemAvatar>
          {bookCover ? (
            <Avatar src={bookCover} alt="обложка" />
          ) : (
            <Avatar>
              <FormatListBulletedIcon />
            </Avatar>
          )}
        </ListItemAvatar>
        <ListItemText primary={title} secondary={getAuthorName(author)} />
        <ListItemSecondaryAction>
          <Tooltip title="Открыть страницу книги" placement="top">
            <IconButton component="a" href={`${PUBLIC_URL}/books/${id}`} target="_blank" rel="noopener noreferrer">
              <LinkIcon />
            </IconButton>
          </Tooltip>
        </ListItemSecondaryAction>
      </ListItem>
    );
  }
}

export default BookItem;
