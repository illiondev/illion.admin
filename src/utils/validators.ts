type FormValidatorMessage = string|undefined;

type ValidateRule<T = string> = (value: T) => FormValidatorMessage;

export const required = (value: string): FormValidatorMessage => (value ? undefined : 'Обязательное поле');

export const requiredId = (value: { id?: number|string}): FormValidatorMessage => (
  value && value.id ? undefined : 'Обязательное поле'
);

export const emailFormat = (value: string): FormValidatorMessage => (
  value && /^\S+@\S+\.\S+/.test(value) ? undefined : 'Ошибка в формате адреса'
);

export const phoneFormat = (value: string): FormValidatorMessage => (
  value && Number(value.replace('+', '')) ? undefined : 'Ошибка в формате'
);


export const minLength = (min: number): ValidateRule => (value: string): FormValidatorMessage => (
  value && value.length >= min ? undefined : `Не менее ${min} символов`
);

export const maxLength = (max: number): ValidateRule => (value: string): FormValidatorMessage => (
  value && value.length <= max ? undefined : `Не более ${max} символов`
);

export const maxNumber = (max: number): ValidateRule<number> => (value: number): FormValidatorMessage => {
  if (!value) return undefined;

  return value <= max ? undefined : `Максимальное значение ${max}`;
};

type ComposedValidateRule = ValidateRule|ValidateRule<number>;

export const composeValidators = (
  ...validators: (ComposedValidateRule)[]
): ComposedValidateRule => (value: any): FormValidatorMessage => (
  validators.reduce(
    (error: FormValidatorMessage, validator: ComposedValidateRule): FormValidatorMessage => error || validator(value),
    undefined
  )
);
