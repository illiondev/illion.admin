import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import NativeSelect from '@material-ui/core/NativeSelect';

import { SelectOption } from 'types';

export interface Props extends FieldRenderProps<any> {
  label: string;
  skipEmpty: boolean;
  options: SelectOption[];
  className: string;
}

class FormFieldSelect extends Component<Props> {
  public render(): React.ReactNode {
    const {
      skipEmpty, label, options, className,
      input: {
        name, onChange, value, ...restInput
      },
      meta,
      ...rest
    } = this.props;

    return (
      <FormControl fullWidth error={Boolean(meta.touched && meta.error)} className={className}>
        <InputLabel>{label}</InputLabel>
        <NativeSelect
          {...rest}
          name={name}
          error={meta.error && meta.touched}
          inputProps={restInput}
          onChange={onChange}
          value={value}
        >
          {!skipEmpty && <option value="" />}
          {options.map((o): React.ReactNode => (
            <option value={o.id} key={o.id}>{o.title}</option>
          ))}
        </NativeSelect>
        {Boolean(meta.touched && meta.error) && (
          <FormHelperText className="b-input__helper-text" error>{meta.error}</FormHelperText>
        )}
      </FormControl>
    );
  }
}

export default FormFieldSelect;
