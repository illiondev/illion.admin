import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

class ProductActions extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <div className="b-product__dialog-actions">
        <Button onClick={this.props.onClose} color="default" className="m-margin_left-auto m-margin_right-normal">
          Закрыть
        </Button>

        {!isReadOnly && (
          <Button onClick={this.props.onSave} color="primary" variant="contained">
            Сохранить
          </Button>
        )}
      </div>
    );
  }
}

export default ProductActions;
