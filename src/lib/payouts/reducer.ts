import { Pager, Payout } from 'types';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { StatisticsAllActions } from './actions';

export interface PayoutsState {
  readonly entries: Payout[];
  readonly pager: Pager;
}

const initialState: PayoutsState = {
  entries: [],
  pager: {} as Pager,
};

const payouts = (state = initialState, action: StatisticsAllActions): PayoutsState => {
  switch (action.type) {
    case types.PAYOUTS_READ:
      return {
        ...state,
        entries: action.payload.entries,
        pager: action.payload.pager,
      };

    case types.PAYOUT_CREATE:
      return {
        ...state,
        entries: [action.payload, ...state.entries],
      };

    case types.PAYOUT_UPDATE:
      return {
        ...state,
        entries: state.entries.map((e: Payout): Payout => (e.id !== action.payload.id ? e : action.payload)),
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default payouts;
