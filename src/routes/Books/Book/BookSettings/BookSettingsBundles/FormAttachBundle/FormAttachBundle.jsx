import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

import { requiredId } from 'utils/validators';

import FormFieldAutoComplete from 'components/FormFieldAutoComplete';

class FormAttachBundle extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number,
      product_id: PropTypes.number,
      quantity: PropTypes.number,
      price: PropTypes.number,
      title: PropTypes.string,
      sold_at: PropTypes.string,
      comment: PropTypes.string,
      is_paid: PropTypes.bool,
    }),
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onBundleAutocomplete: PropTypes.func.isRequired,
  };

  static defaultProps = {
    entry: {},
  };

  form = null;

  handleSubmit = ({ bundle }) => {
    this.props.onSave(bundle.id);
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  render() {
    const { open } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="sm" fullWidth>
        <DialogTitle>Добавление комплекта</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Field
                      autocompleteHandler={this.props.onBundleAutocomplete}
                      label="Название комплекта*"
                      name="bundle"
                      component={FormFieldAutoComplete}
                      validate={requiredId}
                    />
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Добавить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormAttachBundle;
