export const API_SERVER = process.env.API_SERVER;
export const EDITOR_KEY = process.env.EDITOR_KEY;
export const SENTRY_DSN = process.env.SENTRY_DSN;
export const APP_ENV = process.env.APP_ENV;
export const PUBLIC_URL = process.env.PUBLIC_URL;
export const RELEASE = process.env.RELEASE;
export const SHIPTOR_ORDER_VIEW_URL = 'https://shiptor.ru/account/package/view';
