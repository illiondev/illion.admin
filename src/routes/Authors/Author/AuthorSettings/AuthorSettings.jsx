import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';
import FormFieldFile from 'components/FormFieldFile';
import FormFieldEditor from 'components/FormFieldEditor';
import router from 'services/RouterService';
import FormAuthorUser from 'routes/Authors/Author/AuthorSettings/FormAuthorUser';

class AuthorSettings extends Component {
  static propTypes = {
    isNew: PropTypes.bool,
    isReadOnly: PropTypes.bool.isRequired,
    entry: PropTypes.shape({
      name: PropTypes.string,
      surname: PropTypes.string,
      photo: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        })
      }),
      user: PropTypes.shape({}),
    }),
    onSubmit: PropTypes.func.isRequired,
    onDeleteUser: PropTypes.func.isRequired,
    onAddUser: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isNew: false,
    entry: {
      id: 'new',
      name: '',
      surname: '',
      about: '',
    },
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.entry !== prevState.entryProp) {
      return {
        modalIsOpen: false,
        entryProp: nextProps.entry,
      };
    }

    return null;
  }

  state = {
    modalIsOpen: false,
    entryProp: undefined,
  };

  handleAuthorUser = () => {
    const { entry } = this.props;

    if (entry.user) {
      if (window.confirm('Отвязать пользователя ?')) this.props.onDeleteUser(entry.id);
    } else {
      this.setState({ modalIsOpen: true });
    }
  };

  handleAddAuthorUser = (email) => {
    const { entry } = this.props;

    this.props.onAddUser(entry.id, email);
  };

  handleCloseModal = () => this.setState({ modalIsOpen: false });

  handleSubmit = (formData) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { user, photo, ...data } = formData;

    if (!photo.data) {
      data.photo = photo;
    }

    this.props.onSubmit(data);
  };

  render() {
    const { isNew, entry, isReadOnly } = this.props;
    const { modalIsOpen } = this.state;

    return (
      <div className="b-page__content b-page__content_size_small">
        <Paper className="b-paper">
          <Typography variant="h4" align="center">
            {isNew ? 'Новый автор' : 'Настройки автора'}
          </Typography>

          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={entry}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={6}>
                    <Field
                      type="text"
                      name="name"
                      label="Имя"
                      fullWidth
                      component={FormFieldText}
                      validate={required}
                      disabled={isReadOnly}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      type="text"
                      name="surname"
                      label="Фамилия"
                      fullWidth
                      component={FormFieldText}
                      validate={required}
                      disabled={isReadOnly}
                    />
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <Field
                      type="text"
                      name="instagram"
                      label="Instagram"
                      fullWidth
                      component={FormFieldText}
                      validate={required}
                      disabled={isReadOnly}
                    />

                    {!isNew && (
                      <div className="m-margin_top-normal">
                        {Boolean(entry.user) && (
                          <Typography variant="body1" gutterBottom>{entry.user.data.email}</Typography>
                        )}

                        <Button
                          type="button"
                          variant="contained"
                          color="primary"

                          onClick={this.handleAuthorUser}
                        >
                          {!entry.user ? 'Привязать пользователя' : 'Отвязать пользователя'}
                        </Button>
                      </div>
                    )}
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <Field
                      name="photo"
                      label="Фото"
                      component={FormFieldFile}
                      validate={required}
                      disabled={isReadOnly}
                      isImageFile
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography variant="caption" gutterBottom>Об авторе</Typography>
                    <Field
                      name="about"
                      component={FormFieldEditor}
                      options={{
                        plugins: 'code lists textcolor colorpicker',
                        height: 300,
                      }}
                      validate={required}
                      disabled={isReadOnly}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Grid container spacing={24}>
                      <Grid item>
                        <Button
                          color="default"
                          component={Link}
                          to={router.url.AUTHORS()}
                        >
                          Назад
                        </Button>
                      </Grid>

                      {!isReadOnly && (
                        <Grid item className="m-margin_left-auto">
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"

                          >
                            {isNew ? 'Создать' : 'Сохранить'}
                          </Button>
                        </Grid>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </Paper>

        <FormAuthorUser
          open={modalIsOpen}
          onClose={this.handleCloseModal}
          onSave={this.handleAddAuthorUser}
        />
      </div>
    );
  }
}

export default AuthorSettings;
