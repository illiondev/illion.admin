import { connect } from 'react-redux';

import {
  getOrder, orderUpdate, orderPay, orderBillCreate
} from 'lib/orders/actions';
import { getOrdersById, getCustomerFormById } from 'lib/orders/selectors';

import Order from './Order';

function mapStateToProps(state, ownProps) {
  return {
    entry: getOrdersById(state)[ownProps.match.params.id],
    customerForm: getCustomerFormById(state)[ownProps.match.params.id]
  };
}

export default connect(mapStateToProps, {
  getOrder,
  orderUpdate,
  orderPay,
  orderBillCreate,
})(Order);
