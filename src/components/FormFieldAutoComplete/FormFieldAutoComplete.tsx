import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import AutoComplete from 'components/AutoComplete';
import { AutoCompleteProps } from 'components/AutoComplete/AutoComplete';

export interface FormFieldAutoCompleteProps extends FieldRenderProps<any> {
  label: string;
}

class FormFieldAutoComplete extends Component<FormFieldAutoCompleteProps & AutoCompleteProps> {
  public render(): React.ReactNode {
    const {
      input,
      meta,
      ...rest
    } = this.props;

    return (
      <AutoComplete
        {...rest}
        current={input.value || undefined}
        name={input.name}
        error={meta.touched ? meta.error : undefined}
        onChoose={(value: any): void => input.onChange(value)}
      />
    );
  }
}

export default FormFieldAutoComplete;
