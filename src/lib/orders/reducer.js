import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  entries: [],
  pager: {},
  query: {},
  exportUrl: '',
};

const orders = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.ORDERS_LIST:
      return {
        ...state,
        entries: payload.entries,
        pager: payload.pager,
        query: payload.query,
      };

    case types.ORDER_ITEM:
      return {
        ...state,
        entries: [...state.entries, payload],
      };

    case types.ORDER_BILL_CREATE:
      return {
        ...state,
        entries: state.entries.map(e => (e.id !== payload.order_id ? e : ({
          ...e,
          bills: {
            data: [payload, ...e.bills.data]
          },
        }))),
      };

    case types.ORDER_UPDATED:
      return {
        ...state,
        entries: state.entries.map(e => (e.id !== payload.id ? e : payload)),
      };

    case types.ORDER_EXPORT_URL:
      return {
        ...state,
        exportUrl: payload,
      };

    case types.ORDER_EXPORT_URL_DROP:
      return {
        ...state,
        exportUrl: '',
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default orders;
