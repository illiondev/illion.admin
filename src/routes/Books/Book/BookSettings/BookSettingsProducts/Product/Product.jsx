import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import router from 'services/RouterService';

import ProductSettings from './ProductSettings';
import ProductTypeBook from './ProductTypeBook';
import ProductTypeEpub from './ProductTypeEpub';
import ProductTypeVideo from './ProductTypeVideo';

class Product extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
        productId: PropTypes.string,
        type: PropTypes.string,
      }).isRequired,
    }).isRequired,
    product: PropTypes.shape({
      id: PropTypes.number,
      type: PropTypes.number,
      enabled: PropTypes.bool,
      pre_order: PropTypes.bool,
    }),
    isReadOnlyRoute: PropTypes.bool.isRequired,
    bookProductCreate: PropTypes.func.isRequired,
    bookProductUpdate: PropTypes.func.isRequired,
    bookFileDelete: PropTypes.func.isRequired,
    bookEpubPreviewDelete: PropTypes.func.isRequired,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const newType = Number(nextProps.match.params.type);

    // создание продукта
    if (!prevState.product && Number(nextProps.match.params.type)) {
      return {
        product: { type: newType, book_id: Number(nextProps.match.params.id) },
      };
    }

    if (nextProps.product !== prevState.currentProduct) {
      return {
        product: nextProps.product,
        currentProduct: nextProps.product,
      };
    }

    return null;
  }

  state = {
    currentProduct: undefined,
    product: undefined,
  };

  static defaultProps = {
    product: undefined,
  };

  handleDeleteEpub = (type) => {
    const req = {
      book_id: this.state.product.book_id,
      id: this.state.product.id,
      type,
    };

    switch (type) {
      case 'epub':
      case 'pdf':
        this.props.bookFileDelete(req);
        break;
      case 'preview':
        this.props.bookEpubPreviewDelete(req);
        break;
      default:
        break;
    }
  };

  handleCloseProduct = () => router.push(router.url.BOOK_EDIT_PRODUCTS(this.state.product.book_id));

  renderProductType() {
    const { isReadOnlyRoute } = this.props;
    const { product } = this.state;

    switch (product.type) {
      case 1:
        return <ProductTypeBook isReadOnly={isReadOnlyRoute} />;

      case 2:
        return (
          <ProductTypeEpub
            isReadOnly={isReadOnlyRoute}
            onDelete={this.handleDeleteEpub}
          />
        );

      case 3:
        return <ProductTypeVideo isReadOnly={isReadOnlyRoute} />;

      default:
        return null;
    }
  }

  render() {
    const { isReadOnlyRoute } = this.props;
    const { product } = this.state;

    if (!product) {
      return <Typography variant="headline" align="center" color="error">Такого товара не существует</Typography>;
    }

    return (
      <ProductSettings
        product={product}
        isReadOnly={isReadOnlyRoute}
        onClose={this.handleCloseProduct}
        onCreate={this.props.bookProductCreate}
        onUpdate={this.props.bookProductUpdate}
      >
        {this.renderProductType()}
      </ProductSettings>
    );
  }
}

export default Product;
