import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';

import { required, maxLength, composeValidators } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class BookSettingsQuotes extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      text: PropTypes.string,
    })).isRequired,
    bookId: PropTypes.number.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    bookQuotesCreate: PropTypes.func.isRequired,
    bookQuotesUpdate: PropTypes.func.isRequired,
    bookQuotesDelete: PropTypes.func.isRequired,
  }

  state = {
    modalIsOpen: false,
    entry: {},
  }

  handleModalOpen = () => this.setState({ modalIsOpen: true })

  handleModalClose = () => this.setState({ modalIsOpen: false, entry: {} })

  handleEditEntry = entry => this.setState({ entry, modalIsOpen: true })

  handleDeleteEntry = entry => () => this.props.bookQuotesDelete({ id: entry.id, book_id: this.props.bookId })

  handleSubmitForm = (formData) => {
    const { bookId: book_id } = this.props;

    if (formData.id) {
      this.props.bookQuotesUpdate({
        book_id,
        ...formData,
      });
    } else {
      this.props.bookQuotesCreate({
        book_id,
        ...formData,
      });
    }

    this.setState({ modalIsOpen: false, entry: {} });
  }

  render() {
    const { isReadOnly } = this.props;
    const { entry, modalIsOpen } = this.state;

    return (
      <div className="m-width-full">
        <List>
          {this.props.entries.map(e => (
            <ListItem key={e.id} disableGutters button onClick={() => (isReadOnly ? {} : this.handleEditEntry(e))}>
              <ListItemText primary={e.text} secondary={e.name} />
              {!isReadOnly && (
                <ListItemSecondaryAction>
                  <Tooltip title="Удалить" placement="top">
                    <IconButton onClick={this.handleDeleteEntry(e)} color="secondary">
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              )}
            </ListItem>
          ))}
        </List>

        {!isReadOnly && (
          <Button fullWidth onClick={this.handleModalOpen} color="primary">
            Добавить цитату
          </Button>
        )}

        <Dialog onClose={this.handleModalClose} open={modalIsOpen} maxWidth="md">
          <DialogTitle>{entry.id ? 'Редактирование цитаты' : 'Создание цитаты'}</DialogTitle>
          <Form
            onSubmit={this.handleSubmitForm}
            subscription={{ submitting: true, pristine: true }}
            initialValues={entry}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} className="b-book-editor__modal-form">
                <DialogContent>
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <Field
                        name="name"
                        label="Автор цитаты"
                        fullWidth
                        component={FormFieldText}
                        validate={required}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="text"
                        label="Текст цитаты"
                        fullWidth
                        component={FormFieldText}
                        multiline
                        validate={composeValidators(required, maxLength(255))}
                      />
                    </Grid>
                  </Grid>
                </DialogContent>

                <DialogActions>
                  <Button onClick={this.handleModalClose} color="primary">
                    Отмена
                  </Button>
                  <Button type="submit" color="primary">
                    Сохранить
                  </Button>
                </DialogActions>
              </form>
            )}
          />
        </Dialog>
      </div>
    );
  }
}

export default BookSettingsQuotes;
