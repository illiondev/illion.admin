import React, { Component } from 'react';
import { Switch, Redirect } from 'react-router-dom';

import CssBaseline from '@material-ui/core/CssBaseline';

import router from 'services/RouterService';

import LogIn from 'routes/LogIn';
import SignUp from 'routes/SignUp';
import Restore from 'routes/Restore';
import Books from 'routes/Books';
import BookNew from 'routes/Books/BookNew';
import Book from 'routes/Books/Book';
import Content from 'routes/Content';

import Authors from 'routes/Authors';
import AuthorNew from 'routes/Authors/AuthorNew';
import Author from 'routes/Authors/Author';

import Users from 'routes/Users';

import Orders from 'routes/Orders';
import Order from 'routes/Orders/Order';

import Statistics from 'routes/Statistics';
import StatisticsProfit from 'routes/StatisticsProfit';
import Payouts from 'routes/Payouts';
import Logs from 'routes/Logs';
import Vacancies from 'routes/Vacancies';
import Vacancy from 'routes/Vacancies/Vacancy';
import ExternalSales from 'routes/ExternalSales';
import Soon from 'routes/Soon';

import RoutePrivate from 'components/RoutePrivate';
import RoutePublic from 'components/RoutePublic';

import Header from 'components/Header';
import Snackbar from 'components/Snackbar';

import { AppDispatchProps, AppRouterProps, AppStoreProps } from './App.connect';

import './App.scss';

type Props = AppRouterProps & AppStoreProps & AppDispatchProps;

const { path, url } = router;

class App extends Component<Props> {
  public componentDidMount(): void {
    if (this.props.isLogged) this.props.profileMe();
  }

  public componentDidUpdate(prevProps: Props): void {
    if (this.props.isLogged && !prevProps.isLogged) this.props.profileMe();

    // скролл в начало страницы при смене роута
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
    }
  }

  public render(): React.ReactNode {
    const { isLogged } = this.props;

    return (
      <div className="b-page">
        <CssBaseline />
        <Header />
        <Snackbar />

        <Switch>
          <RoutePublic path={path.LOGIN} component={LogIn} isLogged={isLogged} />
          <RoutePublic path={path.SIGNUP} component={SignUp} isLogged={isLogged} />
          <RoutePublic path={path.RESTORE_STATUS} component={Restore} isLogged={isLogged} />

          <RoutePrivate path={path.CONTENT} component={Content} />

          <RoutePrivate path={path.BOOKS} exact component={Books} />
          <RoutePrivate path={path.BOOK_NEW} component={BookNew} />
          <RoutePrivate path={path.BOOK} component={Book} />

          <RoutePrivate path={path.USERS} component={Users} />
          <RoutePrivate path={path.STATISTICS} exact component={Statistics} />
          <RoutePrivate path={path.STATISTICS_PROFIT} exact component={StatisticsProfit} />

          <RoutePrivate path={path.ORDERS} exact component={Orders} />
          <RoutePrivate path={path.ORDER} component={Order} />


          <RoutePrivate path={path.VACANCIES} exact component={Vacancies} />
          <RoutePrivate path={path.VACANCY} component={Vacancy} />

          <RoutePrivate path={path.AUTHORS} exact component={Authors} />
          <RoutePrivate path={path.AUTHOR_NEW} component={AuthorNew} />
          <RoutePrivate path={path.AUTHOR} component={Author} />

          <RoutePrivate path={path.PAYOUTS} component={Payouts} />
          <RoutePrivate path={path.LOGS} component={Logs} />
          <RoutePrivate path={path.EXTERNAL_SALES} component={ExternalSales} />
          <RoutePrivate path={path.SOON} component={Soon} />

          {!isLogged ? <Redirect to={url.LOGIN()} /> : <Redirect to={url.BOOKS()} />}
        </Switch>

      </div>
    );
  }
}

export default App;
