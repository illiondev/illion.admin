import { createBrowserHistory, History } from 'history';

import { ROUTER_PATHS } from 'enums/RouterPathsEnums';

type RouterUrlGenerator = (...args: string[]) => string;

type RouterUrls = { [key in keyof typeof ROUTER_PATHS]: RouterUrlGenerator };

class RouterService {
  public constructor() {
    this.history = createBrowserHistory();
    this.paths = ROUTER_PATHS;
    this.urls = Object.keys(ROUTER_PATHS).reduce(this.setUrlValue, {} as RouterUrls);
  }

  private history: History;

  private paths: typeof ROUTER_PATHS;

  private urls: RouterUrls;

  public get browserHistory(): History {
    return this.history;
  }

  public get path(): typeof ROUTER_PATHS {
    return this.paths;
  }

  public get url(): RouterUrls {
    return this.urls;
  }

  public push(url: string): void {
    this.history.push(url);
  }

  // собираем хеш урлов для навигации на основании ROUTER_PATHS
  // если для урла нужны параметры то значением будет функция, которая принимает параметры
  // если урл не содержит параметров - значение будет строка
  private setUrlValue = (urls: RouterUrls, key: string): RouterUrls => {
    const path: ROUTER_PATHS = this.paths[key as keyof typeof ROUTER_PATHS];
    let url: RouterUrlGenerator = (): ROUTER_PATHS => path;

    if (this.getPathParams(path).length > 0) {
      url = this.pathParse(path);
    }

    return {
      ...urls,
      [key]: url
    };
  };

  private pathParse = (path: string): RouterUrlGenerator => (...values: string[]): string => {
    const params: string[] = this.getPathParams(path).map((p): string => p.replace('/', '')); // убираем лишние слеши
    let url: string = path;

    // нет параметров и нет значений - это обычный роут без параметров
    if (params.length === 0 && values.length === 0) return url;

    // не хватает данных для обязательных параметров
    if (params.filter((p): boolean => !p.includes('?')).length > values.length) return '/ERROR_REQUIRED_PARAMS';

    params.forEach((parameter, index): void => {
      url = url.replace(parameter, values[index]);
    });

    return url;
  };

  private getPathParams(path: string): RegExpMatchArray {
    return path.match(/(:\w*)\??\/?/g) || [];
  }
}

const routerService: RouterService = new RouterService();

export default routerService;
