import authTypes from 'lib/auth/actionTypes';

import { UserProfile } from 'types';
import types from './actionTypes';

import { ProfileAllActions } from './actions';

export interface ProfileState {
  readonly me?: UserProfile;
  readonly isFetching: boolean;
}

const initialState: ProfileState = {
  me: undefined,
  isFetching: false,
};

const profile = (state = initialState, action: ProfileAllActions): ProfileState => {
  switch (action.type) {
    case types.PROFILE_ME:
      return {
        ...state,
        me: action.payload,
        isFetching: false,
      };

    case types.PROFILE_ME_FETCH:
      return {
        ...state,
        isFetching: action.payload,
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default profile;
