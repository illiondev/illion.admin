export const DEVICES = [
  {
    title: 'Galaxy S5',
    id: 'galaxy-s5',
    width: 360,
    height: 640,
  },
  {
    title: 'Pixel 2',
    id: 'pixel-2',
    width: 411,
    height: 731,
  },
  {
    title: 'Pixel XL',
    id: 'pixel-xl',
    width: 411,
    height: 823,
  },
  {
    title: 'iPhone 5/SE',
    id: 'iphone-5',
    width: 320,
    height: 568,
  },
  {
    title: 'iPhone 6/7/8',
    id: 'iphone-6',
    width: 375,
    height: 667,
  },
  {
    title: 'iPhone 6/7/8 Plus',
    id: 'iphone-6-plus',
    width: 414,
    height: 736,
  },
  {
    title: 'iPhone X',
    id: 'iphone-x',
    width: 375,
    height: 812,
  },
  {
    title: 'iPad',
    id: 'ipad',
    width: 768,
    height: 1024,
  },
  {
    title: 'iPad Pro',
    id: 'ipad-pro',
    width: 1024,
    height: 1366,
  },
  {
    title: 'Сброс',
    id: 'default',
  },
];
