import { ITokensData, ITokensStorage } from 'illion-api';
import { Store } from 'redux';

import reduxStore from 'services/StoreService';

import authTypes from 'lib/auth/actionTypes';

const store: Store = reduxStore.get();

class ApiTokensStorage implements ITokensStorage {
  public getTokens(): ITokensData {
    const state = store.getState();

    return state.auth.tokens || {};
  }

  public setTokens(tokens: ITokensData): void {
    store.dispatch({
      type: authTypes.LOG_IN,
      payload: tokens
    });
  }

  public clearTokens(): void {
    store.dispatch({
      type: authTypes.LOG_OUT,
    });
  }
}

export default ApiTokensStorage;
