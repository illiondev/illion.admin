import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  entries: [],
  pager: {},
};

const users = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.USER_LIST:
      return {
        ...state,
        entries: payload.entries,
        pager: payload.pager,
      };

    case types.USER_MODERATOR_UPDATE:
      return {
        ...state,
        entries: state.entries.filter(e => e.id !== payload),
      };

    case types.USER_MODERATOR_CREATE:
      return {
        ...state,
        entries: [payload, ...state.entries],
      };

    case types.USER_UPDATE:
      return {
        ...state,
        entries: state.entries.map(e => (e.id !== payload.id ? e : payload))
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default users;
