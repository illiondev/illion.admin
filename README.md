### Установка

`yarn`

### Настройка
Перед запуском необходимо создать файл `.env` и прописать в нём актуальные настройки приложения

- `API_SERVER` - адресс api-сервера
- `EDITOR_KEY` - ключ tinymce редактора
- `SENTRY_DSN` - ключ для Sentry
- `APP_ENV` - Окружение в котором работает приложение (dev|state|production)
- `PUBLIC_URL` - адресс публичной части приложения

### Команды
`yarn start` - запуск в режиме разработки  
`yarn run compile` - сборка приложения  


### Доступы к разделам сайта

##### Добавление нового уровня доступа
1. Добавить уровень в `PermissionsEnums.js/PERMISSION_LIST`
2. Добавить роуты для уровня в `PermissionsEnums.js/PERMISSION_PATHS`
3. Добавить пункт меню в `MenuEnums.js/MENU`


### Настройка трекера Sentry
Необходимо наличие ключа `SENTRY_DSN`

1. `cp .sentryclirc.example .sentryclirc`
2. Заменить `SENTRY_API_TOKEN` на актуальное значение
3. Для синхронизации с коммитами в git, после сборки бандла, необходимо выполнить команды (все изменения должны быть запушены в репозиторий):
```
# выставляем имя для релиза (хеш последнего коммита)
VERSION=$(./node_modules/.bin/sentry-cli releases propose-version)

# читаем текущее значение APP_ENV из файла `.env`
APP_ENV=$(grep -oP 'APP_ENV=\K.*' .env)

# связываем коммиты с релизом
./node_modules/.bin/sentry-cli releases set-commits --auto $VERSION

# создаем деплой для релиза
./node_modules/.bin/sentry-cli releases deploys $VERSION new -e $APP_ENV
```


### Локальный запуск сервера
```
  yarn compile   
  node server.js  
```
