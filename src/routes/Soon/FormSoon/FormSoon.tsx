import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

import { getAuthorName } from 'utils';
import { required, requiredId } from 'utils/validators';

import { SoonBook, SoonBookReq } from 'types';

import FormFieldAutoComplete from 'components/FormFieldAutoComplete';
import FormFieldText from 'components/FormFieldText';
import FormFieldFile from 'components/FormFieldFile';

interface Props {
  entry: SoonBook;
  open: boolean;
  onClose(): void;
  onSave(data: Partial<SoonBookReq>): void;
  onDelete(id: number): void;
  onAuthorAutocomplete(query: string): void;
}

class FormSoon extends Component<Props> {
  public static defaultProps = {
    entry: {},
  };

  private form = React.createRef<HTMLFormElement>();

  private handleSubmit = (formData: object): void => {
    const { cover, author, ...restData } = formData as SoonBookReq;
    const data = {
      ...restData,
      author_id: author.id,
    } as Partial<SoonBookReq>;

    // @ts-ignore
    if (!cover.data) {
      data.cover = cover;
    }

    // новые записи создаются не активными
    if (!data.id) {
      data.enabled = false;
    }

    this.props.onSave(data);
  };

  private handleSave = (): void => {
    if (this.form.current) {
      this.form.current.dispatchEvent(new Event('submit', { cancelable: true }));
    }
  };

  private handleDelete = (): void => {
    if (this.props.entry && window.confirm('Удалить?')) {
      this.props.onDelete(this.props.entry.id);
    }
  };

  public render(): React.ReactNode {
    const { open, entry } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="sm">
        <DialogTitle>{Boolean(entry) && entry.id ? `Редактирование книги #${entry.id}` : 'Новая книга'}</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={{
              ...entry,
              author: entry.author ? {
                id: entry.author.data.id,
                title: getAuthorName(entry.author),
              } : entry.author,
            }}
            render={({ handleSubmit }): React.ReactNode => (
              <form onSubmit={handleSubmit} ref={this.form}>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Field
                      name="title"
                      label="Название книги*"
                      component={FormFieldText}
                      fullWidth
                      validate={required}
                    />
                  </Grid>
                  <Grid item xs={12} md={7}>
                    <Field
                      autocompleteHandler={this.props.onAuthorAutocomplete}
                      label="Автор*"
                      name="author"
                      component={FormFieldAutoComplete as any}
                      validate={requiredId}
                    />
                  </Grid>
                  <Grid item xs={12} md={5}>
                    <Field
                      name="cover"
                      label="Обложка*"
                      component={FormFieldFile as any}
                      validate={required}
                      isImageFile
                    />
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          {Boolean(entry.id) && (
            <Button onClick={this.handleDelete} color="secondary" variant="outlined">
              Удалить
            </Button>
          )}

          <Button onClick={this.props.onClose} color="default" className="m-margin_left-auto m-margin_right-normal">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Сохранить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormSoon;
