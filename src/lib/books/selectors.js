import { createSelector } from 'reselect';

export const getBooks = state => state.books.entries;
export const getCurrentBook = state => state.books.entry;
export const getBooksById = createSelector(getBooks, getCurrentBook, (entries, entry) => {
  const booksById = entries.reduce((byId, book) => ({
    ...byId,
    [book.id]: book,
  }), {});

  if (entry) {
    booksById[entry.id] = entry;
  }

  return booksById;
});

export const getBooksProductsById = createSelector(getBooks, getCurrentBook, (entries, entry) => {
  const allBooks = entry ? [...entries, entry] : entries;

  return allBooks.reduce((all, book) => ({
    ...all,
    ...book.products.data.reduce((byId, product) => ({ ...byId, [product.id]: product }), {})
  }), {});
});

export const getBooksBundlesById = createSelector(getBooks, getCurrentBook, (entries, entry) => {
  const allBooks = entry ? [...entries, entry] : entries;

  return allBooks.reduce((all, book) => ({
    ...all,
    ...book.bundles.data.reduce((byId, bundle) => ({ ...byId, [bundle.id]: bundle }), {})
  }), {});
});
