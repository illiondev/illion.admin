import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import indigo from '@material-ui/core/colors/indigo';

import { BOOK_TYPES_TITLE_BY_ID, BOOK_TYPES_TITLE_NEW_BY_ID, BOOK_TYPES_ICON_BY_ID } from 'enums/BookTypesEnum';

import FormFieldText from 'components/FormFieldText';
import { required } from 'utils/validators';

import ProductActions from '../ProductActions';

class ProductSettings extends Component {
  static propTypes = {
    product: PropTypes.shape({
      id: PropTypes.number,
      book_id: PropTypes.number,
      enabled: PropTypes.bool,
      pre_order: PropTypes.bool,
      cover: PropTypes.shape({
        data: PropTypes.shape({
          id: PropTypes.number.isRequired,
          url: PropTypes.string.isRequired,
        }),
      }),
      epub: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.string]),
      pdf: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.string]),
      epub_preview: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.string]),
    }),
    children: PropTypes.node.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
  };

  static defaultProps = {
    product: undefined,
  };

  form = null;

  handleSubmit = ({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    coverSmall, includes, cover, epub, epub_preview, pdf, ...formData
  }) => {
    const { product } = this.props;

    const data = {
      ...formData,
      type: product.type,
      book_id: product.book_id,
      epub: epub && !epub.data ? epub : undefined,
      pdf: pdf && !pdf.data ? pdf : undefined,
      epub_preview: epub_preview && !epub_preview.data ? epub_preview : undefined,
    };

    if (product.id) {
      data.id = product.id;
      this.props.onUpdate(data);
    } else {
      this.props.onCreate(data);
    }
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  render() {
    const { product = {}, isReadOnly } = this.props;
    const initialValues = product;

    if (initialValues.epub && !initialValues.epub.data) initialValues.epub = { data: { url: initialValues.epub } };
    if (initialValues.pdf && !initialValues.pdf.data) initialValues.pdf = { data: { url: initialValues.pdf } };
    if (initialValues.epub_preview && !initialValues.epub_preview.data) {
      initialValues.epub_preview = { data: { url: initialValues.epub_preview } };
    }

    return (
      <Fragment>
        <div>
          <div className="b-product__dialog-title">
            <span className="b-product__dialog-title">
              <Avatar style={{ backgroundColor: indigo.A100 }} className="m-margin_right-normal">
                {BOOK_TYPES_ICON_BY_ID[product.type]}
              </Avatar>
              <Typography component="span" variant="title">
                {product.id ? BOOK_TYPES_TITLE_BY_ID[product.type] : BOOK_TYPES_TITLE_NEW_BY_ID[product.type]}
                {product.id && ` #${product.id}`}
              </Typography>
            </span>
          </div>
          <div>
            <Form
              onSubmit={this.handleSubmit}
              subscription={{ submitting: true, pristine: true }}
              initialValues={initialValues}
              render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                  <Grid container spacing={24}>
                    <Grid item xs={12} md={2}>
                      <Typography variant="caption" gutterBottom>
                        Себестоимость:
                      </Typography>
                      <Field
                        name="prime_cost"
                        type="number"
                        label="RUB*"
                        fullWidth
                        component={FormFieldText}
                        validate={required}
                        disabled={isReadOnly}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={24} className="m-margin_top-normal">
                    {this.props.children}
                  </Grid>
                </form>
              )}
            />
          </div>

          <ProductActions
            isReadOnly={isReadOnly}
            onClose={this.props.onClose}
            onSave={this.handleSave}
          />
        </div>
      </Fragment>
    );
  }
}

export default ProductSettings;
