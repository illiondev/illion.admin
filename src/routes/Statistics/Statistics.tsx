import React, { Component, Fragment } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { formatPrice } from 'utils';
import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';

import { StatsBundle } from 'types/stats';

import { StatisticsDispatchProps, StatisticsStoreProps } from './Statistics.connect';

import StatisticsFilter from './StatisticsFilter';


class Statistics extends Component<StatisticsStoreProps & StatisticsDispatchProps> {
  public static defaultProps = {
    summary: undefined,
  };

  public componentDidMount(): void {
    this.props.getStatistics();
  }

  public render(): React.ReactNode {
    const { entries, total, summary } = this.props;

    return (
      <div>
        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Статистика продаж</Typography>

          <StatisticsFilter onSubmit={this.props.getStatistics} />

          {Boolean(summary) && (
            <Fragment>
              <Typography variant="h6" className="b-page__paper-title">Общая сводка</Typography>
              <ListItem className="m-margin_left-small">
                <ListItemText
                  primary={<b>{summary ? formatPrice(summary.delivery) : ''}</b>}
                  secondary="Затраты на доставку"
                />
              </ListItem>
            </Fragment>
          )}

          {entries.length > 0 && (
            <Fragment>
              <Typography variant="h6" className="b-page__paper-title">По продуктам</Typography>
              <div className="b-table-responsive">
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Наименование</TableCell>
                      <TableCell align="right">Заказано</TableCell>
                      <TableCell align="right">Оплачено</TableCell>
                      <TableCell align="right">Не оплачено</TableCell>
                      <TableCell align="right">Возвратов</TableCell>
                      <TableCell align="right">Оплачено анонимно (шт.)</TableCell>
                      <TableCell align="right">Внешние продажи</TableCell>
                      <TableCell align="right">Доставка</TableCell>
                      <TableCell align="right">Сумма (сайт)</TableCell>
                      <TableCell align="right">Сумма (все)</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {entries.map((e: StatsBundle): React.ReactNode => (
                      <TableRow key={e.bundle_id} hover>
                        <TableCell component="th" scope="row">
                          {`${e.title} (${BOOK_TYPES_TITLE_BY_ID[e.type]})`}
                        </TableCell>
                        <TableCell align="right">{e.ordered}</TableCell>
                        <TableCell align="right">{e.paid}</TableCell>
                        <TableCell align="right">{e.not_paid}</TableCell>
                        <TableCell align="right">{e.returned}</TableCell>
                        <TableCell align="right">{e.bills_sales}</TableCell>
                        <TableCell align="right">{formatPrice(e.external_sales_sum)}</TableCell>
                        <TableCell align="right">{formatPrice(e.delivery_sum)}</TableCell>
                        <TableCell align="right">{formatPrice(e.sum + e.bills_sales_sum)}</TableCell>
                        <TableCell align="right">
                          {formatPrice(e.sum + e.bills_sales_sum + e.external_sales_sum + e.delivery_sum)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                  <TableFooter>
                    <TableRow>
                      <TableCell />
                      <TableCell align="right"><b>{total.ordered}</b></TableCell>
                      <TableCell align="right"><b>{total.paid}</b></TableCell>
                      <TableCell align="right"><b>{total.not_paid}</b></TableCell>
                      <TableCell align="right"><b>{total.returned}</b></TableCell>
                      <TableCell align="right"><b>{total.bills_sales}</b></TableCell>
                      <TableCell align="right"><b>{formatPrice(total.external_sales_sum)}</b></TableCell>
                      <TableCell align="right"><b>{formatPrice(total.delivery_sum)}</b></TableCell>
                      <TableCell align="right"><b>{formatPrice(total.sum + total.bills_sales_sum)}</b></TableCell>
                      <TableCell align="right">
                        <b>
                          {/* eslint-disable-next-line max-len */}
                          {formatPrice(total.sum + total.bills_sales_sum + total.external_sales_sum + total.delivery_sum)}
                        </b>
                      </TableCell>
                    </TableRow>
                  </TableFooter>
                </Table>
              </div>
            </Fragment>
          )}
        </Paper>
      </div>
    );
  }
}

export default Statistics;
