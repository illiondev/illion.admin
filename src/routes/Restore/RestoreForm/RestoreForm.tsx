import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';

import { ForgotReq } from 'types';

import { composeValidators, emailFormat, required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

interface IRestoreFormProps {
  onSave(req: ForgotReq): void;
}

class RestoreForm extends Component<IRestoreFormProps> {
  private handleSubmit = (values: object): void => {
    this.props.onSave(values as ForgotReq);
  };

  public render(): React.ReactNode {
    return (
      <Form
        onSubmit={this.handleSubmit}
        subscription={{ submitting: true, pristine: true }}
        initialValues={{ remember_me: false }}
        render={({ handleSubmit }): React.ReactNode => (
          <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <Field
                label="Email"
                name="email"
                type="email"
                autoFocus
                fullWidth
                component={FormFieldText}
                validate={composeValidators(required, emailFormat)}
              />
            </FormControl>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className="b-auth__form-submit"
            >
              Далее
            </Button>
          </form>
        )}
      />
    );
  }
}

export default RestoreForm;
