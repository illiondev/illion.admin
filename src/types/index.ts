import { BOOK_TYPES } from 'enums/BookTypesEnum';

export interface LoginReq {
  email: string;
  password: string;
  remember_me: boolean;
}

export interface RegisterReq {
  email: string;
  password: string;
  name: string;
}

export interface ForgotReq {
  email: string;
}

export interface ListRes<T> {
  entries: T[];
  pager: Pager;
}

export interface SelectOption {
  id: number|string;
  title: string;
}

export interface ProtectedRoute {
  readonly isReadOnlyRoute: boolean;
}

export interface Pager {
  readonly total_pages: number;
  readonly total: number;
  readonly per_page: number;
  readonly current_page: number;
}

export interface PagerReq {
  readonly page: number;
  readonly per_page?: number;
}

export interface AutocompleteEntry {
  readonly id: number|string;
  readonly title: string;
}

export interface MenuItem {
  readonly title: string;
  readonly url: string;
  readonly submenu?: SubmenuItem[];
}

export interface SubmenuItem {
  readonly title: string;
  readonly url: string;
}

export type MouseEventCallback = (event: React.MouseEvent<HTMLElement>) => void;

export interface Permission {
  name: string;
}

export interface UserProfile {
  readonly id: number;
  readonly name: string;
  readonly email: string;
  readonly permissions: {
    data: Permission[];
  };
}

export interface Vacancy {
  readonly id: number;
  readonly priority: number;
  readonly title: string;
  readonly description: string;
  readonly enabled: boolean;
}

export type VacanciesById = Record<number, Vacancy>;

export interface Author {
  readonly id: number;
  readonly name: string;
  readonly surname: string;
}

export interface Cover {
  readonly url: string;
}

export interface SoonBook {
  readonly id: number;
  readonly title: string;
  readonly enabled: boolean;
  readonly author: {
    data: Author;
  };
  readonly cover: {
    data: Cover;
  };
}

export interface SoonBookReq extends Omit<SoonBook, 'author'|'cover'> {
  readonly author: {
    readonly id: number;
  };
  cover: File | {
    readonly url?: string;
    readonly data?: Cover;
  };
  enabled: boolean;
}

export interface SoonCoverReq {
  readonly id: number;
  readonly category?: string;
  readonly image: File;
}

export interface Payout {
  readonly id: number;
  readonly amount: number;
  readonly moderator_email: string;
  readonly payout_date: string;
  readonly created_at: string;
  readonly author: {
    data: Author;
  };
}

export interface PayoutReq {
  id?: number;
  readonly amount: number;
  readonly author_id: string;
  readonly payout_date: string;
}

export interface EmulatedEvent {
  target: {
    name: string;
    value: string;
  };
}

export interface ExternalSale {
  readonly id: number;
  readonly bundle_id: number;
  readonly type: BOOK_TYPES;
  readonly quantity: number;
  readonly is_paid: boolean;
  readonly price: number;
  readonly title: string;
  readonly moderator_email: string;
  readonly sold_at: string;
  readonly comment: string;
  readonly commission_flat: string;
  readonly commission: string;
  readonly author_fee: string;
}

export interface ExternalSalesFilterQuery {
  bundle_id?: number;
  page?: number;
}
