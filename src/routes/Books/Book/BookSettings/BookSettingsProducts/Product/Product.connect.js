import { connect } from 'react-redux';

import {
  bookProductCreate, bookProductUpdate,
  bookFileDelete, bookEpubPreviewDelete
} from 'lib/books/actions';

import { getBooksProductsById } from 'lib/books/selectors';
import Product from './Product';

function mapStateToProps(state, ownProps) {
  return {
    product: getBooksProductsById(state)[ownProps.match.params.productId],
  };
}

export default connect(mapStateToProps, {
  bookProductCreate,
  bookProductUpdate,
  bookFileDelete,
  bookEpubPreviewDelete,
})(Product);
