const path = require('path');
const express = require('express');
const serverStatic = require('serve-static');

const app = express();

app.disable('x-powered-by');
app.enable('trust proxy');
app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, 'templates'));
app.use(serverStatic('dist'));

app.use((req, res) => {
  res.status(200).render('index');
});

app.listen(8080, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info(
      '==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.',
      8080,
      8080,
    );
  }
});
