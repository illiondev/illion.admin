import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import TextField from '@material-ui/core/TextField';

import './FormFieldText.scss';

class FormFieldText extends Component<FieldRenderProps<HTMLElement>> {
  public render(): React.ReactNode {
    const {
      input: {
        name, onChange, value, ...restInput
      },
      meta,
      ...rest
    } = this.props;

    return (
      <TextField
        {...rest}
        name={name}
        helperText={meta.touched ? meta.error : undefined}
        error={meta.error && meta.touched}
        inputProps={{ ...restInput }}
        onChange={onChange}
        value={value}
        FormHelperTextProps={{ className: 'b-input__helper-text' }}
      />
    );
  }
}

export default FormFieldText;
