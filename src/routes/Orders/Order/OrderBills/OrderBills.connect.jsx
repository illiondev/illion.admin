import { connect } from 'react-redux';

import { snackbarShow } from 'lib/app/snackbar/actions';
import { orderBillCreate } from 'lib/orders/actions';
import { getOrdersBillsById } from 'lib/orders/selectors';

import OrderBills from './OrderBills';

function mapStateToProps(state, ownProps) {
  return {
    entries: getOrdersBillsById(state)[ownProps.orderId]
  };
}

export default connect(mapStateToProps, {
  orderBillCreate,
  snackbarShow,
})(OrderBills);
