import React, { Component } from 'react';

import SnackbarMUI from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { SNACKBAR_TYPE_ICONS } from 'enums/SnakbarTypesEnums';

import { SnackbarDispatchProps, SnackbarStoreProps } from './Snackbar.connect';

import './Snackbar.scss';

type Props = SnackbarStoreProps & SnackbarDispatchProps;

interface State {
  isOpen: boolean;
  message: string;
}

class Snackbar extends Component<Props, State> {
  public static getDerivedStateFromProps(nextProps: Props, prevState: State): State|null {
    if (nextProps.message !== prevState.message) {
      return {
        isOpen: Boolean(nextProps.message),
        message: nextProps.message,
      };
    }

    return null;
  }

  public state = {
    isOpen: false,
    message: '',
  };

  private handleClose = (): void => {
    this.setState({ isOpen: false });
  };

  private handleClearMessage = (): void => {
    this.props.snackbarHide();
  };

  public render(): React.ReactNode {
    const { message, type } = this.props;
    const Icon = SNACKBAR_TYPE_ICONS[type];

    return (
      <SnackbarMUI
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        open={this.state.isOpen}
        autoHideDuration={7000}
        onClose={this.handleClose}
        onExited={this.handleClearMessage}
      >
        <SnackbarContent
          className={`b-snackbar b-snackbar_type_${type}`}
          message={(
            <span className="b-snackbar__content">
              {Boolean(Icon) && <Icon className="b-snackbar__icon" />}
              {message}
            </span>
          )}
          action={[
            <IconButton
              key="close"
              color="inherit"
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </SnackbarMUI>
    );
  }
}

export default Snackbar;
