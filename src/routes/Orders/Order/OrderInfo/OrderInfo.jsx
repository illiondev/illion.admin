import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconError from '@material-ui/icons/Error';

import { formatPrice } from 'utils';
import { getBundleType } from 'utils/bundles';

import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';
import { SHIPPING_CATEGORIES_TITLE_BY_ID } from 'enums/OrderEnums';

import OrderBills from '../OrderBills';

class OrderInfo extends PureComponent {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number.isRequired,
      shiptor_id: PropTypes.number,
      shiptor_error: PropTypes.string,
      tracking_number: PropTypes.string,
      payment_id: PropTypes.string,
      status: PropTypes.number.isRequired,
      shipping_method: PropTypes.number,
      shipping_category: PropTypes.string,
      price: PropTypes.number.isRequired,
      delivery_price: PropTypes.number,
      total_price: PropTypes.number.isRequired,
      created_at: PropTypes.string.isRequired,
      surname: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      patronimic: PropTypes.string,
      email: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired,
      postal_code: PropTypes.string,
      country: PropTypes.string.isRequired,
      region: PropTypes.string,
      settlement: PropTypes.string,
      address: PropTypes.string,
      address2: PropTypes.string,
      system_comment: PropTypes.string,
    }).isRequired,
    isReadOnly: PropTypes.bool.isRequired,
  };


  render() {
    const { entry, isReadOnly } = this.props;
    const fullAddress = [
      entry.postal_code, entry.country, entry.region, entry.settlement, entry.address, entry.address2
    ].filter(v => v).join(', ');

    return (
      <Grid container spacing={24} className="m-margin_bottom-medium">
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <Typography variant="h6">
            Содержимое заказа
          </Typography>

          <ul className="m-margin_bottom-medium">
            {entry.items.data.map(b => (
              <li key={b.bundle_id}>
                <Typography>
                  {`
                    ${b.title}(${BOOK_TYPES_TITLE_BY_ID[getBundleType(b.products.data)]}) - ${b.quantity} шт.
                    - ${b.price > 0 ? formatPrice(b.price) : 'Бесплатно'}
                  `}
                  {b.discount > 0 && b.price > 0 && (
                    <Typography variant="caption">
                      {`Старая цена: ${formatPrice(b.original_price)}. Скидка ${b.discount}%`}
                    </Typography>
                  )}
                </Typography>
              </li>
            ))}
          </ul>

          <Typography variant="h6" gutterBottom>
            Сумма
          </Typography>
          <Typography gutterBottom>
            {`Заказ: ${formatPrice(entry.price)}`}
            <br />
            {`Доставка: ${formatPrice(entry.delivery_price)}`}
            <br />
            {`Всего: ${formatPrice(entry.total_price)}`}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={6} md={5} lg={6}>
          <Typography variant="h6" gutterBottom>
            Информация по заказу
          </Typography>

          <Grid container>
            <Grid item md={12} lg={6}>
              <Typography variant="subtitle1" gutterBottom>
                Данные покупателя
              </Typography>

              <Typography gutterBottom>
                <Typography variant="caption">
                  ФИО
                </Typography>
                {`${entry.surname} ${entry.name} ${entry.patronimic}`}
              </Typography>

              <Typography gutterBottom>
                <Typography variant="caption">
                  Контакты
                </Typography>
                {`Телефон: ${entry.phone}`}
                <br />
                {`Email: ${entry.email}`}
              </Typography>

              <Typography gutterBottom>
                <Typography variant="caption">
                  Адресс доставки
                </Typography>
                {fullAddress}
              </Typography>
            </Grid>

            <Grid item md={12} lg={6}>
              <Typography variant="subtitle1" gutterBottom>
                Прочее
              </Typography>

              <Typography gutterBottom>
                <Typography variant="caption">
                  Создан
                </Typography>
                {new Date(entry.created_at).toLocaleString()}
              </Typography>

              {Boolean(entry.payment_id) && (
                <Typography gutterBottom>
                  <Typography variant="caption">
                    YandexId
                  </Typography>
                  {entry.payment_id}
                </Typography>
              )}

              <Typography gutterBottom>
                <Typography variant="caption">
                  Способ доставки
                </Typography>
                {`${SHIPPING_CATEGORIES_TITLE_BY_ID[entry.shipping_category]}(ID: ${entry.shipping_method})`}
              </Typography>

              {Boolean(entry.shiptor_id || entry.shiptor_error) && (
                <Typography gutterBottom>
                  <Typography variant="caption">
                    Shiptor
                  </Typography>
                  {Boolean(entry.shiptor_error) && (
                    <Typography color="error">
                      <IconError className="b-orders__icon-error" />
                      &nbsp;
                      {entry.shiptor_error}
                    </Typography>
                  )}
                  {Boolean(entry.shiptor_id) && (
                    <Typography>
                      ID:&nbsp;
                      {entry.shiptor_id}
                    </Typography>
                  )}
                  {Boolean(entry.tracking_number) && (
                    <Typography>
                      Tracking:&nbsp;
                      <a
                        href={`https://gdeposylka.ru/courier/shiptor/tracking/${entry.tracking_number}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {entry.tracking_number}
                      </a>
                    </Typography>
                  )}
                </Typography>
              )}

              {Boolean(entry.comment) && (
                <Typography gutterBottom>
                  <Typography variant="caption">
                    Комментарий к заказу
                  </Typography>
                  {entry.comment}
                </Typography>
              )}

              {Boolean(entry.system_comment) && (
                <Typography gutterBottom>
                  <Typography variant="caption">
                    Дополнительная информация
                  </Typography>
                  {entry.system_comment}
                </Typography>
              )}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <Typography variant="h6" gutterBottom>
            Счета на оплату
          </Typography>
          <OrderBills isReadOnly={isReadOnly} orderId={entry.id} />
        </Grid>
      </Grid>
    );
  }
}

export default OrderInfo;
