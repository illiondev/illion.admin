import React, { PureComponent } from 'react';
// @ts-ignore
import Flatpickr from 'react-flatpickr';
// @ts-ignore
import cx from 'classnames';

import 'flatpickr/dist/themes/material_blue.css';

import { EmulatedEvent } from 'types';

import TextField, { TextFieldProps } from '@material-ui/core/TextField';

import './DatePicker.scss';

interface Props {
  name: string;
  type: string;
  value: string;
  className: string;
  onChange(e: EmulatedEvent): void;
  inputProps: {
    min?: string;
    max?: string;
  };
}

function isBrowserSupportDateInput(): boolean {
  const input: HTMLInputElement = document.createElement('input');

  input.setAttribute('type', 'date');

  return input.type !== 'text';
}

const IS_BROWSER_SUPPORT_DATE_INPUT: boolean = isBrowserSupportDateInput();

class DatePicker extends PureComponent<Props & TextFieldProps> {
  public static defaultProps = {
    className: '',
    inputProps: {},
  };

  private handleChange = (selectedDates: string, dateStr: string): void => {
    this.props.onChange({
      target: {
        name: this.props.name,
        value: dateStr
      }
    });
  };

  public render(): React.ReactNode {
    const {
      name, value, inputProps, type
    } = this.props;
    const { className, ...fieldProps } = this.props;

    return (
      <div className={cx('b-datepicker__wrapper', className)}>
        <TextField
          {...fieldProps}
          className="b-datepicker__native"
          FormHelperTextProps={{ className: 'b-input__helper-text' }}
        />
        {
          (!IS_BROWSER_SUPPORT_DATE_INPUT || type === 'datetime-local') && (
            <Flatpickr
              className={cx('b-datepicker', { 'b-datepicker_state_filled': value })}
              name={name}
              value={value}
              onChange={this.handleChange}
              options={{
                minDate: inputProps.min,
                maxDate: inputProps.max,
                enableTime: type === 'datetime-local',
              }}
            />
          )}
      </div>
    );
  }
}

export default DatePicker;
