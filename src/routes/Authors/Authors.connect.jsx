import { connect } from 'react-redux';

import { getAuthorList } from 'lib/authors/actions';

import Authors from './Authors';

function mapStateToProps(state) {
  return {
    entries: state.authors.entries,
    pager: state.authors.pager,
  };
}

export default connect(mapStateToProps, {
  getAuthorList
})(Authors);
