import { Author } from 'types';
import { BOOK_TYPES } from 'enums/BookTypesEnum';

export interface StatsBundle {
  readonly title: string;
  readonly bundle_id: number;
  readonly type: BOOK_TYPES;
  readonly ordered: number;
  readonly paid: number;
  readonly not_paid: number;
  readonly returned: number;
  readonly external_sales_sum: number;
  readonly sum: number;
  readonly bills_sales: number;
  readonly bills_sales_sum: number;
  readonly delivery_sum: number;
}

export interface StatsBundleTotal {
  readonly ordered: number;
  readonly paid: number;
  readonly not_paid: number;
  readonly returned: number;
  readonly external_sales_sum: number;
  readonly sum: number;
  readonly bills_sales: number;
  readonly bills_sales_sum: number;
  readonly delivery_sum: number;
}

export interface StatsSummary {
  readonly delivery: number;
}

export interface StatsFilter {
  date_from: string;
  date_to: string;
}

export interface StatsProfit {
  readonly author_id: number;
  readonly authors_profit: number;
  readonly house_profit: number;
  readonly author: {
    data: Author;
  };
}

export interface StatsProfitTotal {
  readonly authors_profit: number;
  readonly house_profit: number;
}
