import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import SettingsIcon from '@material-ui/icons/Settings';
import LinkIcon from '@material-ui/icons/Link';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';

import router from 'services/RouterService';

const LINKS = [
  { id: 'cart', text: 'Добавить в корзину' },
  { id: 'bill', text: 'Анонимная покупка' },
];

class BundleCard extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      enabled: PropTypes.bool.isRequired,
      title: PropTypes.string,
      products: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.number.isRequired,
          type: PropTypes.number.isRequired,
          title: PropTypes.string.isRequired,
          is_surprise: PropTypes.bool.isRequired,
        })).isRequired,
      }).isRequired,
    }).isRequired,
    bookId: PropTypes.number.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    onCopyLink: PropTypes.func.isRequired,
    onToggleEnabled: PropTypes.func.isRequired,
  };

  state = {
    anchorEl: null,
  };

  handleToggleEnabled = () => this.props.onToggleEnabled(this.props.entry);

  handleLinkChoose = link => () => {
    // если вначале не закрыть меню, копирование не срабатывает
    this.setState({ anchorEl: null }, () => {
      this.props.onCopyLink(this.props.entry, link.id);
    });
  };

  handleOpenLinkMenu = (e) => {
    this.setState({ anchorEl: e.currentTarget });
  };

  handleCloseLinkMenu = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { isReadOnly, entry, bookId } = this.props;
    const { anchorEl } = this.state;

    return (
      <Card>
        <CardHeader
          action={(
            <Fragment>
              {entry.hidden && (
                <Tooltip title="Скрытый комплект" placement="top">
                  <span>
                    <IconButton disabled>
                      <VisibilityOffIcon color="disabled" />
                    </IconButton>
                  </span>
                </Tooltip>
              )}

              {entry.enabled && !entry.hidden && (
                <Fragment>
                  <Tooltip title="Ссылки на бандл" placement="top">
                    <IconButton onClick={this.handleOpenLinkMenu}>
                      <LinkIcon color="primary" />
                    </IconButton>
                  </Tooltip>
                  <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleCloseLinkMenu}
                    disableAutoFocusItem
                  >
                    {LINKS.map(option => (
                      <MenuItem key={option.id} onClick={this.handleLinkChoose(option)}>
                        {option.text}
                      </MenuItem>
                    ))}
                  </Menu>
                </Fragment>
              )}
            </Fragment>
          )}
          title={entry.title || ''}
          titleTypographyProps={{ variant: 'title' }}
          classes={{ content: 'b-product-card__title' }}
        />

        {entry.products.data.length > 0 && (
          <CardContent>
            <Typography variant="body2">
              Содержимое:
            </Typography>
            {entry.products.data.map(p => (
              <Typography key={p.id} variant="caption" noWrap>
                {`[${BOOK_TYPES_TITLE_BY_ID[p.type]}] - ${p.is_surprise ? `Бонус: ${p.title}` : p.title}`}
              </Typography>
            ))}
          </CardContent>
        )}

        <CardActions disableActionSpacing>
          {!isReadOnly && (
            <FormControlLabel
              control={(
                <Switch
                  name="enabled"
                  onChange={this.handleToggleEnabled}
                  checked={entry.enabled}
                  color="secondary"
                />
              )}
              label="Активен"
            />
          )}

          <Tooltip title="Настройки" placement="top">
            <IconButton
              className="m-margin_left-auto"
              component={Link}
              to={router.url.BOOK_EDIT_BUNDLE(bookId, entry.id)}
            >
              <SettingsIcon />
            </IconButton>
          </Tooltip>
        </CardActions>
      </Card>
    );
  }
}

export default BundleCard;
