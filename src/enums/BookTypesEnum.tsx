import React from 'react';

import BookIcon from '@material-ui/icons/Book';
import EpubIcon from '@material-ui/icons/ChromeReaderMode';
import VideoIcon from '@material-ui/icons/OndemandVideo';
import CourseIcon from '@material-ui/icons/VideoLibrary';

export enum BOOK_TYPES {
  PAPER = 1,
  EPUB = 2,
  VIDEO = 3,
  COURSE = 4,
}

export const BOOK_TYPES_LIST: BookType[] = [
  {
    id: BOOK_TYPES.PAPER,
    title: 'Книга',
    title_new: 'Новая книга',
    icon: <BookIcon />,
  },
  {
    id: BOOK_TYPES.EPUB,
    title: 'ePub',
    title_new: 'Новый ePub',
    icon: <EpubIcon />,
  },
  {
    id: BOOK_TYPES.VIDEO,
    title: 'Видео',
    title_new: 'Новое видео',
    icon: <VideoIcon />,
  },
  {
    id: BOOK_TYPES.COURSE,
    title: 'Курс',
    title_new: 'Новый курс',
    icon: <CourseIcon />,
  },
];

interface BookType {
  id: BOOK_TYPES;
  title: string;
  title_new: string;
  icon: React.ReactElement;
}

type BookTypesTitleById = { [key in BOOK_TYPES]: string };
type BookTypesIconById = { [key in BOOK_TYPES]: React.ReactElement };

export const BOOK_TYPES_TITLE_BY_ID: BookTypesTitleById = BOOK_TYPES_LIST
  .reduce(
    (byId: BookTypesTitleById, type: BookType): BookTypesTitleById => ({ ...byId, [type.id]: type.title }),
    {} as BookTypesTitleById
  );

export const BOOK_TYPES_TITLE_NEW_BY_ID: BookTypesTitleById = BOOK_TYPES_LIST
  .reduce(
    (byId: BookTypesTitleById, type: BookType): BookTypesTitleById => ({ ...byId, [type.id]: type.title_new }),
    {} as BookTypesTitleById
  );

export const BOOK_TYPES_ICON_BY_ID: BookTypesIconById = BOOK_TYPES_LIST
  .reduce(
    (byId: BookTypesIconById, type: BookType): BookTypesIconById => ({ ...byId, [type.id]: type.icon }),
    {} as BookTypesIconById
  );
