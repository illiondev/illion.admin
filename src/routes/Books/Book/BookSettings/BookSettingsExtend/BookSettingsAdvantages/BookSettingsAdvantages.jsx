import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';

import { required, maxLength, composeValidators } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';
import FormFieldFile from 'components/FormFieldFile';


class BookSettingsAdvantages extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      text: PropTypes.string,
      image: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        }),
      }),
    })).isRequired,
    bookId: PropTypes.number.isRequired,
    bookAdvantagesCreate: PropTypes.func.isRequired,
    bookAdvantagesUpdate: PropTypes.func.isRequired,
    bookAdvantagesDelete: PropTypes.func.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
  }

  state = {
    modalIsOpen: false,
    entry: {},
  }

  handleModalOpen = () => this.setState({ modalIsOpen: true })

  handleModalClose = () => this.setState({ modalIsOpen: false, entry: {} })

  handleEditEntry = entry => this.setState({ entry, modalIsOpen: true })

  handleDeleteEntry = entry => () => this.props.bookAdvantagesDelete({ id: entry.id, book_id: this.props.bookId })

  handleSubmitForm = ({ image, ...formData }) => {
    const { bookId: book_id } = this.props;
    const data = {
      book_id,
      ...formData,
    };

    // если есть поле data, то в image лежит родное значение, полученное с сервера
    if (image && !image.data) {
      data.image = image;
    }

    if (data.id) {
      this.props.bookAdvantagesUpdate(data);
    } else {
      this.props.bookAdvantagesCreate(data);
    }

    this.setState({ modalIsOpen: false, entry: {} });
  }

  render() {
    const { isReadOnly } = this.props;
    const { entry, modalIsOpen } = this.state;

    return (
      <div className="m-width-full">
        <List>
          {this.props.entries.map(e => (
            <ListItem key={e.id} disableGutters button onClick={() => (isReadOnly ? {} : this.handleEditEntry(e))}>
              {Boolean(e.image) && (
                <ListItemAvatar>
                  <Avatar src={e.image.data.url} />
                </ListItemAvatar>
              )}
              <ListItemText primary={e.text} />
              {!isReadOnly && (
                <ListItemSecondaryAction>
                  <Tooltip title="Удалить" placement="top">
                    <IconButton onClick={this.handleDeleteEntry(e)} color="secondary">
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              )}
            </ListItem>
          ))}
        </List>

        {!isReadOnly && (
          <Button fullWidth onClick={this.handleModalOpen} color="primary">
            Добавить преимущество
          </Button>
        )}

        <Dialog onClose={this.handleModalClose} open={modalIsOpen} maxWidth="md">
          <DialogTitle>{entry.id ? 'Редактирование преимущества' : 'Создание преимущество'}</DialogTitle>
          <Form
            onSubmit={this.handleSubmitForm}
            subscription={{ submitting: true, pristine: true }}
            initialValues={entry}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} className="b-book-editor__modal-form">
                <DialogContent>
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <Field
                        name="text"
                        label="Текст преимущества"
                        fullWidth
                        component={FormFieldText}
                        multiline
                        validate={composeValidators(required, maxLength(255))}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="image"
                        label="Изображение"
                        component={FormFieldFile}
                        isImageFile
                      />
                    </Grid>
                  </Grid>
                </DialogContent>

                <DialogActions>
                  <Button onClick={this.handleModalClose} color="primary">
                    Отмена
                  </Button>
                  <Button type="submit" color="primary">
                    Сохранить
                  </Button>
                </DialogActions>
              </form>
            )}
          />
        </Dialog>
      </div>
    );
  }
}

export default BookSettingsAdvantages;
