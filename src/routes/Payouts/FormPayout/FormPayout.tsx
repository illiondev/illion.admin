import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

import { getAuthorName, getCurrentDate } from 'utils';
import { required, requiredId } from 'utils/validators';

import { Payout, PayoutReq } from 'types';

import FormFieldAutoComplete from 'components/FormFieldAutoComplete';
import FormFieldText from 'components/FormFieldText';
import FormFieldDate from 'components/FormFieldDate';

interface Props {
  entry: Payout;
  open: boolean;
  onClose(): void;
  onSave(data: PayoutReq): void;
  onAuthorAutoComplete(query: string): void;
}

class FormPayout extends Component<Props> {
  public static defaultProps = {
    entry: {},
  };

  private form = React.createRef<HTMLFormElement>();

  private handleSubmit = (formData: object): void => {
    const {
      amount, author, payout_date, id
    } = formData as Payout;

    const data = {
      // @ts-ignore
      author_id: author.id,
      amount: Number(amount),
      payout_date,
    } as PayoutReq;

    if (id) {
      data.id = id;
    }

    this.props.onSave(data);
    this.props.onClose();
  };

  private handleSave = (): void => {
    if (this.form.current) {
      this.form.current.dispatchEvent(new Event('submit', { cancelable: true }));
    }
  };

  public render(): React.ReactNode {
    const { open, entry } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="md">
        <DialogTitle>{entry.id ? `Редактирование выплаты #${entry.id}` : 'Новая выплата'}</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={{
              payout_at: getCurrentDate(),
              ...entry,
              author: entry.author ? {
                id: entry.author.data.id,
                title: getAuthorName(entry.author),
              } : entry.author,
            }}
            render={({ handleSubmit }): React.ReactNode => (
              <form onSubmit={handleSubmit} ref={this.form}>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Field
                      autocompleteHandler={this.props.onAuthorAutoComplete}
                      label="Автор*"
                      name="author"
                      component={FormFieldAutoComplete as any}
                      validate={requiredId}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      name="amount"
                      label="Сумма*"
                      type="number"
                      component={FormFieldText}
                      validate={required}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      name="payout_date"
                      label="Дата Выплаты*"
                      fullWidth
                      type="date"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      inputProps={{
                        max: getCurrentDate(),
                      }}
                      component={FormFieldDate}
                      validate={required}
                    />
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            Сохранить
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormPayout;
