import { connect } from 'react-redux';

import {
  bookBundleUpdate, bookBundleCreate,
  bookBundleCoverDelete, bookBundleDetach
} from 'lib/books/actions';

import { getBooksBundlesById } from 'lib/books/selectors';

import Bundle from './Bundle';

function mapStateToProps(state, ownProps) {
  return {
    bundle: getBooksBundlesById(state)[ownProps.match.params.bundleId],
  };
}

export default connect(mapStateToProps, {
  bookBundleCreate,
  bookBundleUpdate,
  bookBundleCoverDelete,
  bookBundleDetach,
})(Bundle);
