import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Book } from 'epubjs';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import apiService from 'services/ApiService';

import ViewerDevices from 'components/ViewerDevices';

import './BookChapterPreview.scss';

class BookChapterPreview extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
        chapter: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }

  state = {
    defaultSize: null
  }

  viewer = null

  rendition = null

  componentDidMount() {
    const { match: { params: { id: book_id, chapter: id } } } = this.props;
    const defaultSize = this.getDefaultViewerSize();
    const book = new Book({
      requestMethod: () => apiService.bookChapterPreview({ id, book_id }).then(res => new Uint8Array(res.data))
    });

    book.open('contentUrl', 'epub').then(() => {
      const { width, height } = defaultSize;
      this.rendition = book.renderTo(this.viewer, { flow: 'paginated', width, height });

      this.rendition.display();
    });

    this.setState({ defaultSize });
  }


  getDefaultViewerSize() {
    return {
      width: this.viewer.parentElement.offsetWidth,
      height: '100%',
    };
  }

  handlePrevPage = () => this.rendition.prev();

  handleNextPage = () => this.rendition.next();

  handleChangeSize = ({ width, height }) => this.rendition.resize(width, height)

  render() {
    const { defaultSize } = this.state;

    return (
      <Paper className="b-paper b-chapter-preview">
        <div className="b-chapter-preview__actions">
          <div className="b-chapter-preview__pager">
            <Button color="secondary" onClick={this.handlePrevPage}>
              <ArrowBackIcon />
            </Button>
            <Button color="secondary" onClick={this.handleNextPage}>
              <ArrowForwardIcon />
            </Button>
          </div>

          <div className="b-chapter-preview__dimensions">
            <ViewerDevices onChange={this.handleChangeSize} defaultSize={defaultSize} />
          </div>
        </div>

        <div className="b-chapter-preview__content-wrapper">
          <div ref={(node) => { this.viewer = node; }} className="b-chapter-preview__content" />
        </div>
      </Paper>
    );
  }
}

export default BookChapterPreview;
