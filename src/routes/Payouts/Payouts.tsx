import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import { formatPrice, getAuthorName } from 'utils';
import { Payout, PayoutReq, ProtectedRoute } from 'types';

import FormPayout from './FormPayout';
import { PayoutsDispatchProps, PayoutsStoreProps } from './Payouts.connect';

type Props = PayoutsStoreProps & PayoutsDispatchProps & ProtectedRoute;

interface State {
  modalIsOpen: boolean;
  entry?: Payout;
}

class Payouts extends Component<Props, State> {
  public state: State = {
    modalIsOpen: false,
    entry: undefined,
  };

  public componentDidMount(): void {
    this.props.payoutsRead();
  }

  private handleChangePage = (e: any, page: number): void => {
    this.props.payoutsRead({ page: page + 1 });
  };

  private handleModalClose = (): void => this.setState({ modalIsOpen: false, entry: undefined });

  private handleEditEntry = (entry: Payout): void => this.setState({ entry, modalIsOpen: true });

  private handleAuthorAutoComplete = (query: string): void => {
    this.props.authorAutocomplete(query);
  };

  private handleCreateEntry = (): void => {
    this.setState({
      entry: {} as Payout,
      modalIsOpen: true
    });
  };

  private handleUpdateEntry = (data: PayoutReq): void => {
    if (data.id) {
      this.props.payoutUpdate(data);
    } else {
      this.props.payoutCreate(data);
    }
  };

  public render(): React.ReactNode {
    const { entries, pager, isReadOnlyRoute } = this.props;
    const { modalIsOpen, entry } = this.state;

    return (
      <div className="b-page__content">
        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Выплаты авторам</Typography>

          <div className="b-table-responsive">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Автор</TableCell>
                  <TableCell>Добавил</TableCell>
                  <TableCell align="right">Cумма</TableCell>
                  <TableCell align="right">Дата выплаты</TableCell>
                  <TableCell align="right">Дата создания</TableCell>
                  <TableCell align="right" />
                </TableRow>
              </TableHead>
              <TableBody>
                {entries.map((e: Payout): React.ReactNode => (
                  <TableRow key={e.id} hover>
                    <TableCell component="th" scope="row">
                      {e.id}
                    </TableCell>
                    <TableCell>{getAuthorName(e.author)}</TableCell>
                    <TableCell>{e.moderator_email}</TableCell>
                    <TableCell align="right">{formatPrice(e.amount)}</TableCell>
                    <TableCell align="right">{new Date(e.payout_date).toLocaleDateString()}</TableCell>
                    <TableCell align="right">{new Date(e.created_at).toLocaleDateString()}</TableCell>
                    <TableCell align="right">
                      {!isReadOnlyRoute && (
                        <Tooltip title="Редактировать" placement="top">
                          <IconButton onClick={(): void => this.handleEditEntry(e)}>
                            <EditIcon />
                          </IconButton>
                        </Tooltip>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>

          {pager.total_pages > 1 && (
            <TablePagination
              component="div"
              count={pager.total}
              rowsPerPage={pager.per_page}
              rowsPerPageOptions={[pager.per_page]}
              page={pager.current_page - 1}
              onChangePage={this.handleChangePage}
            />
          )}
        </Paper>

        <FormPayout
          onClose={this.handleModalClose}
          onSave={this.handleUpdateEntry}
          onAuthorAutoComplete={this.handleAuthorAutoComplete}
          open={modalIsOpen}
          entry={entry}
        />

        {!isReadOnlyRoute && (
          <Tooltip title="Добавить выплату" placement="top">
            <Fab
              color="primary"
              className="b-page__actions"
              onClick={this.handleCreateEntry}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}
      </div>
    );
  }
}

export default Payouts;
