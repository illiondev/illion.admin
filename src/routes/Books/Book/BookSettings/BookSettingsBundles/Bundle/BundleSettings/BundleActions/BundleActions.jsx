import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

class BundleActions extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onDelete: PropTypes.func,
    onClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    onDelete: undefined,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Grid container spacing={24} className="m-margin_top-normal">
        {!isReadOnly && this.props.onDelete && (
          <Grid item xs={12} sm="auto">
            <Button onClick={this.props.onDelete} color="secondary" variant="outlined">
              Удалить комплект из книги
            </Button>
          </Grid>
        )}

        <Grid item className="m-margin_left-auto">
          <Button onClick={this.props.onClose} color="default">
            Закрыть
          </Button>
        </Grid>

        {!isReadOnly && (
          <Grid item>
            <Button onClick={this.props.onSave} color="primary" variant="contained">
              Сохранить
            </Button>
          </Grid>
        )}
      </Grid>
    );
  }
}

export default BundleActions;
