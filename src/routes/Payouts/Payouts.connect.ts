import { connect } from 'react-redux';

import { RootState } from 'lib';
import { Pager, Payout } from 'types';

import { authorAutocomplete } from 'lib/authors/actions';
import { payoutsRead, payoutCreate, payoutUpdate } from 'lib/payouts/actions';

import Payouts from './Payouts';

export interface PayoutsStoreProps {
  entries: Payout[];
  pager: Pager;
}

export interface PayoutsDispatchProps {
  payoutsRead: typeof payoutsRead;
  payoutCreate: typeof payoutCreate;
  payoutUpdate: typeof payoutUpdate;
  authorAutocomplete: typeof authorAutocomplete;
}

function mapStateToProps(state: RootState): PayoutsStoreProps {
  return {
    entries: state.payouts.entries,
    pager: state.payouts.pager,
  };
}

export default connect<PayoutsStoreProps, PayoutsDispatchProps, {}, RootState>(mapStateToProps, {
  payoutsRead,
  payoutCreate,
  payoutUpdate,
  authorAutocomplete,
})(
  // @ts-ignore
  Payouts
);
