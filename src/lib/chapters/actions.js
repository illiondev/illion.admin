import api from 'services/ApiService';
import router from 'services/RouterService';

import { chapterDraftDelete } from 'lib/chapterDraft/actions';

import * as types from './actionTypes';

export function bookChapterInit(payload) {
  return {
    type: types.CHAPTER_INIT,
    payload
  };
}


function bookChapterCreateSuccess(payload) {
  return {
    type: types.CHAPTER_CREATE,
    payload,
  };
}

export function bookChapterCreate(data) {
  return dispatch => api.bookChapterCreate(data).then((res) => {
    const entry = res.data.data;

    dispatch(bookChapterCreateSuccess({ entry }));
    router.push(router.url.BOOK_CHAPTER(entry.book_id, entry.id));
  });
}

function bookChapterUpdateSuccess(payload) {
  return {
    type: types.CHAPTER_UPDATE,
    payload,
  };
}

export function bookChapterUpdate(data) {
  return dispatch => api.bookChapterUpdate(data).then((res) => {
    const entry = res.data.data;

    dispatch(bookChapterUpdateSuccess({ entry }));
    dispatch(chapterDraftDelete(entry.id));
  });
}

function bookChapterDeleteSuccess(payload) {
  return {
    type: types.CHAPTER_DELETE,
    payload,
  };
}

export function bookChapterDelete(data) {
  return dispatch => api.bookChapterDelete(data).then(() => {
    dispatch(bookChapterDeleteSuccess({ entry: data }));
    dispatch(chapterDraftDelete(data.id));

    router.push(router.url.BOOK_EDIT(data.book_id));
  });
}
