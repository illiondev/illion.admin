import {
  createStore, applyMiddleware, compose, Store, Middleware, Reducer, DeepPartial
} from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { SENTRY_DSN } from 'config';
import { sentryReporter } from 'utils/sentry';
import rootReducer from 'lib';
import { Persistor, PersistConfig } from 'redux-persist/es/types';

const persistConfig: PersistConfig = {
  key: 'hoi.admin',
  storage,
  whitelist: ['auth']
};

class StoreService {
  public constructor() {
    this.store = this.createStore();
    this.persistor = new Promise((resolve): Persistor => persistStore(this.store, undefined, (): any => resolve()));
  }

  private store: Store;

  private persistor: Promise<Persistor>;

  private createStore(): Store {
    /* eslint-disable no-underscore-dangle */
    const persistedReducer: Reducer = persistReducer(persistConfig, rootReducer);
    // @ts-ignore
    const __REDUX_DEVTOOLS_EXTENSION__: Function = window.__REDUX_DEVTOOLS_EXTENSION__;
    const initialState: DeepPartial<any> = {};
    const middlewares: Middleware[] = [thunk];

    if (SENTRY_DSN) {
      middlewares.push(sentryReporter);
    }

    return createStore(
      persistedReducer,
      initialState,
      compose(
        applyMiddleware(...middlewares),
        (__REDUX_DEVTOOLS_EXTENSION__ && process.env.APP_ENV !== 'production')
          // @ts-ignore
          ? __REDUX_DEVTOOLS_EXTENSION__() : (f): any => f
      )
    );
    /* eslint-enable */
  }

  public get(): Store {
    return this.store;
  }

  public getPersistor(): Promise<Persistor> {
    return this.persistor;
  }
}

const store = new StoreService();

export default store;
