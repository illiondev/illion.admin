import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

export interface Props extends FieldRenderProps<any> {
  label: string;
  className?: string;
}

class FormFieldSwitch extends Component<Props> {
  public static defaultProps = {
    className: '',
  };

  public render(): React.ReactNode {
    const {
      className,
      input: {
        name, onChange, value, ...restInput
      },
      label,
      ...rest
    } = this.props;

    return (
      <FormControlLabel
        className={className}
        control={(
          <Switch
            {...rest}
            name={name}
            inputProps={restInput}
            onChange={onChange}
            checked={Boolean(value)}
            value={Boolean(value)}
          />
        )}
        label={label}
      />
    );
  }
}

export default FormFieldSwitch;
