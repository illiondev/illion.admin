import { createSelector } from 'reselect';

export const getChapters = state => state.chapters.entries;
export const getBookChaptersById = createSelector(
  getChapters,
  entries => entries.reduce((byId, chapter, index) => ({
    ...byId,
    [chapter.id]: {
      ...chapter,
      counter: index + 1 // что бы корректно отображать номер главы в редакторе
    },
  }), {})
);
