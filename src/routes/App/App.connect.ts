import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { RootState } from 'lib';

import { profileMe } from 'lib/profile/actions';

import App from './App';

export interface AppRouterProps extends RouteComponentProps {}

export interface AppStoreProps {
  isLogged: boolean;
}

export interface AppDispatchProps {
  profileMe: typeof profileMe;
}

function mapStateToProps(state: RootState): AppStoreProps {
  return {
    isLogged: Boolean(state.auth.tokens),
  };
}

export default withRouter<AppRouterProps>(
  connect<AppStoreProps, AppDispatchProps, {}, RootState>(mapStateToProps, { profileMe })(
    // @ts-ignore
    App
  )
);
