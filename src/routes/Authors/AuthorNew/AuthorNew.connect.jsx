import { connect } from 'react-redux';

import { authorCreate } from 'lib/authors/actions';

import AuthorNew from './AuthorNew';

export default connect(null, {
  authorCreate
})(AuthorNew);
