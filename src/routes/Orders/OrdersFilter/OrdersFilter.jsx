import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import Grid from '@material-ui/core/Grid';
import GridIcon from '@material-ui/icons/GridOn';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import { getCurrentDate } from 'utils';
import { PAYMENT_STATUS_LIST } from 'enums/PaymentEnums';
import { SHIPTOR_STATUS_LIST } from 'enums/OrderEnums';
import DatePicker from 'components/DatePicker';

import './OrdersFilter.scss';

class OrdersFilter extends Component {
  static propTypes = {
    isActive: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onExport: PropTypes.func.isRequired,
    current: PropTypes.shape({
      id: PropTypes.number,
      email: PropTypes.string,
      date_from: PropTypes.string,
      date_to: PropTypes.string,
      fio: PropTypes.string,
      status: PropTypes.number,
      price_from: PropTypes.number,
      price_to: PropTypes.number,
      shiptor: PropTypes.number,
      bundle_id: PropTypes.number,
      only_with_failed_orders: PropTypes.bool,
    }),
    bundles: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
    })).isRequired,
  };

  static defaultProps = {
    current: {},
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.current !== prevState.prevFilter) {
      return {
        currentFilter: {
          ...OrdersFilter.initValues,
          ...nextProps.current,
        },
        prevFilter: nextProps.current,
      };
    }

    return null;
  }

  static initValues = {
    id: '',
    email: '',
    status: '',
    price_from: '',
    price_to: '',
    date_from: '',
    date_to: getCurrentDate(true),
    fio: '',
    shiptor: '',
    bundle_id: '',
    only_with_failed_orders: false,
  };

  state = {
    currentFilter: { ...OrdersFilter.initValues },
    prevFilter: {},
  };

  componentDidMount() {
    window.addEventListener('keyup', this.handleKeyUp);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.handleKeyUp);
  }

  getFilterParams(currentFilter) {
    let params = Object.keys(currentFilter).reduce((mem, key) => {
      let val = currentFilter[key];

      if (val) {
        if (['id', 'status', 'price_from', 'price_to', 'shiptor'].includes(key)) {
          val = Number(val);
        }

        if (['date_from', 'date_to'].includes(key)) {
          val = val.replace('T', ' ');
        }

        if (key === 'only_with_failed_orders') {
          val = Number(val);
        }

        return {
          ...mem,
          [key]: val,
        };
      }

      return mem;
    }, {});

    if (Object.keys(params).length === 0) {
      params = null;
    }

    return params;
  }

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  };

  handleChange = (e) => {
    const {
      name, value, type, checked
    } = e.target;

    this.setState(prevState => ({
      ...prevState,
      currentFilter: {
        ...prevState.currentFilter,
        [name]: type === 'checkbox' ? Boolean(checked) : value,
      }
    }));
  };

  handleSubmit = () => {
    this.props.onSubmit(this.getFilterParams(this.state.currentFilter));
  };

  handleExport = () => {
    this.props.onExport(this.getFilterParams(this.state.currentFilter));
  };

  handleReset = () => {
    this.setState({
      currentFilter: { ...OrdersFilter.initValues },
    });

    if (this.props.isActive) {
      this.props.onSubmit();
    }
  };

  render() {
    const { bundles } = this.props;
    const {
      id, email, status, price_from, price_to, date_from, date_to, fio, shiptor, bundle_id, only_with_failed_orders
    } = this.state.currentFilter;
    const hasValues = [
      id, email, status, price_from, price_to,
      date_from, date_to, fio, shiptor, bundle_id,
      only_with_failed_orders
    ].some(v => Boolean(v));

    return (
      <Grid container spacing={24} className="b-paper">
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <TextField
            type="number"
            name="id"
            label="ID"
            onChange={this.handleChange}
            value={id}
            className="b-orders-filter__number"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <TextField
            name="fio"
            label="ФИО"
            onChange={this.handleChange}
            value={fio}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <TextField
            type="email"
            name="email"
            label="Email"
            onChange={this.handleChange}
            value={email}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <TextField
            type="number"
            name="price_from"
            label="Мин. сумма"
            onChange={this.handleChange}
            value={price_from}
            className="b-orders-filter__number"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <TextField
            type="number"
            name="price_to"
            label="Макс. сумма"
            onChange={this.handleChange}
            value={price_to}
            className="b-orders-filter__number"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <DatePicker
            name="date_from"
            value={date_from}
            onChange={this.handleChange}
            label="C"
            type="datetime-local"
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
            inputProps={{
              max: date_to || getCurrentDate(true),
            }}
            className="b-orders-filter__date"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <DatePicker
            name="date_to"
            value={date_to}
            onChange={this.handleChange}
            label="По"
            type="datetime-local"
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
            inputProps={{
              min: date_from,
              max: getCurrentDate(true),
            }}
            className="b-orders-filter__date"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <FormControl fullWidth disabled={Boolean(only_with_failed_orders)}>
            <InputLabel>Статус заказа</InputLabel>
            <NativeSelect
              name="status"
              onChange={this.handleChange}
              value={status}
            >
              <option value="" />
              {PAYMENT_STATUS_LIST.map(o => (
                <option value={o.id} key={o.id}>{o.title}</option>
              ))}
            </NativeSelect>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg="auto">
          <FormControl fullWidth>
            <InputLabel>Shiptor</InputLabel>
            <NativeSelect
              name="shiptor"
              onChange={this.handleChange}
              value={shiptor}
            >
              <option value="" />
              {SHIPTOR_STATUS_LIST.map(o => (
                <option value={o.id} key={o.id}>{o.title}</option>
              ))}
            </NativeSelect>
          </FormControl>
        </Grid>

        {bundles.length > 0 && (
          <Grid item xs={12} sm={6} md={3} lg={2}>
            <FormControl fullWidth>
              <InputLabel>Комплект</InputLabel>
              <NativeSelect
                name="bundle_id"
                onChange={this.handleChange}
                value={bundle_id}
              >
                <option value="" />
                {bundles.map(o => (
                  <option value={o.id} key={o.id}>{o.title}</option>
                ))}
              </NativeSelect>
            </FormControl>
          </Grid>
        )}

        <Grid item xs={12} sm={6} md={3} lg="auto">
          <FormControlLabel
            disabled={!bundle_id || Boolean(status)}
            control={(
              <Switch
                name="only_with_failed_orders"
                onChange={this.handleChange}
                checked={Boolean(only_with_failed_orders)}
                value={only_with_failed_orders}
              />
            )}
            label="Ни одной покупки"
          />
        </Grid>

        <Grid item xs={12} lg="auto">
          {(this.props.isActive || hasValues) && (
            <Button
              variant="outlined"
              color="secondary"
              onClick={this.handleReset}
            >
              Сбросить
            </Button>
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
            disabled={!hasValues}
            className="m-margin_left-normal"
          >
            Искать
          </Button>
          <Button
            color="default"
            onClick={this.handleExport}
            className="m-margin_left-normal"
          >
            Экспорт в таблицу&nbsp;&nbsp;
            <GridIcon />
          </Button>
        </Grid>
      </Grid>
    );
  }
}

export default OrdersFilter;
