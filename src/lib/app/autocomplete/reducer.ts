import { AutocompleteEntry } from 'types';

import types from './actionTypes';
import { AppAutocompleteAllActions } from './actions';

export interface AppAutocompleteState {
  readonly entries: AutocompleteEntry[];
  readonly isFetching: boolean;
  readonly type: string;
}

const initialState: AppAutocompleteState = {
  isFetching: false,
  entries: [],
  type: '',
};

const autocomplete = (state = initialState, action: AppAutocompleteAllActions): AppAutocompleteState => {
  switch (action.type) {
    case types.APP_AUTOCOMPLETE_RESULT:
      return {
        ...state,
        entries: action.payload,
        isFetching: false,
      };

    case types.APP_AUTOCOMPLETE_FETCH:
      return {
        ...state,
        isFetching: action.payload,
        entries: [],
      };

    case types.APP_AUTOCOMPLETE_SET_TYPE:
      return {
        ...state,
        type: action.payload,
      };

    case types.APP_AUTOCOMPLETE_DROP:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default autocomplete;
