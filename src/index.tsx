import React, { ComponentType } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { hot } from 'react-hot-loader/root'; // eslint-disable-line import/no-extraneous-dependencies
import { Persistor } from 'redux-persist/es/types';
import { Store } from 'redux';

import router from 'services/RouterService';
import reduxStore from 'services/StoreService';
import sentry from 'services/SentryService';

import App from 'routes/App';

const store: Store = reduxStore.get();
const persistor: Promise<Persistor> = reduxStore.getPersistor();

sentry.init();

function render(AppComponent: ComponentType): void {
  const HotComponent: ComponentType = process.env.NODE_ENV !== 'production' ? hot(AppComponent) : AppComponent;

  ReactDOM.render(
    <Provider store={store}>
      <Router history={router.browserHistory}>
        <HotComponent />
      </Router>
    </Provider>,
    document.getElementById('root') as HTMLElement,
  );
}

persistor.then((): void => render(App));
