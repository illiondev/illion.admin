import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Fab from '@material-ui/core/Fab';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import { BOOK_TYPES_LIST } from 'enums/BookTypesEnum';

import router from 'services/RouterService';

import ProductCard from './ProductCard';

class BookSettingsProducts extends Component {
  static propTypes = {
    book: PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        productId: PropTypes.string,
      }).isRequired,
    }).isRequired,
  };

  render() {
    const { book } = this.props;

    return (
      <Fragment>
        <div className="m-align-center">
          {BOOK_TYPES_LIST.map(type => (
            <Fab
              key={type.id}
              variant="extended"
              color="primary"
              className="m-margin_right-medium"
              component={Link}
              to={router.url.BOOK_PRODUCT_NEW(book.id, type.id)}
            >
              {type.icon}
              <span className="m-margin_left-normal">{type.title_new}</span>
            </Fab>
          ))}
        </div>

        <Divider variant="middle" className="m-margin_top-medium m-margin_bottom-medium" />

        <Grid container spacing={24}>
          {book.products.data.sort((p1, p2) => Number(p1.hidden) - Number(p2.hidden)).map(p => (
            <Grid item key={p.id}>
              <ProductCard entry={p} />
            </Grid>
          ))}
        </Grid>
      </Fragment>
    );
  }
}

export default BookSettingsProducts;
