import { connect } from 'react-redux';

import { RootState } from 'lib';
import { authLogin } from 'lib/auth/actions';

import LogIn from './LogIn';

export interface LogInDispatchProps {
  authLogin: typeof authLogin;
}

export default connect<null, LogInDispatchProps, {}, RootState>(null, { authLogin })(
  // @ts-ignore
  LogIn
);
