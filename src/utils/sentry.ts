import * as Sentry from '@sentry/browser';
import { Dispatch, Middleware } from 'redux';

import { Action } from 'types/redux';

// Sentry redux middlewares
export const sentryReporter: Middleware = (): any => (next: Dispatch): any => (action: Action): Action => {
  Sentry.addBreadcrumb({
    message: action.type,
    category: 'REDUX ACTION',
    level: Sentry.Severity.Info,
    data: {
      payload: action.payload
    }
  });

  return next(action);
};
