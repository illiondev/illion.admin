import React, { Component } from 'react';
import {
  Route, Redirect, RouteComponentProps, RouteProps
} from 'react-router-dom';

import router from 'services/RouterService';

interface Props extends RouteProps {
  isLogged: boolean;
  component: any;
}

class RoutePublic extends Component<Props> {
  public render(): React.ReactNode {
    const { isLogged, component: RouteComponent, ...rest } = this.props;

    return (
      <Route
        {...rest}
        render={(props: RouteComponentProps): React.ReactNode => (
          !isLogged ? (
            <RouteComponent {...props} />
          ) : (
            <Redirect to={{ pathname: router.url.BOOKS() }} />
          )
        )}
      />
    );
  }
}

export default RoutePublic;
