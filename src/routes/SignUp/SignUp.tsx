import React, { Component } from 'react';

import Avatar from '@material-ui/core/Avatar';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import SignUpForm from './SignUpForm';
import { SignUpDispatchProps as Props } from './SignUp.connect';

import './SignUp.scss';

class SignUp extends Component<Props> {
  public render(): React.ReactNode {
    return (
      <div className="b-auth">
        <Paper className="b-auth__paper">
          <Avatar className="b-auth__avatar">
            <LockIcon />
          </Avatar>
          <Typography variant="h5">Регистрация</Typography>

          <SignUpForm onSave={this.props.authRegister} />
        </Paper>
      </div>
    );
  }
}

export default SignUp;
