import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import FormFieldText from 'components/FormFieldText';
import FormFieldEditor from 'components/FormFieldEditor';
import FormFieldFile from 'components/FormFieldFile';

class BundleDetails extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    onDeleteCover: PropTypes.func.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Grid container spacing={24} className="m-margin_top-normal">
        <Grid item xs={12} md={9}>
          <Typography variant="subtitle1">
            Описание
          </Typography>
          <Field
            name="title"
            label="Название"
            fullWidth
            component={FormFieldText}
            disabled={isReadOnly}
            className="m-margin_bottom-normal"
          />
          <Field
            name="notification"
            label="Сообщение для покупателей"
            fullWidth
            component={FormFieldText}
            disabled={isReadOnly}
          />
          <Typography variant="caption" className="m-margin_bottom-normal">
            Отображается на странице книги и в корзине
          </Typography>
          <Field
            name="description"
            label="Описание"
            fullWidth
            component={FormFieldEditor}
            disabled={isReadOnly}
            options={{ height: 200 }}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <Field
            name="cover"
            label="Обложка"
            isImageFile
            component={FormFieldFile}
            disabled={isReadOnly}
            onDeleteFile={this.props.onDeleteCover}
            className="m-margin_bottom-normal"
          />
          <Field
            name="slug"
            label="Slug"
            fullWidth
            component={FormFieldText}
            disabled={isReadOnly}
            className="m-margin_bottom-normal"
          />
        </Grid>
      </Grid>
    );
  }
}

export default BundleDetails;
