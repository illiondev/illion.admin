import { connect } from 'react-redux';
import withWidth from '@material-ui/core/withWidth';

import { MenuItem } from 'types';
import { RootState } from 'lib';

import { authLogout } from 'lib/auth/actions';
import { getProfileName, getUserMenuItems } from 'lib/profile/selectors';

import Header from './Header';

export interface HeaderStoreProps {
  isLogged: boolean;
  isLoading: boolean;
  username?: string;
  menuItems: MenuItem[];
}

export interface HeaderDispatchProps {
  authLogout: typeof authLogout;
}

function mapStateToProps(state: RootState): HeaderStoreProps {
  return {
    isLogged: Boolean(state.auth.tokens),
    isLoading: state.app.loader.counter > 0,
    username: getProfileName(state),
    menuItems: getUserMenuItems(state),
  };
}

export default connect<HeaderStoreProps, HeaderDispatchProps, {}, RootState>(mapStateToProps, { authLogout })(
  // @ts-ignore
  withWidth()(Header)
);
