import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import router from 'services/RouterService';

import BundleSettings from './BundleSettings';

class Bundle extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string.isRequired,
        bundleId: PropTypes.string,
      }).isRequired,
    }).isRequired,
    bundle: PropTypes.shape({}),
    isReadOnlyRoute: PropTypes.bool.isRequired,
    bookBundleCreate: PropTypes.func.isRequired,
    bookBundleUpdate: PropTypes.func.isRequired,
    bookBundleCoverDelete: PropTypes.func.isRequired,
    bookBundleDetach: PropTypes.func.isRequired,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    // создание бандла
    if (nextProps.match.params.bundleId === 'new') {
      return {
        bundle: { book_id: Number(nextProps.match.params.id) },
      };
    }

    if (nextProps.bundle !== prevState.currentBundle) {
      return {
        bundle: nextProps.bundle,
        currentBundle: nextProps.bundle,
      };
    }

    return null;
  }

  state = {
    currentBundle: undefined,
    bundle: undefined,
  };

  static defaultProps = {
    bundle: undefined,
  };

  handleClose = () => router.push(router.url.BOOK_EDIT_BUNDLES(this.props.match.params.id));

  render() {
    const { isReadOnlyRoute, match } = this.props;
    const { bundle } = this.state;

    if (!bundle) {
      return <Typography variant="headline" align="center" color="error">Такого комплекта не существует</Typography>;
    }

    return (
      <BundleSettings
        bookId={Number(match.params.id)}
        bundle={bundle}
        isReadOnly={isReadOnlyRoute}
        onClose={this.handleClose}
        onCreate={this.props.bookBundleCreate}
        onUpdate={this.props.bookBundleUpdate}
        onDelete={this.props.bookBundleDetach}
        onDeleteCover={this.props.bookBundleCoverDelete}
      />
    );
  }
}

export default Bundle;
