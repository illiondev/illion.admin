import React, { Component, Fragment } from 'react';
import { FieldRenderProps } from 'react-final-form';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import SaveIcon from '@material-ui/icons/Save';
import { ButtonProps } from '@material-ui/core/Button';

import './FormFieldFile.scss';

interface FormFieldFileProps extends FieldRenderProps<any> {
  label: string;
  className: string;
  accept: string;
  showPreview: boolean;
  isImageFile: boolean;
  disabled: boolean;
  onDeleteFile(): void;
  onUpdateFile(): void;
}

interface State {
  filePreview: string;
  currentValue: any;
}

class FormFieldFile extends Component<FormFieldFileProps, State> {
  public static defaultProps = {
    accept: '*',
    label: '',
    className: '',
    showPreview: true,
    disabled: false,
    isImageFile: false,
    onDeleteFile: undefined,
    onUpdateFile: undefined,
  };

  public static getDerivedStateFromProps(nextProps: FormFieldFileProps, prevState: State): State|null {
    if (nextProps.input.value !== prevState.currentValue) {
      const currentValue = nextProps.input.value;
      let filePreview = prevState.filePreview;

      // если значения нет - сбрасываем превью
      if (!currentValue) {
        return {
          currentValue,
          filePreview: '',
        };
      }

      // если в значении данные с сервера, подставляем в превью урл
      if (currentValue.data) {
        filePreview = currentValue.data.url;
      }

      return {
        currentValue,
        filePreview,
      };
    }

    return null;
  }

  private input = React.createRef<HTMLInputElement>();

  public state: State = {
    filePreview: '',
    currentValue: '',
  };

  private getFileName(): string {
    const { currentValue } = this.state;

    if (currentValue.data) return currentValue.data.url.split('/').pop() || '';

    if (currentValue.name) return currentValue.name;

    return '';
  }

  private handleChangeFile = (e: React.ChangeEvent<HTMLInputElement>): void => {
    e.preventDefault();
    const reader = new FileReader();
    // @ts-ignore
    const file = e.target.files[0];

    if (this.props.showPreview) {
      reader.onloadend = (): void => {
        this.setState({
          filePreview: reader.result as string,
        });
        this.props.input.onChange(file as any);
      };

      reader.readAsDataURL(file);
    } else {
      this.props.input.onChange(file as any);
    }
  };

  private handleReset = (e: React.MouseEvent<HTMLInputElement>): void => {
    e.preventDefault();

    this.props.input.onChange(this.props.meta.initial);

    if (this.input.current) {
      this.input.current.value = '';
    }
  };

  public render(): React.ReactNode {
    const {
      showPreview,
      label,
      accept,
      disabled,
      input: { name, value },
      meta,
      isImageFile,
      className,
    } = this.props;
    const { filePreview } = this.state;
    const hasPreview = filePreview && showPreview;
    const currentFileName = this.getFileName();
    const acceptFiles = isImageFile ? '.jpg, .jpeg, .png' : accept || '';
    const hasError = Boolean(meta.error && meta.touched);
    const showDeleteAction = this.props.onDeleteFile && value.data && !disabled;
    const showPreviewAction = value.data && showPreview;
    const showCancelAction = !value.data && meta.initial;
    const showUpdateAction = this.props.onUpdateFile && value !== meta.initial && meta.valid;

    return (
      <Card className={`b-file-picker ${className}`}>
        <div className="b-file-picker__info">
          <CardContent>
            <Typography variant="caption" color={hasError ? 'error' : 'textSecondary'}>
              {label}
            </Typography>
            <Typography
              title={currentFileName || ''}
              variant="subtitle1"
              noWrap
              color={hasError ? 'error' : 'textPrimary'}
            >
              {hasError ? meta.error : (currentFileName || 'Выберите файл')}
            </Typography>
          </CardContent>
          <CardActions className="b-file-picker__actions">
            {showDeleteAction && (
              <Tooltip title="Удалить файл" placement="top">
                <IconButton className="m-margin_right-auto" onClick={this.props.onDeleteFile}>
                  <DeleteIcon color="secondary" />
                </IconButton>
              </Tooltip>
            )}

            {showPreviewAction && (
              <Tooltip title="Просмотр" placement="top">
                <IconButton component="a" href={filePreview} target="_blank" rel="noopener noreferrer">
                  <VisibilityIcon />
                </IconButton>
              </Tooltip>
            )}

            {showCancelAction && (
              <Tooltip title="Отмена" placement="top">
                <IconButton onClick={this.handleReset}>
                  <ArrowBackIcon color="action" />
                </IconButton>
              </Tooltip>
            )}

            {!disabled && (
              <Fragment>
                <Tooltip title={value ? 'Изменить' : 'Добавить'} className="m-margin_left-auto" placement="top">
                  <IconButton
                    component={(props: ButtonProps): any => <label htmlFor={`${name}_file_picker`} {...props} />} // eslint-disable-line jsx-a11y/label-has-associated-control, jsx-a11y/label-has-for
                  >
                    {value ? <EditIcon color="primary" /> : <AddIcon color="primary" />}
                  </IconButton>
                </Tooltip>

                {showUpdateAction && (
                  <Tooltip title="Сохранить" className="m-margin_left-normal" placement="top">
                    <IconButton onClick={this.props.onUpdateFile}>
                      <SaveIcon color="secondary" />
                    </IconButton>
                  </Tooltip>
                )}
              </Fragment>
            )}
          </CardActions>
        </div>

        {hasPreview && (
          <CardMedia
            className="b-file-picker__preview"
            image={filePreview}
          />
        )}

        <input
          className="b-file-picker__input"
          type="file"
          name={name}
          accept={acceptFiles}
          id={`${name}_file_picker`}
          onChange={this.handleChangeFile}
          ref={this.input}
        />
      </Card>
    );
  }
}

export default FormFieldFile;
