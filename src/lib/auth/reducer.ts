import { ITokensData } from 'illion-api';

import types from './actionTypes';
import { AuthAllActions } from './actions';

export interface AuthState {
  readonly tokens: ITokensData|undefined;
}

const initialState: AuthState = {
  tokens: undefined,
};

const auth = (state = initialState, action: AuthAllActions): AuthState => {
  switch (action.type) {
    case types.LOG_IN:
      return {
        ...state,
        tokens: action.payload
      };

    case types.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default auth;
