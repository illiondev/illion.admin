import React, { Component } from 'react';
// @ts-ignore
import ClickOutside from 'react-click-outside';

import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';

import { AutocompleteEntry } from 'types';

import { AutoCompleteDispatchProps, AutoCompleteStoreProps } from './AutoComplete.connect';

import './AutoComplete.scss';

export interface AutoCompleteProps {
  name: string;
  label: string;
  autocompleteHandler(query: string): void;
  onChoose(query?: AutocompleteEntry): void;
  skip: (number|string)[];
  current: AutocompleteEntry;
  minLength: number;
  error: string;
  disabled: boolean;
  multiple: boolean;
}

type Props = AutoCompleteStoreProps & AutoCompleteDispatchProps & AutoCompleteProps;

interface State {
  query: string;
  isOpen: boolean;
  currentId?: number|string;
  entries?: AutocompleteEntry[];
}

class AutoComplete extends Component<Props, State> {
  public static defaultProps = {
    entries: [],
    skip: [],
    current: {} as AutocompleteEntry,
    label: '',
    error: '',
    minLength: 1,
    disabled: false,
    multiple: false,
  };

  public static getDerivedStateFromProps(nextProps: Props, prevState: State): State|null {
    const { current, entries } = nextProps;
    const nextState = {} as State;

    if (entries && entries !== prevState.entries && entries.length > 0) {
      nextState.isOpen = true;
      nextState.entries = entries;
    }

    if (current.id !== prevState.currentId) {
      nextState.query = current.title || '';
      nextState.currentId = current.id;
    }

    if (Object.keys(nextState).length > 0) return nextState;

    return null;
  }

  public state: State = {
    query: '',
    isOpen: false,
    currentId: undefined,
    entries: [],
  };

  private textField = React.createRef<HTMLElement>();

  private submitTimer: NodeJS.Timeout|null = null;

  public componentWillUnmount(): void {
    if (this.submitTimer) {
      clearTimeout(this.submitTimer);
    }
    this.props.appAutocompleteDrop();
  }

  private handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (this.submitTimer) {
      clearTimeout(this.submitTimer);
    }

    const value = e.target.value;

    if (value.length >= this.props.minLength) {
      this.submitTimer = setTimeout((): void => {
        this.props.autocompleteHandler(value);
      }, 500);
    }

    this.setState({
      query: value,
    });

    // если из инпута все удалили, сбрасываем текущее значение
    if (!value) {
      this.props.onChoose();
    }
  };

  private handleCloseList = (): void => this.setState({ isOpen: false });

  private handleChoose = (suggestion: AutocompleteEntry): void => {
    const { entries = [], multiple, skip } = this.props;
    // если можно выбрать много - дропдаун не закрываем
    const isOpenList = !multiple ? false
      : entries.filter((e: AutocompleteEntry): boolean => !skip.includes(e.id)).length > 1;
    this.props.onChoose(suggestion);

    // при выборе сразу обновляем query, надо для случаев когда выбирается вариант с тем же ID который был выбран до этого
    this.setState({
      isOpen: isOpenList,
      query: multiple ? '' : suggestion.title,
    });
  };

  public render(): React.ReactNode {
    const {
      entries = [], isFetching, label, current, name, error, disabled, skip
    } = this.props;
    const { query, isOpen } = this.state;

    return (
      <div className="b-autocomplete">
        <TextField
          fullWidth
          name={name}
          label={label}
          value={query}
          helperText={error}
          error={Boolean(error)}
          disabled={disabled}
          onChange={this.handleChangeInput}
          InputProps={{
            inputRef: this.textField,
            autoComplete: 'off'
          }}
          FormHelperTextProps={{ className: 'b-input__helper-text' }}
        />

        {isFetching && <CircularProgress className="b-autocomplete__progress" size={30} />}

        <Popper open={isOpen} anchorEl={this.textField.current} className="b-autocomplete__dropdown">
          <ClickOutside onClickOutside={this.handleCloseList}>
            <Paper square style={{ width: this.textField.current ? this.textField.current.clientWidth : undefined }}>
              {entries
                .filter((e: AutocompleteEntry): boolean => !skip.includes(e.id))
                .map((suggestion: AutocompleteEntry): React.ReactNode => (
                  <MenuItem
                    key={suggestion.id}
                    selected={current.id === suggestion.id}
                    component="div"
                    onClick={(): void => this.handleChoose(suggestion)}
                    style={{
                      fontWeight: current.id === suggestion.id ? 500 : 400,
                    }}
                  >
                    <div className="b-autocomplete__item-text">{suggestion.title}</div>
                  </MenuItem>
                ))
              }
            </Paper>
          </ClickOutside>
        </Popper>
      </div>
    );
  }
}

export default AutoComplete;
