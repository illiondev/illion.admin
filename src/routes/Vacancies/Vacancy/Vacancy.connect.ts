import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import { Vacancy as VacancyType } from 'types';

import { RootState } from 'lib';
import { vacancyCreate, vacancyRead, vacancyUpdate } from 'lib/vacancies/actions';
import { getVacanciesById } from 'lib/vacancies/selectors';

import Vacancy from './Vacancy';

export interface VacancyStoreProps {
  entry: VacancyType;
}

export interface VacancyDispatchProps {
  vacancyCreate: typeof vacancyCreate;
  vacancyRead: typeof vacancyRead;
  vacancyUpdate: typeof vacancyUpdate;
}

export interface VacancyRouteParams extends RouteComponentProps<{id: string}> {}

function mapStateToProps(state: RootState, ownProps: VacancyRouteParams): VacancyStoreProps {
  return {
    entry: getVacanciesById(state)[Number(ownProps.match.params.id)],
  };
}

export default connect<VacancyStoreProps, VacancyDispatchProps, VacancyRouteParams, RootState>(mapStateToProps, {
  vacancyCreate,
  vacancyRead,
  vacancyUpdate,
})(
  // @ts-ignore
  Vacancy
);
