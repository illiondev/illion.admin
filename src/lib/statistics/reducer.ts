import { StatsBundle, StatsProfit, StatsSummary } from 'types/stats';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { StatisticsAllActions } from './actions';

export interface StatisticsState {
  readonly entries: StatsBundle[];
  readonly summary: StatsSummary|undefined;
  readonly profit: StatsProfit[];
}

const initialState: StatisticsState = {
  entries: [],
  profit: [],
  summary: undefined,
};

const statistics = (state = initialState, action: StatisticsAllActions): StatisticsState => {
  switch (action.type) {
    case types.STATISTICS_LIST:
      return {
        ...state,
        entries: action.payload.stats,
        summary: action.payload.summary,
      };

    case types.STATISTICS_PROFIT:
      return {
        ...state,
        profit: action.payload,
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default statistics;
