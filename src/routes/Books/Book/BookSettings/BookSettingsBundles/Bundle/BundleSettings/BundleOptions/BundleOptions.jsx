import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import FormFieldSwitch from 'components/FormFieldSwitch';

class BundleOptions extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">Настройки</Typography>
        </Grid>
        <Grid item>
          <Field
            name="enabled"
            type="checkbox"
            label="Активен"
            component={FormFieldSwitch}
            disabled={isReadOnly}
          />
        </Grid>

        <Grid item>
          <Field
            name="pre_order"
            type="checkbox"
            label="Предзаказ"
            component={FormFieldSwitch}
            disabled={isReadOnly}
          />
        </Grid>

        <Grid item>
          <Field
            name="hidden"
            type="checkbox"
            label="Скрыт"
            component={FormFieldSwitch}
            disabled={isReadOnly}
          />
        </Grid>
      </Grid>
    );
  }
}

export default BundleOptions;
