import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import indigo from '@material-ui/core/colors/indigo';
import SettingsIcon from '@material-ui/icons/Settings';

import { BOOK_TYPES_ICON_BY_ID, BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';

import router from 'services/RouterService';

class ProductCard extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      book_id: PropTypes.number.isRequired,
      id: PropTypes.number.isRequired,
      type: PropTypes.number.isRequired,
      product_title: PropTypes.string,
    }).isRequired,
  };

  render() {
    const { entry } = this.props;

    return (
      <Card>
        <CardHeader
          avatar={(
            <Avatar style={{ backgroundColor: indigo.A100 }}>
              {BOOK_TYPES_ICON_BY_ID[entry.type]}
            </Avatar>
          )}
          action={(
            <Tooltip title="Настройки" placement="top">
              <IconButton
                className="m-margin_left-auto"
                component={Link}
                to={router.url.BOOK_EDIT_PRODUCT(entry.book_id, entry.id)}
              >
                <SettingsIcon />
              </IconButton>
            </Tooltip>
          )}
          title={`${entry.product_title || BOOK_TYPES_TITLE_BY_ID[entry.type]} #${entry.id}`}
          titleTypographyProps={{ noWrap: true }}
          classes={{ content: 'b-product-card__title' }}
        />
      </Card>
    );
  }
}

export default ProductCard;
