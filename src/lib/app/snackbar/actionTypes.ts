enum AppSnackbarActionTypes {
  APP_SNACKBAR_SHOW = 'APP_SNACKBAR_SHOW',
  APP_SNACKBAR_HIDE = 'APP_SNACKBAR_HIDE',
}

export default AppSnackbarActionTypes;
