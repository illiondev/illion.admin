import * as Sentry from '@sentry/browser';

import { SENTRY_DSN, APP_ENV, RELEASE } from 'config';

import reduxStore from 'services/StoreService';
import { RootState } from 'lib';

const store = reduxStore.get();

class SentryService {
  public static init(): void {
    if (SENTRY_DSN) {
      Sentry.init({
        dsn: SENTRY_DSN,
        release: RELEASE,
        integrations: [new Sentry.Integrations.RewriteFrames()],
        environment: APP_ENV,
        beforeSend(event: Sentry.SentryEvent): Sentry.SentryEvent {
          const { auth }: RootState = store.getState();

          /* eslint-disable no-param-reassign */
          event.extra = event.extra || {};
          event.extra.redux = {
            auth: Boolean(auth.tokens)
          };
          event.extra.connection = SentryService.getConnectionInfo();

          if (event.extra.Error && event.extra.Error.response) {
            event.tags = event.tags || {};
            event.tags['HTTP status'] = event.extra.Error.response.status;
            event.tags['HTTP status message'] = event.extra.Error.response.statusText;
          }

          /* eslint-enable no-param-reassign */

          return event;
        }
      });
    }
  }

  public static getConnectionInfo(): Record<string, string|number> {
    const networkInformation = window.navigator
      // @ts-ignore
      ? (window.navigator.connection || window.navigator.webkitConnection) : undefined;
    const connection: Record<string, string|number> = {};

    if (!networkInformation) return connection;

    for (const key in networkInformation) { // eslint-disable-line no-restricted-syntax
      if (typeof networkInformation[key] !== 'function') {
        connection[key] = networkInformation[key];
      }
    }

    return connection;
  }

  public static setFingerprints(fingerprints: string[]): void {
    Sentry.configureScope((scope: Sentry.Scope): void => {
      scope.setFingerprint(fingerprints);
    });
  }
}

export default SentryService;
