import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { PUBLIC_URL } from 'config';
import { copyToClipboard } from 'utils/index';
import router from 'services/RouterService';

import BundleCard from './BundleCard';
import FormAttachBundle from './FormAttachBundle';

class BookSettingsBundles extends Component {
  static propTypes = {
    isReadOnlyRoute: PropTypes.bool.isRequired,
    book: PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      bundles: PropTypes.shape({
        data: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.number,
          book_id: PropTypes.number,
          enabled: PropTypes.bool,
        })).isRequired,
      }).isRequired,
    }).isRequired,
    bookBundleUpdate: PropTypes.func.isRequired,
    bookBundleAttach: PropTypes.func.isRequired,
    bundleAutocomplete: PropTypes.func.isRequired,
    snackbarShow: PropTypes.func.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string,
      }).isRequired,
    }).isRequired,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.book !== prevState.bookProp) {
      return {
        modalIsOpen: false,
        bookProp: nextProps.book,
      };
    }

    return null;
  }

  state = {
    modalIsOpen: false,
    bookProp: undefined,
  };

  handleToggleEnabled = bundle => this.props.bookBundleUpdate({
    enabled: Number(!bundle.enabled),
    book_id: this.props.book.id,
    id: bundle.id
  });

  handleShowAttachForm = () => this.setState({ modalIsOpen: true });

  handleAttachBundle = (id) => {
    this.props.bookBundleAttach({ book_id: this.props.book.id, id });
  };

  handleModalClose = () => this.setState({ modalIsOpen: false });

  handleCopyLink = (bundle, type) => {
    switch (type) {
      case 'cart':
        copyToClipboard(`${PUBLIC_URL}/buy/${bundle.id}`);
        break;

      case 'bill':
        copyToClipboard(`${PUBLIC_URL}/payment/product/${bundle.id}`);
        break;

      default:
        break;
    }

    this.props.snackbarShow({ type: 'info', message: 'Ссылка скопирована в буфер' });
  };


  render() {
    const { book, isReadOnlyRoute } = this.props;
    const { modalIsOpen } = this.state;

    return (
      <Fragment>
        <div className="m-align-center">
          <Button
            color="primary"
            className="m-margin_right-medium"
            component={Link}
            to={router.url.BOOK_EDIT_BUNDLE(book.id, 'new')}
          >
            Добавить новый комплект
          </Button>
          <Button
            color="primary"
            onClick={this.handleShowAttachForm}
          >
            Добавить из другой книги
          </Button>
        </div>

        <Divider variant="middle" className="m-margin_top-medium m-margin_bottom-medium" />

        <Grid container spacing={24}>
          {book.bundles.data.map(p => (
            <Grid item xs={12} md={4} key={p.id}>
              <BundleCard
                entry={p}
                bookId={book.id}
                onCopyLink={this.handleCopyLink}
                onToggleEnabled={this.handleToggleEnabled}
                isReadOnly={isReadOnlyRoute}
              />
            </Grid>
          ))}
        </Grid>

        <FormAttachBundle
          onClose={this.handleModalClose}
          onSave={this.handleAttachBundle}
          onBundleAutocomplete={this.props.bundleAutocomplete}
          open={modalIsOpen}
        />
      </Fragment>
    );
  }
}

export default BookSettingsBundles;
