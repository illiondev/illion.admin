import { createSelector } from 'reselect';
import { CUSTOMER_FORM_FIELDS } from 'enums/OrderEnums';

export const getEntries = state => state.orders.entries;
export const getOrdersById = createSelector(getEntries, entries => entries.reduce((byId, entry) => ({
  ...byId,
  [`${entry.id}`]: entry,
}), {}));

export const getOrdersBillsById = createSelector(getEntries, entries => entries.reduce((byId, entry) => ({
  ...byId,
  [`${entry.id}`]: entry.bills.data,
}), {}));

export const getCustomerFormById = createSelector(getEntries, entries => entries.reduce((byId, entry) => ({
  ...byId,
  [`${entry.id}`]: CUSTOMER_FORM_FIELDS.reduce((form, field) => ({ ...form, [field]: entry[field] || '' }), {}),
}), {}));
