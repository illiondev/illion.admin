import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  bundles: [],
  entries: [],
  pager: {},
  entry: undefined,
  isFetchingEntry: false,
  preview: undefined,
};

const books = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.BOOK_LIST:
      return {
        ...state,
        entries: payload.entries,
        pager: payload.pager,
      };

    case types.BOOK_INIT:
      if (state.entry && state.entry.id === payload.id) return state;

      return {
        ...state,
        entry: payload,
      };

    case types.BOOK_CREATE:
    case types.BOOK_UPDATE:
      return {
        ...state,
        entry: payload,
      };

    case types.BOOK_READ:
      return {
        ...state,
        entries: payload,
        isFetchingEntry: false,
      };

    case types.BOOK_PREVIEW:
      return {
        ...state,
        preview: payload,
      };

    case types.BOOK_ENTRY_FETCH:
      return {
        ...state,
        isFetchingEntry: payload,
      };

    case types.BOOK_PUBLISH:
    case types.BOOK_UNPUBLISH:
      return {
        ...state,
        entry: payload.entry,
        entries: state.entries.some(e => e.id === payload.entry.id)
          ? state.entries.map(e => (e.id !== payload.entry.id ? e : payload.entry)) : state.entries,
      };

    case types.BOOK_PRODUCT_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          products: {
            data: [...state.entry.products.data, payload],
          }
        },
      };

    case types.BOOK_PRODUCT_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          products: {
            data: state.entry.products.data.map(p => (p.id !== payload.id ? p : payload)),
          }
        },
      };

    case types.BOOK_PRODUCT_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          products: {
            data: state.entry.products.data.filter(p => (p.id !== payload.id)),
          }
        },
      };

    case types.QUOTES_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          quotes: {
            data: [...state.entry.quotes.data, payload.entry],
          }
        },
      };

    case types.QUOTES_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          quotes: {
            data: state.entry.quotes.data.map(r => (r.id !== payload.entry.id ? r : payload.entry)),
          }
        },
      };

    case types.QUOTES_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          quotes: {
            data: state.entry.quotes.data.filter(r => (r.id !== payload.entry.id)),
          }
        },
      };

    case types.ADVANTAGES_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          advantages: {
            data: [...state.entry.advantages.data, payload.entry],
          }
        },
      };

    case types.ADVANTAGES_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          advantages: {
            data: state.entry.advantages.data.map(r => (r.id !== payload.entry.id ? r : payload.entry)),
          }
        },
      };

    case types.ADVANTAGES_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          advantages: {
            data: state.entry.advantages.data.filter(r => (r.id !== payload.entry.id)),
          }
        },
      };

    case types.REVIEWS_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          reviews: {
            data: [...state.entry.reviews.data, payload.entry],
          }
        },
      };

    case types.REVIEWS_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          reviews: {
            data: state.entry.reviews.data.map(r => (r.id !== payload.entry.id ? r : payload.entry)),
          }
        },
      };

    case types.REVIEWS_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          reviews: {
            data: state.entry.reviews.data.filter(r => (r.id !== payload.entry.id)),
          }
        },
      };

    case types.FEATURES_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          features: {
            data: [...state.entry.features.data, payload.entry],
          }
        },
      };

    case types.FEATURES_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          features: {
            data: state.entry.features.data.map(r => (r.id !== payload.entry.id ? r : payload.entry)),
          }
        },
      };

    case types.FEATURES_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          features: {
            data: state.entry.features.data.filter(r => (r.id !== payload.entry.id)),
          }
        },
      };

    case types.IMAGE_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          photos: {
            data: [...state.entry.photos.data, payload.entry],
          }
        },
      };

    case types.FRAGMENT_UPDATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          fragment: payload.entry,
        },
      };

    case types.IMAGE_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          photos: {
            data: state.entry.photos.data.filter(r => (r.id !== payload.entry.id)),
          }
        },
      };

    case types.BOOK_CLEAR:
      return {
        ...state,
        entry: undefined,
        preview: undefined,
      };


    case types.BOOK_BUNDLE_CREATE:
      return {
        ...state,
        entry: {
          ...state.entry,
          bundles: {
            data: [...state.entry.bundles.data, payload],
          }
        },
      };

    case types.BOOK_BUNDLE_READ:
      return {
        ...state,
        bundles: payload,
      };

    case types.BOOK_BUNDLE_UPDATE:
    case types.BOOK_BUNDLE_PRODUCT_CREATE:
    case types.BOOK_BUNDLE_PRODUCT_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          bundles: {
            data: state.entry.bundles.data.map(r => (r.id !== payload.id ? r : payload)),
          }
        },
      };

    case types.BOOK_BUNDLE_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          bundles: {
            data: state.entry.bundles.data.filter(r => (r.id !== payload.id)),
          }
        },
      };

    case types.BOOK_BUNDLE_COVER_DELETE:
      return {
        ...state,
        entry: {
          ...state.entry,
          bundles: {
            data: state.entry.bundles.data.map(p => (p.id !== payload.bundle_id ? p : ({
              ...p,
              cover: undefined,
            }))),
          }
        },
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default books;
