import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class FormComment extends Component {
  static propTypes = {
    currentValue: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
  };

  form = null;

  handleSubmit = ({ comment }) => {
    this.props.onSave(comment);
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  render() {
    const { open, currentValue } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="xs" fullWidth>
        <DialogTitle>Комментарий</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.handleSubmit}
            subscription={{ submitting: true, pristine: true }}
            initialValues={{
              comment: currentValue,
            }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                <Field
                  label="Сообщение*"
                  name="comment"
                  component={FormFieldText}
                  validate={required}
                  fullWidth
                  multiline
                  rows={5}
                />
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            {currentValue ? 'Добавить' : 'Сохранить'}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormComment;
