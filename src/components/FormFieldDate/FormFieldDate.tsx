import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import DatePicker from 'components/DatePicker';

class FormFieldDate extends Component<FieldRenderProps<any>> {
  public render(): React.ReactNode {
    const {
      input,
      meta,
      ...rest
    } = this.props;

    return (
      // @ts-ignore
      <DatePicker
        {...rest}
        name={input.name}
        helperText={meta.touched ? meta.error : undefined}
        error={meta.error && meta.touched}
        value={input.value}
        onChange={(value: any): void => input.onChange(value)}
      />
    );
  }
}

export default FormFieldDate;
