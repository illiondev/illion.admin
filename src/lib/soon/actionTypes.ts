enum SoonActionTypes {
  SOON_READ = 'SOON_READ',
  SOON_CREATE = 'SOON_CREATE',
  SOON_UPDATE = 'SOON_UPDATE',
  SOON_DELETE = 'SOON_DELETE',
}

export default SoonActionTypes;
