import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import TablePagination from '@material-ui/core/TablePagination';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';

import router from 'services/RouterService';

import BookItem from './BookItem';

class Books extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
    })).isRequired,
    pager: PropTypes.shape({}).isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
    getBookList: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.getBookList();
  }

  handleChangePage = (e, page) => {
    this.props.getBookList({ page: page + 1 });
  };

  render() {
    const { entries, pager, isReadOnlyRoute } = this.props;

    return (
      <div className="b-page__content">
        {!isReadOnlyRoute && (
          <Tooltip title="Новая книга" placement="top">
            <Fab
              color="primary"
              className="b-page__actions"
              component={Link}
              to={router.url.BOOK_NEW()}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}

        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Книги</Typography>
          {entries.length > 0 && (
            <List>
              {entries.map(entry => <BookItem entry={entry} key={entry.id} />)}
            </List>
          )}

          {pager.total_pages > 1 && (
            <TablePagination
              component="div"
              count={pager.total}
              rowsPerPage={pager.per_page}
              rowsPerPageOptions={[pager.per_page]}
              page={pager.current_page - 1}
              onChangePage={this.handleChangePage}
            />
          )}
        </Paper>
      </div>
    );
  }
}

export default Books;
