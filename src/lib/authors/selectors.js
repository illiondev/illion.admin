import { createSelector } from 'reselect';

export const getAuthors = state => state.authors.entries;
export const getCurrentAuthor = state => state.authors.entry;
export const getAuthorsById = createSelector(getAuthors, getCurrentAuthor, (entries, entry) => {
  const authorsById = entries.reduce((byId, author) => ({
    ...byId,
    [author.id]: author,
  }), {});

  if (entry && !authorsById[entry.id]) {
    authorsById[entry.id] = entry;
  }

  return authorsById;
});
