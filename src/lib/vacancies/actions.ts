import api from 'services/ApiService';
import router from 'services/RouterService';

import { Vacancy } from 'types';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

type VacanciesReadSuccess = Action<types.VACANCIES_READ, Vacancy[]>;
type VacancyCreateSuccess = Action<types.VACANCY_CREATE, Vacancy>;
type VacancyReadSuccess = Action<types.VACANCY_READ, Vacancy>;
type VacancyUpdateSuccess = Action<types.VACANCY_UPDATED, Vacancy>;

export type VacanciesAllActions = VacanciesReadSuccess | VacancyCreateSuccess |
VacancyReadSuccess | VacancyUpdateSuccess | AuthLogout;


function vacanciesReadSuccess(payload: Vacancy[]): VacanciesReadSuccess {
  return {
    type: types.VACANCIES_READ,
    payload,
  };
}

export function vacanciesRead(): ActionThunk {
  return (dispatch): Promise<void> => api.vacanciesRead().then((res): void => {
    dispatch(vacanciesReadSuccess(res.data.data || []));
  });
}

function vacancyCreateSuccess(payload: Vacancy): VacancyCreateSuccess {
  return {
    type: types.VACANCY_CREATE,
    payload,
  };
}

export function vacancyCreate(data: Omit<Vacancy, 'id'>): ActionThunk {
  return (dispatch): Promise<void> => api.vacancyCreate(data).then((res): void => {
    const entry: Vacancy = res.data.data;

    dispatch(vacancyCreateSuccess(res.data.data));

    router.push(router.url.VACANCY(`${entry.id}`));
  });
}

function vacancyReadSuccess(payload: Vacancy): VacancyReadSuccess {
  return {
    type: types.VACANCY_READ,
    payload,
  };
}

export function vacancyRead(id: number): ActionThunk {
  return (dispatch): Promise<void> => api.vacancyRead(id).then((res): void => {
    dispatch(vacancyReadSuccess(res.data.data));
  });
}

function vacancyUpdateSuccess(payload: Vacancy): VacancyUpdateSuccess {
  return {
    type: types.VACANCY_UPDATED,
    payload,
  };
}

export function vacancyUpdate(data: Vacancy): ActionThunk {
  return (dispatch): Promise<void> => api.vacancyUpdate(data).then((res): void => {
    dispatch(vacancyUpdateSuccess(res.data.data));
  });
}
