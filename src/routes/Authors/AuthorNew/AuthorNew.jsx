import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AuthorSettings from 'routes/Authors/Author/AuthorSettings';

class AuthorNew extends Component {
  static propTypes = {
    authorCreate: PropTypes.func.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  render() {
    return (
      <div className="b-page__content m-margin_top-normal">
        <AuthorSettings isNew onSubmit={this.props.authorCreate} isReadOnly={this.props.isReadOnlyRoute} />
      </div>
    );
  }
}

export default AuthorNew;
