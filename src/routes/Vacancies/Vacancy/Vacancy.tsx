import React, { Component } from 'react';

import { Vacancy as VacancyType, ProtectedRoute } from 'types';

import VacancySettings from './VacancySettings';
import { VacancyStoreProps, VacancyDispatchProps, VacancyRouteParams } from './Vacancy.connect';

type Props = VacancyStoreProps & VacancyDispatchProps & VacancyRouteParams & ProtectedRoute;

class Vacancy extends Component<Props> {
  public static defaultProps = {
    entry: {
      title: '',
      priority: null,
      enabled: false,
      description: '',
    },
  };

  public componentDidMount(): void {
    const { match: { params: { id } }, entry } = this.props;

    if (!entry.id && id !== 'new') {
      this.props.vacancyRead(Number(this.props.match.params.id));
    }
  }

  private handleSubmit = (data: VacancyType): void => {
    if (data.id) {
      this.props.vacancyUpdate(data);
    } else {
      this.props.vacancyCreate(data);
    }
  };

  public render(): React.ReactNode {
    const { entry, isReadOnlyRoute } = this.props;

    return (
      <div className="b-page__content">
        <VacancySettings entry={entry} onSubmit={this.handleSubmit} isReadOnly={isReadOnlyRoute} />
      </div>
    );
  }
}

export default Vacancy;
