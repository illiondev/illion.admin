import React, { Component, Fragment } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import CodeIcon from '@material-ui/icons/Code';
import Popover from '@material-ui/core/Popover';
import Chip from '@material-ui/core/Chip';

import { LOG_TYPES_TITLE_BY_ID, LOG_TYPES } from 'enums/LogsEnums';

import { Log, LogsFilterQuery } from 'types/logs';

import LogsFilter from './LogsFilter';
import { LogsDispatchProps, LogsStoreProps } from './Logs.connect';

import './Logs.scss';

type Props = LogsStoreProps & LogsDispatchProps;

interface State {
  currentId: number|null;
  popoverAnchorEl: HTMLElement|null;
}

class Logs extends Component<Props, State> {
  public state: State = {
    currentId: null,
    popoverAnchorEl: null,
  };

  public componentDidMount(): void {
    this.props.getLogs();

    if (this.props.logClasses.length === 0) {
      this.props.getLogClasses();
    }
  }

  public componentDidUpdate(prevProps: Props): void {
    if (this.props.entries !== prevProps.entries) {
      window.scrollTo(0, 0);
    }
  }

  private handleChangePage = (e: any, page: number): void => {
    const params = {
      ...this.props.query,
      page: page + 1
    };

    this.props.getLogs(params);
  };

  private handleChangeFilter = (filter: LogsFilterQuery): void => {
    let params: LogsFilterQuery;

    if (filter) {
      params = {
        page: 1,
        ...filter,
      };
    } else { // если фильтра нет, сбрасываем пагинацию
      params = {
        page: 1,
      };
    }

    this.props.getLogs(params);
  };

  private handlePopoverOpen = (currentId: number): any => (event: Event): void => {
    this.setState({
      popoverAnchorEl: event.currentTarget as HTMLElement,
      currentId,
    });
  };

  private handlePopoverClose = (): void => {
    this.setState({ popoverAnchorEl: null, currentId: null });
  };

  private handleFilterEntry = (entry: Log): void => {
    this.handleChangeFilter({
      subject_label: entry.subject_label,
      subject_id: entry.subject_id,
    });
  };

  public render(): React.ReactNode {
    const {
      entries, pager, logClasses, query
    } = this.props;
    const { currentId, popoverAnchorEl } = this.state;
    const isActiveFilter = Object.keys(this.props.query).length > 0;

    return (
      <div className="b-page__content">
        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Логи</Typography>

          <LogsFilter onSubmit={this.handleChangeFilter} current={query} logClasses={logClasses} />

          {entries.length === 0 && isActiveFilter && (
            <Typography variant="h5" align="center" className="m-margin_top-medium">
              По данному критерию ничего не найдено
              <br />
              <br />
            </Typography>
          )}

          {entries.length > 0 && (
            <Fragment>
              <div className="b-table-responsive">
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Пользователь</TableCell>
                      <TableCell align="center">ID</TableCell>
                      <TableCell>Тип данных</TableCell>
                      <TableCell>Действие</TableCell>
                      <TableCell align="right">Дата</TableCell>
                      <TableCell />
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {entries.map((e): React.ReactNode => (
                      <TableRow
                        key={e.id}
                        hover
                      >
                        <TableCell>{e.email}</TableCell>
                        <TableCell align="center">
                          <Tooltip title="Показать все записи с таким ID" placement="top">
                            <Chip
                              onClick={(): void => this.handleFilterEntry(e)}
                              variant="outlined"
                              clickable
                              label={`#${e.subject_id}`}
                            />
                          </Tooltip>
                        </TableCell>
                        <TableCell>{e.subject_name}</TableCell>
                        <TableCell>{LOG_TYPES_TITLE_BY_ID[e.description]}</TableCell>
                        <TableCell align="right">{e.created_at}</TableCell>
                        <TableCell align="right">
                          <Tooltip title="Изменения" placement="top">
                            <IconButton onClick={this.handlePopoverOpen(e.id)}>
                              <CodeIcon />
                            </IconButton>
                          </Tooltip>

                          <Popover
                            id={`log-popover-${e.id}`}
                            classes={{
                              paper: 'b-logs__popover',
                            }}
                            open={Boolean(popoverAnchorEl) && currentId === e.id}
                            anchorEl={popoverAnchorEl}
                            anchorOrigin={{
                              vertical: 'bottom',
                              horizontal: 'left',
                            }}
                            transformOrigin={{
                              vertical: 'top',
                              horizontal: 'right',
                            }}
                            onClose={this.handlePopoverClose}
                            disableRestoreFocus
                          >
                            {e.description === LOG_TYPES.DELETED ? (
                              <pre className="b-logs__diff-old">
                                {JSON.stringify(e.properties.attributes, null, 2)}
                              </pre>
                            ) : (
                              <Fragment>
                                <pre className="b-logs__diff-new">
                                  {JSON.stringify(e.properties.attributes, null, 2)}
                                </pre>
                                <pre className="b-logs__diff-old">{JSON.stringify(e.properties.old, null, 2)}</pre>
                              </Fragment>
                            )}
                          </Popover>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>

              {pager.total_pages > 1 && (
                <TablePagination
                  colSpan={3}
                  component="div"
                  count={pager.total}
                  rowsPerPage={pager.per_page}
                  rowsPerPageOptions={[pager.per_page]}
                  page={pager.current_page - 1}
                  onChangePage={this.handleChangePage}
                />
              )}
            </Fragment>
          )}
        </Paper>
      </div>
    );
  }
}

export default Logs;
