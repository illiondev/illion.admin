import api from 'services/ApiService';
import router from 'services/RouterService';

import { appAutocompleteFetch, appAutocompleteResult, appAutocompleteSetType } from 'lib/app/autocomplete/actions';

import * as types from './actionTypes';

function authorListSuccess(payload) {
  return {
    type: types.AUTHOR_LIST,
    payload,
  };
}

export function getAuthorList(query) {
  return dispatch => api.authorList(query).then((res) => {
    dispatch(authorListSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
    }));
  });
}

function authorCreateSuccess(payload) {
  return {
    type: types.AUTHOR_CREATE,
    payload,
  };
}

export function authorCreate({ photo, ...data }) {
  return dispatch => api.authorCreate(data).then((res) => {
    const entry = res.data.data;
    const uploader = photo ? api.authorPhotoUpdate({ photo, id: entry.id }) : Promise.resolve();

    uploader.then((file) => {
      if (file) {
        entry.photo = file.data;
      }

      dispatch(authorCreateSuccess(entry));
      router.push(router.url(entry.id));
    });
  });
}

function authorUpdateSuccess(payload) {
  return {
    type: types.AUTHOR_UPDATE,
    payload,
  };
}

export function authorUpdate({ photo, ...data }) {
  const uploader = photo ? api.authorPhotoUpdate({ photo, id: data.id }) : Promise.resolve();

  return dispatch => uploader.then(() => api.authorUpdate(data).then((res) => {
    dispatch(authorUpdateSuccess(res.data.data));
    router.push(router.url.AUTHORS());
  }));
}

function authorReadSuccess(payload) {
  return {
    type: types.AUTHOR_READ,
    payload,
  };
}

function authorReadLoader(payload) {
  return {
    type: types.AUTHOR_ENTRY_FETCH,
    payload,
  };
}

export function authorRead(data) {
  return (dispatch) => {
    dispatch(authorReadLoader(true));

    return api.authorRead(data).then((res) => {
      dispatch(authorReadSuccess(res.data.data));
    }).catch((e) => {
      dispatch(authorReadLoader(false));
      throw e;
    });
  };
}

export function authorAutocomplete(query) {
  return (dispatch) => {
    dispatch(appAutocompleteFetch(true));
    dispatch(appAutocompleteSetType('author'));

    return api.authorAutocomplete(query).then((res) => {
      const result = res.data.data.map(entry => ({ id: entry.id, title: `${entry.name} ${entry.surname}` }));

      dispatch(appAutocompleteResult(result));
    }).catch((e) => {
      dispatch(appAutocompleteFetch(false));
      throw e;
    });
  };
}

export function authorUserCreate(id, email) {
  return dispatch => api.authorUserCreate(id, { email }).then((res) => {
    dispatch(authorUpdateSuccess(res.data.data));
  });
}

export function authorUserDelete(id) {
  return dispatch => api.authorUserDelete(id).then((res) => {
    dispatch(authorUpdateSuccess(res.data.data));
  });
}
