import { AxiosPromise } from 'axios';
import { Api, ITokensData } from 'illion-api';

import { filterQueryBuilder } from 'utils';
import {
  ExternalSale,
  ExternalSalesFilterQuery,
  ForgotReq, LoginReq, PagerReq, PayoutReq, RegisterReq, SoonBookReq, SoonCoverReq, Vacancy
} from 'types';
import { StatsFilter } from 'types/stats';
import { LogsFilterQuery } from 'types/logs';

const UPLOAD_TIMEOUT = { timeout: 5 * 60 * 1000 };

type ApiRes = AxiosPromise<any>;

class ApiResources {
  public api: Api;

  public constructor(api: Api) {
    this.api = api;
  }

  // auth endpoints
  public authLogin({ email, password, remember_me }: LoginReq): AxiosPromise<ITokensData> {
    return this.api.login(email, password, remember_me);
  }

  public authRegister({ email, password, name }: RegisterReq): AxiosPromise<ITokensData> {
    return this.api.register(email, password, name);
  }

  public authLogout(): AxiosPromise<void> {
    return this.api.logout();
  }

  public authForgot(data: ForgotReq): AxiosPromise<void> {
    return this.api.post('/auth/password/forgot', data);
  }

  // author endpoints
  public authorList(data: PagerReq|undefined): ApiRes {
    return this.api.get(`/authors${data ? `?page=${data.page}` : ''}`);
  }

  public authorCreate(data: any): ApiRes {
    return this.api.post('/authors', data);
  }

  public authorRead(id: any): ApiRes {
    return this.api.get(`/authors/${id}`);
  }

  public authorUpdate({ id, ...data }: any): ApiRes {
    return this.api.put(`/authors/${id}`, data);
  }

  public authorPhotoUpdate({ id, photo }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', photo, photo.filename);

    return this.api.post(`/authors/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  public authorAutocomplete(query: any): ApiRes {
    return this.api.get(`/authors/autocomplete?query=${query}`);
  }

  public authorUserCreate(id: number, data: any): ApiRes {
    return this.api.post(`/authors/${id}/user`, data);
  }

  public authorUserDelete(id: number): ApiRes {
    return this.api.delete(`/authors/${id}/user`);
  }

  // book endpoints
  public bookList(data: PagerReq|undefined): ApiRes {
    return this.api.get(`/books${data ? `?page=${data.page}` : ''}`);
  }

  public bookCreate(data: any): ApiRes {
    const formData = new FormData();

    Object.keys(data).forEach((key): void => {
      formData.append(key, data[key]);
    });

    return this.api.post('/books', formData, UPLOAD_TIMEOUT);
  }

  public bookRead(id: any): ApiRes {
    return this.api.get(`/books/${id}`);
  }

  public bookUpdate({ id, ...data }: any): ApiRes {
    return this.api.put(`/books/${id}`, data);
  }

  public bookImageUpdate({ book_id, image, category }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', image, image.filename);
    formData.append('category', category);

    return this.api.post(`/books/${book_id}/image`, formData, UPLOAD_TIMEOUT);
  }

  public bookImageDelete({ book_id, id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/image/${id}`);
  }

  public bookPublish(id: number): ApiRes {
    return this.api.put(`/books/${id}/publish`);
  }

  public bookUnpublish(id: number): ApiRes {
    return this.api.put(`/books/${id}/unpublish`);
  }

  public bookPreview(id: number): ApiRes {
    return this.api.post(`/books/${id}/preview`);
  }

  public productAutocomplete(query: string): ApiRes {
    return this.api.get(`/books/products/autocomplete?query=${query}`);
  }

  // book chapters endpoints
  public bookChapterCreate({ book_id, ...data }: any): ApiRes {
    return this.api.post(`/books/${book_id}/chapters`, data);
  }

  public bookChapterUpdate({ id, book_id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/chapters/${id}`, data);
  }

  public bookChapterDelete({ id, book_id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/chapters/${id}`);
  }

  public bookChapterImage({ id, book_id }: any, image: any): ApiRes {
    const formData = new FormData();
    formData.append('image', image.blob(), image.name());

    return this.api.post(`/books/${book_id}/chapters/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  public bookChapterPreview({ id, book_id }: any): ApiRes {
    return this.api.get(`/books/${book_id}/chapters/${id}/preview`, {
      responseType: 'arraybuffer',
    });
  }

  public bookProductCreate({ book_id, ...data }: any): ApiRes {
    const formData = new FormData();

    Object.keys(data).forEach((key): void => {
      formData.append(key, data[key]);
    });

    return this.api.post(`/books/${book_id}/products`, formData, UPLOAD_TIMEOUT);
  }

  public bookProductUpdate({ book_id, id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/products/${id}`, data);
  }

  public bookProductDelete({ book_id, id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/products/${id}`);
  }

  public bookFileUpdate({
    book_id, id, file, type
  }: any): ApiRes {
    const formData = new FormData();
    formData.append('epub', file, file.name);

    return this.api.post(`books/${book_id}/products/${id}/${type}`, formData, UPLOAD_TIMEOUT);
  }

  public bookFileDelete({ book_id, id, type }: any): ApiRes {
    return this.api.delete(`books/${book_id}/products/${id}/${type}`);
  }

  public bookEpubPreviewUpdate({ book_id, id, epub }: any): ApiRes {
    const formData = new FormData();
    formData.append('epub_preview', epub, epub.name);

    return this.api.post(`books/${book_id}/products/${id}/epub_preview`, formData, UPLOAD_TIMEOUT);
  }

  public bookEpubPreviewDelete({ book_id, id }: any): ApiRes {
    return this.api.delete(`books/${book_id}/products/${id}/epub_preview`);
  }

  // book quotes
  public bookQuotesCreate({ book_id, ...data }: any): ApiRes {
    return this.api.post(`/books/${book_id}/quotes`, data);
  }

  public bookQuotesUpdate({ id, book_id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/quotes/${id}`, data);
  }

  public bookQuotesDelete({ id, book_id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/quotes/${id}`);
  }

  // book advantages
  public bookAdvantagesCreate({ book_id, ...data }: any): ApiRes {
    return this.api.post(`/books/${book_id}/advantages`, data);
  }

  public bookAdvantagesUpdate({ id, book_id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/advantages/${id}`, data);
  }

  public bookAdvantagesDelete({ id, book_id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/advantages/${id}`);
  }

  public bookAdvantagesImage({ id, book_id, image }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', image, image.name);

    return this.api.post(`/books/${book_id}/advantages/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  // book features
  public bookFeaturesCreate({ book_id, ...data }: any): ApiRes {
    return this.api.post(`/books/${book_id}/features`, data);
  }

  public bookFeaturesUpdate({ id, book_id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/features/${id}`, data);
  }

  public bookFeaturesDelete({ id, book_id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/features/${id}`);
  }

  public bookFeaturesImage({ id, book_id, image }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', image, image.name);

    return this.api.post(`/books/${book_id}/features/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  // book reviews
  public bookReviewsCreate({ book_id, ...data }: any): ApiRes {
    return this.api.post(`/books/${book_id}/reviews`, data);
  }

  public bookReviewsUpdate({ id, book_id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/reviews/${id}`, data);
  }

  public bookReviewsDelete({ id, book_id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/reviews/${id}`);
  }

  public bookReviewsImage({ id, book_id, image }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', image, image.name);

    return this.api.post(`/books/${book_id}/reviews/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  public announcementRead(): ApiRes {
    return this.api.get('/announcement');
  }

  public announcementCreate(data: any): ApiRes {
    return this.api.post('/announcement', data);
  }

  public announcementUpdate(data: any): ApiRes {
    return this.api.put('/announcement', data);
  }

  public profileMe(): ApiRes {
    return this.api.get('/profile/me');
  }

  public users(params: any): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/users${query}`);
  }

  public userUpdate({ id, ...data }: any): ApiRes {
    return this.api.put(`/users/${id}`, data);
  }

  public userModeratorCreate(data: any): ApiRes {
    return this.api.post('/users/moderator', data);
  }

  public userModeratorDelete(id: number): ApiRes {
    return this.api.delete(`/users/${id}/moderator`);
  }

  public userCommentCreate(id: number, data: any): ApiRes {
    return this.api.post(`/users/${id}/comment`, data);
  }

  public statistics(params: StatsFilter): ApiRes {
    const query: string = filterQueryBuilder(params);

    return this.api.get(`/statistics${query}`);
  }

  public statisticsSummary(params: StatsFilter): ApiRes {
    const query: string = filterQueryBuilder(params);

    return this.api.get(`/statistics/summary${query}`);
  }

  public statisticsProfit(params: StatsFilter): ApiRes {
    const query: string = filterQueryBuilder(params);

    return this.api.get(`/statistics/profit${query}`);
  }

  public orders(params: any): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/orders${query}`);
  }

  public ordersSpreadsheet(params: any): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/orders/spreadsheet${query}`);
  }

  public order(id: any): ApiRes {
    return this.api.get(`/orders/${id}`);
  }

  public orderUpdate({ id, ...data }: any): ApiRes {
    return this.api.put(`/orders/${id}`, data);
  }

  public orderPay(id: any): ApiRes {
    return this.api.post(`/orders/${id}/pay`);
  }

  public orderBillCreate({ id, ...data }: any): ApiRes {
    return this.api.post(`/orders/${id}/bill`, data);
  }

  public payoutsRead(params: PagerReq): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/payouts${query}`);
  }

  public payoutCreate(data: PayoutReq): ApiRes {
    return this.api.post('/payouts', data);
  }

  public payoutUpdate({ id, ...data }: PayoutReq): ApiRes {
    return this.api.put(`/payouts/${id}`, data);
  }

  public logs(params: LogsFilterQuery): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/logs${query}`);
  }

  public logClasses(): ApiRes {
    return this.api.get('/logs/subject-classes');
  }

  public vacanciesRead(): ApiRes {
    return this.api.get('/vacancies');
  }

  public vacancyCreate(data: Omit<Vacancy, 'id'>): ApiRes {
    return this.api.post('/vacancies', data);
  }

  public vacancyRead(id: number): ApiRes {
    return this.api.get(`/vacancies/${id}`);
  }

  public vacancyUpdate({ id, ...data }: Vacancy): ApiRes {
    return this.api.put(`/vacancies/${id}`, data);
  }

  public externalSalesRead(params: ExternalSalesFilterQuery): ApiRes {
    const query = filterQueryBuilder(params);

    return this.api.get(`/external-sales${query}`);
  }

  public externalSaleCreate(data: ExternalSale): ApiRes {
    return this.api.post('/external-sales', data);
  }

  public externalSaleUpdate({ id, ...data }: ExternalSale): ApiRes {
    return this.api.put(`/external-sales/${id}`, data);
  }

  public soonRead(): ApiRes {
    return this.api.get('/soon');
  }

  public soonCreate(data: Partial<SoonBookReq>): ApiRes {
    return this.api.post('/soon', data);
  }

  public soonUpdate({ id, ...data }: Partial<SoonBookReq>): ApiRes {
    return this.api.put(`/soon/${id}`, data);
  }

  public soonDelete(id: number): ApiRes {
    return this.api.delete(`/soon/${id}`);
  }

  public soonImageUpdate({ id, image, category = 'cover' }: SoonCoverReq): ApiRes {
    const formData = new FormData();
    // @ts-ignore
    formData.append('image', image, image.filename);
    formData.append('category', category);

    return this.api.post(`/soon/${id}/image`, formData, UPLOAD_TIMEOUT);
  }

  // bundles
  public bundleAutocomplete(query: any): ApiRes {
    return this.api.get(`/books/bundles/autocomplete?query=${query}`);
  }

  public bundleRead(): ApiRes {
    return this.api.get('/books/bundles');
  }

  public bookBundleCreate(data: any): ApiRes {
    return this.api.post(`/books/${data.book_id}/bundles`, data);
  }

  public bookBundleUpdate({ book_id, id, ...data }: any): ApiRes {
    return this.api.put(`/books/${book_id}/bundles/${id}`, data);
  }

  public bookBundleDelete({ book_id, id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/bundles/${id}`);
  }

  public bookBundleCoverUpdate({
    book_id, id, cover, category
  }: any): ApiRes {
    const formData = new FormData();
    formData.append('image', cover, cover.filename);
    formData.append('category', category);

    return this.api.post(`books/${book_id}/bundles/${id}/images`, formData, UPLOAD_TIMEOUT);
  }

  public bookBundleCoverDelete({ book_id, bundle_id, id }: any): ApiRes {
    return this.api.delete(`books/${book_id}/bundles/${bundle_id}/images/${id}`);
  }

  public bookBundleProductCreate({
    book_id, bundle_id, id, ...data
  }: any): ApiRes {
    return this.api.post(`/books/${book_id}/bundles/${bundle_id}/products/${id}`, data);
  }

  public bookBundleProductDelete({ book_id, bundle_id, id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/bundles/${bundle_id}/products/${id}`);
  }

  public bookBundleAttach({ book_id, id }: any): ApiRes {
    return this.api.post(`/books/${book_id}/bundles/${id}/attach`);
  }

  public bookBundleDetach({ book_id, id }: any): ApiRes {
    return this.api.delete(`/books/${book_id}/bundles/${id}/detach`);
  }
}

export default ApiResources;
