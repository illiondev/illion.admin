import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { formatPrice, getAuthorName } from 'utils';

import { StatsProfit } from 'types/stats';

import StatisticsFilter from 'routes/Statistics/StatisticsFilter';

import { StatisticsProfitDispatchProps, StatisticsProfitStoreProps } from './StatisticsProfit.connect';


class StatisticsProfit extends Component<StatisticsProfitStoreProps & StatisticsProfitDispatchProps> {
  public componentDidMount(): void {
    this.props.statisticsProfit();
  }

  public render(): React.ReactNode {
    const { entries, total } = this.props;

    return (
      <div>
        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Прибыль</Typography>

          <StatisticsFilter onSubmit={this.props.statisticsProfit} />

          {entries.length > 0 && (
            <div className="b-table-responsive">
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Автор</TableCell>
                    <TableCell align="right">Прибыль автора</TableCell>
                    <TableCell align="right">Прибыль издательства</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {entries.map((e: StatsProfit): React.ReactNode => (
                    <TableRow key={e.author_id} hover>
                      <TableCell component="th" scope="row">
                        {getAuthorName(e.author)}
                      </TableCell>
                      <TableCell align="right">{formatPrice(e.authors_profit)}</TableCell>
                      <TableCell align="right">{formatPrice(e.house_profit)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TableCell />
                    <TableCell align="right"><b>{formatPrice(total.authors_profit)}</b></TableCell>
                    <TableCell align="right"><b>{formatPrice(total.house_profit)}</b></TableCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </div>
          )}
        </Paper>
      </div>
    );
  }
}

export default StatisticsProfit;
