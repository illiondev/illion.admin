enum PayoutsActionTypes {
  PAYOUTS_READ = 'PAYOUTS_READ',
  PAYOUT_CREATE = 'PAYOUT_CREATE',
  PAYOUT_UPDATE = 'PAYOUT_UPDATE',
}

export default PayoutsActionTypes;
