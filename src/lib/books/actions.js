import api from 'services/ApiService';
import router from 'services/RouterService';

import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';
import { getAuthorName } from 'utils';

import { appAutocompleteFetch, appAutocompleteResult, appAutocompleteSetType } from 'lib/app/autocomplete/actions';

import * as types from './actionTypes';

function bookListSuccess(payload) {
  return {
    type: types.BOOK_LIST,
    payload,
  };
}

export function getBookList(query) {
  return dispatch => api.bookList(query).then((res) => {
    dispatch(bookListSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
    }));
  });
}

export function bookInit(payload) {
  return {
    type: types.BOOK_INIT,
    payload,
  };
}

function bookCreateSuccess(payload) {
  return {
    type: types.BOOK_CREATE,
    payload,
  };
}

export function bookCreate({
  cover, coverSmall, hardcover, ...data
}) {
  return async (dispatch) => {
    try {
      const res = await api.bookCreate(data);
      const entry = res.data.data;

      if (cover) {
        const coverRes = await api.bookImageUpdate({ image: cover, book_id: entry.id, category: 'cover' });
        entry.cover = coverRes.data;
      }

      if (hardcover) {
        const hardcoverRes = await api.bookImageUpdate({ image: hardcover, book_id: entry.id, category: 'hardcover' });
        entry.hardcover = hardcoverRes.data;
      }

      if (coverSmall) {
        const coverSmallRes = await api.bookImageUpdate({
          image: coverSmall,
          book_id: entry.id,
          category: 'cover-small'
        });
        entry.coverSmall = coverSmallRes.data;
      }

      dispatch(bookCreateSuccess(entry));
      router.push(router.url.BOOK_EDIT(entry.id));
    } catch (e) {
      throw e;
    }
  };
}

function bookUpdateSuccess(payload) {
  return {
    type: types.BOOK_UPDATE,
    payload,
  };
}

export function bookUpdate({
  cover, coverSmall, hardcover, ...data
}) {
  return async (dispatch) => {
    try {
      if (cover) await api.bookImageUpdate({ image: cover, book_id: data.id, category: 'cover' });
      if (hardcover) await api.bookImageUpdate({ image: hardcover, book_id: data.id, category: 'hardcover' });
      if (coverSmall) await api.bookImageUpdate({ image: coverSmall, book_id: data.id, category: 'cover-small' });

      const res = await api.bookUpdate(data);
      const entry = res.data.data;

      dispatch(bookUpdateSuccess(entry));
    } catch (e) {
      throw e;
    }
  };
}

export function bookClear() {
  return {
    type: types.BOOK_CLEAR,
  };
}

function bookReadSuccess(payload) {
  return {
    type: types.BOOK_READ,
    payload,
  };
}

function bookReadLoader(payload) {
  return {
    type: types.BOOK_ENTRY_FETCH,
    payload,
  };
}

export function bookRead(data) {
  return (dispatch) => {
    dispatch(bookReadLoader(true));

    return api.bookRead(data).then((res) => {
      const entry = res.data.data;

      dispatch(bookReadSuccess([entry]));
    }).catch((e) => {
      dispatch(bookReadLoader(false));
      throw e;
    });
  };
}


function bookPublishSuccess(payload) {
  return {
    type: types.BOOK_PUBLISH,
    payload,
  };
}

export function bookPublish(bookId) {
  return dispatch => api.bookPublish(bookId).then((res) => {
    const entry = res.data.data;

    dispatch(bookPublishSuccess({ entry }));
  });
}

function bookUnpublishSuccess(payload) {
  return {
    type: types.BOOK_UNPUBLISH,
    payload,
  };
}

export function bookUnpublish(bookId) {
  return dispatch => api.bookUnpublish(bookId).then((res) => {
    const entry = res.data.data;

    dispatch(bookUnpublishSuccess({ entry }));
  });
}

function bookProductCreateSuccess(payload) {
  return {
    type: types.BOOK_PRODUCT_CREATE,
    payload,
  };
}

function bookProductUpdateSuccess(payload) {
  return {
    type: types.BOOK_PRODUCT_UPDATE,
    payload,
  };
}

export function bookFileDelete(data) {
  return dispatch => api.bookFileDelete(data).then((res) => {
    const product = res.data.data;

    dispatch(bookProductUpdateSuccess(product));
  });
}

export function bookEpubPreviewDelete(data) {
  return dispatch => api.bookEpubPreviewDelete(data).then((res) => {
    const product = res.data.data;

    dispatch(bookProductUpdateSuccess(product));
  });
}


export function bookProductCreate({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  cover, epub, pdf, epub_preview, ...data
}) {
  return async (dispatch) => {
    try {
      const res = await api.bookProductCreate(data);
      let product = res.data.data;

      if (epub) {
        product = await api.bookFileUpdate({
          file: epub, book_id: data.book_id, id: product.id, type: 'epub'
        });
        product = product.data.data;
      }

      if (pdf) {
        product = await api.bookFileUpdate({
          file: pdf, book_id: data.book_id, id: product.id, type: 'pdf'
        });
        product = product.data.data;
      }

      if (epub_preview) {
        product = await api.bookEpubPreviewUpdate({ epub: epub_preview, book_id: data.book_id, id: product.id });
        product = product.data.data;
      }

      dispatch(bookProductCreateSuccess(product));

      router.push(router.url.BOOK_EDIT_PRODUCT(product.book_id, product.id));
    } catch (e) {
      throw e;
    }
  };
}

export function bookProductUpdate({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  cover, epub, pdf, epub_preview, ...data
}) {
  return async (dispatch) => {
    try {
      if (epub) {
        await api.bookFileUpdate({
          file: epub, book_id: data.book_id, id: data.id, type: 'epub'
        });
      }

      if (pdf) {
        await api.bookFileUpdate({
          file: pdf, book_id: data.book_id, id: data.id, type: 'pdf'
        });
      }

      if (epub_preview) await api.bookEpubPreviewUpdate({ epub: epub_preview, book_id: data.book_id, id: data.id });

      let product = await api.bookProductUpdate(data);
      product = product.data.data;

      dispatch(bookProductUpdateSuccess(product));

      router.push(router.url.BOOK_EDIT_PRODUCTS(product.book_id));
    } catch (e) {
      throw e;
    }
  };
}

function bookProductDeleteSuccess(payload) {
  return {
    type: types.BOOK_PRODUCT_DELETE,
    payload,
  };
}

export function bookProductDelete(data) {
  return dispatch => api.bookProductDelete(data).then(() => {
    router.push(router.url.BOOK_EDIT_PRODUCTS(data.book_id));

    dispatch(bookProductDeleteSuccess(data));
  });
}

// quote
function bookQuotesCreateSuccess(payload) {
  return {
    type: types.QUOTES_CREATE,
    payload,
  };
}

export function bookQuotesCreate(data) {
  return dispatch => api.bookQuotesCreate(data).then((res) => {
    const entry = res.data.data;

    dispatch(bookQuotesCreateSuccess({ entry }));
  });
}

function bookQuotesUpdateSuccess(payload) {
  return {
    type: types.QUOTES_UPDATE,
    payload,
  };
}

export function bookQuotesUpdate(data) {
  return dispatch => api.bookQuotesUpdate(data).then((res) => {
    const entry = res.data.data;

    dispatch(bookQuotesUpdateSuccess({ entry }));
  });
}

function bookQuotesDeleteSuccess(payload) {
  return {
    type: types.QUOTES_DELETE,
    payload,
  };
}

export function bookQuotesDelete(data) {
  return dispatch => api.bookQuotesDelete(data).then(() => {
    dispatch(bookQuotesDeleteSuccess({ entry: data }));
  });
}

// advantages
function bookAdvantagesCreateSuccess(payload) {
  return {
    type: types.ADVANTAGES_CREATE,
    payload,
  };
}

function bookAdvantagesUpdateSuccess(payload) {
  return {
    type: types.ADVANTAGES_UPDATE,
    payload,
  };
}

function bookAdvantagesDeleteSuccess(payload) {
  return {
    type: types.ADVANTAGES_DELETE,
    payload,
  };
}

export function bookAdvantagesCreate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookAdvantagesCreate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookAdvantagesImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookAdvantagesCreateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

export function bookAdvantagesUpdate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookAdvantagesUpdate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookAdvantagesImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookAdvantagesUpdateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

export function bookAdvantagesDelete(data) {
  return dispatch => api.bookAdvantagesDelete(data).then(() => {
    dispatch(bookAdvantagesDeleteSuccess({ entry: data }));
  });
}


// features
function bookFeaturesCreateSuccess(payload) {
  return {
    type: types.FEATURES_CREATE,
    payload,
  };
}

export function bookFeaturesCreate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookFeaturesCreate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookFeaturesImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookFeaturesCreateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

function bookFeaturesUpdateSuccess(payload) {
  return {
    type: types.FEATURES_UPDATE,
    payload,
  };
}

export function bookFeaturesUpdate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookFeaturesUpdate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookFeaturesImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookFeaturesUpdateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

function bookFeaturesDeleteSuccess(payload) {
  return {
    type: types.FEATURES_DELETE,
    payload,
  };
}

export function bookFeaturesDelete(data) {
  return dispatch => api.bookFeaturesDelete(data).then(() => {
    dispatch(bookFeaturesDeleteSuccess({ entry: data }));
  });
}

// photos
function bookImageUpdateSuccess(payload) {
  return {
    type: types.IMAGE_UPDATE,
    payload,
  };
}

function bookFragmentUpdateSuccess(payload) {
  return {
    type: types.FRAGMENT_UPDATE,
    payload,
  };
}

function bookImageDeleteSuccess(payload) {
  return {
    type: types.IMAGE_DELETE,
    payload,
  };
}

export function bookImageUpdate(data) {
  return dispatch => api.bookImageUpdate(data).then((res) => {
    if (data.category === 'fragment') {
      dispatch(bookFragmentUpdateSuccess({ entry: res.data }));
    } else {
      dispatch(bookImageUpdateSuccess({ entry: res.data.data }));
    }
  });
}

export function bookImageDelete(data) {
  return dispatch => api.bookImageDelete(data).then(() => {
    dispatch(bookImageDeleteSuccess({ entry: data }));
  });
}


// reviews
function bookReviewsCreateSuccess(payload) {
  return {
    type: types.REVIEWS_CREATE,
    payload,
  };
}

function bookReviewsUpdateSuccess(payload) {
  return {
    type: types.REVIEWS_UPDATE,
    payload,
  };
}

function bookReviewsDeleteSuccess(payload) {
  return {
    type: types.REVIEWS_DELETE,
    payload,
  };
}

export function bookReviewsCreate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookReviewsCreate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookReviewsImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookReviewsCreateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

export function bookReviewsUpdate({ image, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookReviewsUpdate(data);
      const entry = res.data.data;

      if (image) {
        const img = await api.bookReviewsImage({ image, book_id: data.book_id, id: entry.id });
        entry.image = img.data;
      }

      dispatch(bookReviewsUpdateSuccess({ entry }));
    } catch (e) {
      throw e;
    }
  };
}

export function bookReviewsDelete(data) {
  return dispatch => api.bookReviewsDelete(data).then(() => {
    dispatch(bookReviewsDeleteSuccess({ entry: data }));
  });
}

function bookPreviewSuccess(payload) {
  return {
    type: types.BOOK_PREVIEW,
    payload,
  };
}

export function bookPreview(bookId) {
  return dispatch => api.bookPreview(bookId).then((res) => {
    dispatch(bookPreviewSuccess({
      ...res.data,
      bookId,
    }));
  });
}

export function productAutocomplete(query) {
  return (dispatch) => {
    dispatch(appAutocompleteFetch(true));
    dispatch(appAutocompleteSetType('product'));

    return api.productAutocomplete(query).then((res) => {
      const result = res.data.data.map(entry => ({
        id: entry.id,
        // eslint-disable-next-line max-len
        title: `[${BOOK_TYPES_TITLE_BY_ID[entry.type]}] ${entry.product_title || entry.title} - ${getAuthorName(entry.author)}`
      }));

      dispatch(appAutocompleteResult(result));
    }).catch((e) => {
      dispatch(appAutocompleteFetch(false));
      throw e;
    });
  };
}

export function bundleAutocomplete(query, type = 'bundle') {
  return (dispatch) => {
    dispatch(appAutocompleteFetch(true));
    dispatch(appAutocompleteSetType(type));

    return api.bundleAutocomplete(query).then((res) => {
      const result = res.data.data.map(entry => ({
        id: entry.id,
        title: `[${BOOK_TYPES_TITLE_BY_ID[entry.type]}] ${entry.title}`
      }));

      dispatch(appAutocompleteResult(result));
    }).catch((e) => {
      dispatch(appAutocompleteFetch(false));
      throw e;
    });
  };
}


// BUNDLES
function bundleReadSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_READ,
    payload,
  };
}

export function bundleRead() {
  return dispatch => api.bundleRead().then((res) => {
    dispatch(bundleReadSuccess(res.data.data));
  });
}

function bookBundleCreateSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_CREATE,
    payload,
  };
}

export function bookBundleCreate({ cover, ...data }) {
  return async (dispatch) => {
    try {
      const res = await api.bookBundleCreate(data);
      const bundle = res.data.data;

      if (cover) {
        const bundleCover = await api.bookBundleCoverUpdate({
          cover, book_id: data.book_id, id: bundle.id, category: 'cover'
        });

        bundle.cover = bundleCover.data.data;
      }

      dispatch(bookBundleCreateSuccess(bundle));

      router.push(router.url.BOOK_EDIT_BUNDLE(data.book_id, bundle.id));
    } catch (e) {
      throw e;
    }
  };
}

function bookBundleUpdateSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_UPDATE,
    payload,
  };
}

export function bookBundleUpdate({ cover, ...data }) {
  return async (dispatch) => {
    try {
      if (cover) {
        await api.bookBundleCoverUpdate({
          cover, book_id: data.book_id, id: data.id, category: 'cover'
        });
      }

      let bundle = await api.bookBundleUpdate(data);
      bundle = bundle.data.data;

      dispatch(bookBundleUpdateSuccess(bundle));

      router.push(router.url.BOOK_EDIT_BUNDLES(data.book_id));
    } catch (e) {
      throw e;
    }
  };
}


function bookBundleDeleteSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_DELETE,
    payload,
  };
}

export function bookBundleDelete(data) {
  return dispatch => api.bookBundleDelete(data).then(() => {
    dispatch(bookBundleDeleteSuccess(data));
  });
}

function bookBundleCoverDeleteSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_COVER_DELETE,
    payload,
  };
}


export function bookBundleCoverDelete(params) {
  return dispatch => api.bookBundleCoverDelete(params).then(() => dispatch(bookBundleCoverDeleteSuccess(params)));
}

// BUNDLE PRODUCTS
function bookBundleProductCreateSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_PRODUCT_CREATE,
    payload,
  };
}

function bookBundleProductDeleteSuccess(payload) {
  return {
    type: types.BOOK_BUNDLE_PRODUCT_DELETE,
    payload,
  };
}

export function bookBundleProductCreate(data) {
  return dispatch => api.bookBundleProductCreate(data).then((res) => {
    dispatch(bookBundleProductCreateSuccess(res.data.data));
  });
}

export function bookBundleProductDelete(data) {
  return dispatch => api.bookBundleProductDelete(data).then((res) => {
    dispatch(bookBundleProductDeleteSuccess(res.data.data));
  });
}

export function bookBundleAttach(data) {
  return dispatch => api.bookBundleAttach(data).then((res) => {
    dispatch(bookUpdateSuccess(res.data.data));
  });
}

export function bookBundleDetach(data) {
  return dispatch => api.bookBundleDetach(data).then((res) => {
    router.push(router.url.BOOK_EDIT_BUNDLES(data.book_id));
    dispatch(bookUpdateSuccess(res.data.data));
  });
}
