import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button/Button';
import ScreenRotationIcon from '@material-ui/icons/ScreenRotation';

import { DEVICES } from 'enums/DevicesEnum';

const devicesById = DEVICES.reduce((mem, d) => ({ ...mem, [d.id]: d }), {});

class ViewerDevices extends Component {
  static propTypes = {
    defaultSize: PropTypes.shape({
      width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
    onChange: PropTypes.func.isRequired,
  }

  static defaultProps = {
    defaultSize: {}
  }

  state = {
    size: 'default',
    isRotated: false
  }

  handleChangeSize = (e) => {
    const device = devicesById[e.currentTarget.dataset.device];
    const { width, height } = device.id !== 'default' ? device : this.props.defaultSize;

    this.setState({ size: device.id, isRotated: false });

    this.props.onChange({ width, height, isRotated: false });
  }

  handleRotate = () => {
    this.setState(prevState => ({
      isRotated: !prevState.isRotated
    }), () => {
      if (this.state.size !== 'default') {
        const device = devicesById[this.state.size];
        let { width, height } = device;

        if (this.state.isRotated) {
          [width, height] = [height, width];
        }

        this.props.onChange({ width, height, isRotated: this.state.isRotated });
      }
    });
  }

  render() {
    const { size } = this.state;

    return (
      <div>
        {size !== 'default' && (
          <Button onClick={this.handleRotate}>
            <ScreenRotationIcon />
          </Button>
        )}
        {DEVICES.map(d => (
          <Button
            onClick={this.handleChangeSize}
            data-device={d.id}
            color={size === d.id && size !== 'default' ? 'secondary' : 'default'}
            key={d.id}
          >
            {d.title}
            {Boolean(d.width) && (
              <small>
                &nbsp;
                {`(${d.width} x ${d.height})`}
              </small>
            )}
          </Button>
        ))}
      </div>
    );
  }
}

export default ViewerDevices;
