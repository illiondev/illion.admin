import { connect } from 'react-redux';

import { bookCreate, bookUpdate, bookPreview } from 'lib/books/actions';
import { authorAutocomplete } from 'lib/authors/actions';
import { getBooksById } from 'lib/books/selectors';
import { snackbarShow } from 'lib/app/snackbar/actions';

import BookSettingsBasic from './BookSettingsBasic';

function mapStateToProps(state, ownProps) {
  return {
    preview: state.books.preview,
    entry: getBooksById(state)[ownProps.match.params.id],
  };
}

export default connect(mapStateToProps, {
  bookCreate,
  bookUpdate,
  authorAutocomplete,
  bookPreview,
  snackbarShow,
})(BookSettingsBasic);
