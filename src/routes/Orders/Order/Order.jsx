import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import { SHIPTOR_ORDER_VIEW_URL } from 'config';
import { PAYMENT_STATUS_TITLE_BY_ID, STATUS_WAITING_FOR_PAYMENT, STATUS_CANCELED } from 'enums/PaymentEnums';

import OrderInfo from './OrderInfo';
import CustomerForm from './CustomerForm';

class Order extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number.isRequired,
      shiptor_id: PropTypes.number,
      status: PropTypes.number.isRequired,
    }),
    customerForm: PropTypes.shape({}),
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string,
      }).isRequired,
    }).isRequired,
    getOrder: PropTypes.func.isRequired,
    orderUpdate: PropTypes.func.isRequired,
    orderPay: PropTypes.func.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    entry: undefined,
    customerForm: undefined,
  };

  state = {
    isRawOpen: false,
    viewMode: 'info'
  };

  componentDidMount() {
    if (!this.props.entry) {
      this.props.getOrder(this.props.match.params.id);
    }
  }

  handleToggleRaw = () => this.setState(prevState => ({ isRawOpen: !prevState.isRawOpen }))

  handleEditCustomer = () => this.setState({ viewMode: 'edit' })

  handleCloseEdit = () => this.setState({ viewMode: 'info' })

  handleUpdateOrder = (data) => {
    this.props.orderUpdate(data);
    this.handleCloseEdit();
  }

  handleRetryPay = () => {
    this.props.orderPay(this.props.entry.id);
  }

  render() {
    const { entry, customerForm, isReadOnlyRoute } = this.props;
    const { isRawOpen, viewMode } = this.state;
    const isInfoMode = viewMode === 'info';

    if (!entry) return null;

    return (
      <div className="b-page__content">
        <Paper className="b-paper">
          <Typography variant="h5" className="m-margin_bottom-medium" align="center">
            {`Заказ #${entry.id} [${PAYMENT_STATUS_TITLE_BY_ID[entry.status]}]`}
          </Typography>

          {isInfoMode && <OrderInfo entry={entry} isReadOnly={isReadOnlyRoute} />}

          {viewMode === 'edit' && (
            <CustomerForm
              entry={customerForm}
              onSubmit={this.handleUpdateOrder}
              onCancel={this.handleCloseEdit}
            />
          )}

          {!isReadOnlyRoute && isInfoMode && (
            <Fragment>
              {Boolean(entry.shiptor_id) && (
                <Typography variant="caption" color="error" className="m-margin_bottom-normal">
                  заказ уже отправлен в шиптор и должен редактироваться там
                </Typography>
              )}

              <Grid container spacing={24}>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleEditCustomer}
                    disabled={Boolean(entry.shiptor_id)}
                  >
                    Редактировать данные покупателя
                  </Button>
                </Grid>

                {Boolean(entry.shiptor_id) && (
                  <Grid item>
                    <Button
                      variant="contained"
                      color="default"
                      href={`${SHIPTOR_ORDER_VIEW_URL}/${entry.shiptor_id}`}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Заказ в Shiptor
                    </Button>
                  </Grid>
                )}

                {[STATUS_WAITING_FOR_PAYMENT, STATUS_CANCELED].includes(entry.status) && (
                  <Grid item>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={this.handleRetryPay}
                    >
                      Провести оплату
                    </Button>
                  </Grid>
                )}
              </Grid>


              <div className="m-margin_top-normal">
                <Button
                  color="default"
                  onClick={this.handleToggleRaw}
                >
                  {isRawOpen ? 'Закрыть raw данные' : 'Показать raw данные'}
                </Button>

                {isRawOpen && (
                  <Fragment>
                    <pre>
                      {JSON.stringify(entry, ' ', 4)}
                    </pre>
                    <Typography variant="button" onClick={this.handleToggleRaw}>
                      Закрыть raw данные
                    </Typography>
                  </Fragment>
                )}
              </div>
            </Fragment>
          )}
        </Paper>
      </div>
    );
  }
}

export default Order;
