import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { composeValidators, phoneFormat, required } from 'utils/validators';
import FormFieldText from 'components/FormFieldText';

class CustomerForm extends PureComponent {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number.isRequired,
      surname: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      patronimic: PropTypes.string,
      email: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired,
      postal_code: PropTypes.string,
      country: PropTypes.string.isRequired,
      comment: PropTypes.string.isRequired,
      region: PropTypes.string,
      settlement: PropTypes.string,
      address: PropTypes.string,
      address2: PropTypes.string,
    }).isRequired,
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  handleSubmit = (formData) => {
    this.props.onSubmit(formData);
  }

  render() {
    return (
      <Fragment>
        <Typography variant="h6" gutterBottom>
          Редактирование данных покупателя
        </Typography>

        <Form
          onSubmit={this.handleSubmit}
          subscription={{ submitting: true, pristine: true }}
          initialValues={this.props.entry}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Grid container spacing={24}>
                <Grid item md={4}>
                  <Field
                    name="surname"
                    label="Фамилия"
                    fullWidth
                    component={FormFieldText}
                    validate={required}
                  />
                </Grid>
                <Grid item md={4}>
                  <Field
                    name="name"
                    label="Имя"
                    fullWidth
                    component={FormFieldText}
                    validate={required}
                  />
                </Grid>

                <Grid item xs={4} md={4}>
                  <Field
                    name="patronimic"
                    label="отчество"
                    fullWidth
                    component={FormFieldText}
                  />
                </Grid>

                <Grid item md={4}>
                  <Field
                    name="email"
                    label="Email*"
                    fullWidth
                    component={FormFieldText}
                    disabled
                  />
                </Grid>
                <Grid item md={4}>
                  <Field
                    name="phone"
                    label="Телефон"
                    fullWidth
                    component={FormFieldText}
                    validate={composeValidators(required, phoneFormat)}
                  />
                </Grid>

                <Grid item xs={4} md={4}>
                  <Field
                    name="postal_code"
                    label="Индекс"
                    fullWidth
                    component={FormFieldText}
                  />
                </Grid>

                <Grid item md={4}>
                  <Field
                    name="country"
                    label="Страна"
                    fullWidth
                    component={FormFieldText}
                    disabled
                  />
                </Grid>
                <Grid item md={4}>
                  <Field
                    name="region"
                    label="Область"
                    fullWidth
                    component={FormFieldText}
                    disabled
                  />
                </Grid>

                <Grid item xs={4} md={4}>
                  <Field
                    name="settlement"
                    label="Город"
                    fullWidth
                    component={FormFieldText}
                    disabled
                  />
                </Grid>


                <Grid item md={8}>
                  <Grid container spacing={24}>
                    <Grid item xs={12}>
                      <Field
                        name="address"
                        label="Cтрока адреса 1"
                        fullWidth
                        component={FormFieldText}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        name="address2"
                        label="Cтрока адреса 2"
                        fullWidth
                        component={FormFieldText}
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item md={4}>
                  <Field
                    name="comment"
                    label="Комментарий к заказу"
                    fullWidth
                    component={FormFieldText}
                    multiline
                    rows={5}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Button
                    type="button"
                    variant="contained"
                    color="default"
                    onClick={this.props.onCancel}
                    className="m-margin_top-medium"
                  >
                    Отмена
                  </Button>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className="m-margin_left-normal m-margin_top-medium"
                  >
                    Сохранить
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </Fragment>
    );
  }
}

export default CustomerForm;
