import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';

class ProductTypeBook extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Fragment>
        <Grid item xs={12}>
          <Typography variant="subtitle1" className="m-margin_top-normal">
              Информация для доставки (Shiptor)
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={24} alignItems="flex-end">
            <Grid item xs={11} md={2}>
              <Field
                name="article"
                label="Артикул книги"
                validate={required}
                fullWidth
                component={FormFieldText}
                disabled={isReadOnly}
              />
            </Grid>
            <Grid item xs="auto" md={1} />
            <Grid item xs={6} sm={6} md={2}>
              <Typography variant="caption" gutterBottom>Габариты книги:</Typography>
              <Field
                name="weight"
                type="number"
                label="Вес, кг"
                validate={required}
                fullWidth
                component={FormFieldText}
                disabled={isReadOnly}
              />
            </Grid>
            <Grid item xs={6} sm={6} md={2}>
              <Field
                name="width"
                type="number"
                label="Ширина, см"
                validate={required}
                fullWidth
                component={FormFieldText}
                disabled={isReadOnly}
              />
            </Grid>
            <Grid item xs={6} sm={6} md={2}>
              <Field
                name="height"
                type="number"
                label="Высота, см"
                validate={required}
                fullWidth
                component={FormFieldText}
                disabled={isReadOnly}
              />
            </Grid>
            <Grid item xs={6} sm={6} md={2}>
              <Field
                name="length"
                type="number"
                validate={required}
                label="Длинна, см"
                fullWidth
                component={FormFieldText}
                disabled={isReadOnly}
              />
            </Grid>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

export default ProductTypeBook;
