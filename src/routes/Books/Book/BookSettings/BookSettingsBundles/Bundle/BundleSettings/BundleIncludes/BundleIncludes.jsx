import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Table from '@material-ui/core/Table';
import FormBundleIncludes from './FormBundleIncludes';
import BundleIncludesItem from './BundleIncludesItem';

class BundleIncludes extends Component {
  static propTypes = {
    bookId: PropTypes.number,
    bundleId: PropTypes.number,
    isReadOnly: PropTypes.bool.isRequired,
    productAutocomplete: PropTypes.func.isRequired,
    includes: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
    })),
    bookBundleProductCreate: PropTypes.func.isRequired,
    bookBundleProductDelete: PropTypes.func.isRequired,
  };

  static defaultProps = {
    includes: [],
    bundleId: undefined,
    bookId: undefined,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.includes !== prevState.includesProp) {
      return {
        modalIsOpen: false,
        includesProp: nextProps.includes,
        entry: undefined,
      };
    }

    return null;
  }

  state = {
    modalIsOpen: false,
    includesProp: undefined,
    entry: undefined,
  };

  handleSubmit = ({
    product, is_surprise, price, authors_percent
  }) => {
    const { bookId, bundleId } = this.props;
    const data = {
      book_id: bookId,
      bundle_id: bundleId,
      is_surprise: Boolean(is_surprise),
      price: Number(price),
      authors_percent: Number(authors_percent),
      id: product.id,
    };

    this.props.bookBundleProductCreate(data);
  };

  handleShowForm = entry => this.setState({ modalIsOpen: true, entry });

  handleModalClose = () => this.setState({ modalIsOpen: false, entry: undefined });

  handleDeleteProduct = (id) => {
    const { bookId, bundleId } = this.props;

    this.props.bookBundleProductDelete({ book_id: bookId, bundle_id: bundleId, id });
  };

  render() {
    const { isReadOnly, includes, bundleId } = this.props;
    const { modalIsOpen, entry } = this.state;

    return (
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Typography variant="subtitle1" gutterBottom>
            Содержимое комплекта
          </Typography>

          <FormBundleIncludes
            entry={entry}
            onClose={this.handleModalClose}
            onSave={this.handleSubmit}
            onProductAutocomplete={this.props.productAutocomplete}
            open={modalIsOpen}
            includes={includes}
          />

          {bundleId ? (
            <div className="b-table-responsive">
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell padding="none" />
                    <TableCell>Название товара</TableCell>
                    <TableCell align="right">Цена продажи, руб.</TableCell>
                    <TableCell align="right">Процент автора, %</TableCell>
                    <TableCell />
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {includes.map(p => (
                    <BundleIncludesItem
                      key={p.id}
                      product={p}
                      onEdit={this.handleShowForm}
                      onDelete={this.handleDeleteProduct}
                      isReadOnly={isReadOnly}
                    />
                  ))}
                </TableBody>
              </Table>
            </div>
          ) : (
            <Typography variant="caption">
              Чтобы добавить вложения, вначале нужно создать комплект
            </Typography>
          )}

          {!isReadOnly && bundleId && (
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={() => this.handleShowForm()}
              className="m-margin_top-small"
            >
              Добавить товар в комплект
            </Button>
          )}
        </Grid>
      </Grid>
    );
  }
}

export default BundleIncludes;
