import { MenuItem } from 'types';

import router from 'services/RouterService';

const { url } = router;

export const MENU: MenuItem[] = [
  {
    title: 'Книги',
    url: url.BOOKS(),
  },
  {
    title: 'Авторы',
    url: url.AUTHORS(),
  },
  {
    title: 'Контент',
    url: '',
    submenu: [
      {
        title: 'Вакансии',
        url: url.VACANCIES(),
      },
      {
        title: 'Объявление',
        url: url.CONTENT(),
      },
      {
        title: 'Скоро в продаже',
        url: url.SOON(),
      },
    ]
  },
  {
    title: 'Пользователи',
    url: url.USERS(),
  },
  {
    title: 'Заказы',
    url: url.ORDERS(),
  },
  {
    title: 'Продажи',
    url: '',
    submenu: [
      {
        title: 'Статистика',
        url: url.STATISTICS(),
      },
      {
        title: 'Внешние продажи',
        url: url.EXTERNAL_SALES(),
      },
      {
        title: 'Прибыль',
        url: url.STATISTICS_PROFIT(),
      },
    ]
  },
  {
    title: 'Выплаты',
    url: url.PAYOUTS(),
  },
  {
    title: 'Логи',
    url: url.LOGS(),
  },
];
