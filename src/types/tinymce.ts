export interface BlobInfoData {
  id?: string;
  name?: string;
  blob: Blob;
  base64: string;
  blobUri?: string;
  uri?: string;
}

export interface BlobInfo {
  id: () => string;
  name: () => string;
  filename: () => string;
  blob: () => Blob;
  base64: () => string;
  blobUri: () => string;
  uri: () => string;
}

// copy-paste from https://github.com/tinymce/tinymce/tree/master/src/core/main/ts
export interface BlobCache {
  create: (o: string | BlobInfoData, blob?: Blob, base64?: string, filename?: string) => BlobInfo;
  add: (blobInfo: BlobInfo) => void;
  get: (id: string) => BlobInfo;
  getByUri: (blobUri: string) => BlobInfo;
  findFirst: (predicate: (blobInfo: BlobInfo) => boolean) => any;
  removeByUri: (blobUri: string) => void;
  destroy: () => void;
}


export type FilePickerCallback = (callback: Function, value: any, meta?: Record<string, any>) => void;

// eslint-disable-next-line max-len
export type UploadHandler = (blobInfo: BlobInfo, success: (url: string) => void, failure: (err: string) => void, progress?: (percent: number) => void) => void;

// end copy-paste

export interface EditorTypes {
  setContent(arg: string): void;
  settings: {
    images_upload_handler: UploadHandler;
    file_picker_callback(callback: FilePickerCallback): void;
  };
  options: Record<string, any>;
  editorUpload: {
    blobCache: BlobCache;
  };
}

