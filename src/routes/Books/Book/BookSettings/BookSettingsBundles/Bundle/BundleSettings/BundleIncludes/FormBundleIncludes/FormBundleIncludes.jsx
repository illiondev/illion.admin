import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcardTwoTone';

import {
  composeValidators, requiredId, required, maxNumber
} from 'utils/validators';

import FormFieldAutoComplete from 'components/FormFieldAutoComplete';
import FormFieldSwitch from 'components/FormFieldSwitch';
import FormFieldText from 'components/FormFieldText';

class FormBundleIncludes extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      price: PropTypes.number,
      authors_percent: PropTypes.number,
      is_surprise: PropTypes.bool,
    }),
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onProductAutocomplete: PropTypes.func.isRequired,
    includes: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
    })),
  };

  static defaultProps = {
    entry: {},
    includes: [],
  };

  form = null;

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  };

  render() {
    const {
      includes, open, entry
    } = this.props;

    return (
      <Dialog onClose={this.props.onClose} open={open} maxWidth="sm" fullWidth>
        <DialogTitle>{`${entry.id ? 'Редактирование' : 'Добавление'} продукта`}</DialogTitle>
        <DialogContent>
          <Form
            onSubmit={this.props.onSave}
            initialValues={{
              price: entry.price,
              authors_percent: entry.authors_percent,
              is_surprise: entry.is_surprise,
              product: {
                id: entry.id,
                title: entry.title,
              }
            }}
            subscription={{ submitting: true, pristine: true }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} ref={(el) => { this.form = el; }}>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Field
                      autocompleteHandler={this.props.onProductAutocomplete}
                      label="Название товара"
                      name="product"
                      skip={[...includes.map(i => i.id)]}
                      onChoose={this.handleChooseProduct}
                      component={FormFieldAutoComplete}
                      validate={requiredId}
                      disabled={Boolean(entry.id)}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      label="Цена товара, руб.*"
                      name="price"
                      type="number"
                      component={FormFieldText}
                      validate={required}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      label="Процент автора, %*"
                      name="authors_percent"
                      type="number"
                      component={FormFieldText}
                      validate={composeValidators(required, maxNumber(100))}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Tooltip title="Это бонусный товар" placement="top">
                      <Field
                        name="is_surprise"
                        type="checkbox"
                        component={FormFieldSwitch}
                        label={<CardGiftcardIcon color="secondary" />}
                      />
                    </Tooltip>
                  </Grid>
                </Grid>
              </form>
            )}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={this.props.onClose} color="default">
            Отмена
          </Button>
          <Button onClick={this.handleSave} color="primary" variant="contained">
            {entry.id ? 'Сохранить' : 'Добавить'}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default FormBundleIncludes;
