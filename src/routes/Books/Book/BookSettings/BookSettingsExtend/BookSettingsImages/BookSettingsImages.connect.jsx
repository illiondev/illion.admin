import { connect } from 'react-redux';

import { bookImageDelete, bookImageUpdate } from 'lib/books/actions';

import BookSettingsImages from './BookSettingsImages';

export default connect(null, {
  bookImageUpdate,
  bookImageDelete,
})(BookSettingsImages);
