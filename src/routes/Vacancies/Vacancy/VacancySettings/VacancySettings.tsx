import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Field } from 'react-final-form';

import Button, { ButtonProps } from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { required } from 'utils/validators';
import { Vacancy } from 'types';

import router from 'services/RouterService';

import FormFieldText from 'components/FormFieldText';
import FormFieldSwitch from 'components/FormFieldSwitch';
import FormFieldEditor from 'components/FormFieldEditor';

interface VacancySettingsProps {
  isReadOnly: boolean;
  entry: Vacancy;
  onSubmit(formData: Vacancy): void;
}

class VacancySettings extends Component<VacancySettingsProps> {
  private handleSubmit = (values: object): void => {
    const data = {
      ...values,
      // @ts-ignore
      priority: Number(values.priority),
    };

    this.props.onSubmit(data as Vacancy);
  };

  public render(): React.ReactNode {
    const { entry, isReadOnly } = this.props;
    const isNew = !entry.id;

    return (
      <Paper className="b-paper">
        <Typography variant="h4" align="center">
          {isNew ? 'Новая вакансия' : 'Редактирование вакансии'}
        </Typography>

        <Form
          onSubmit={this.handleSubmit}
          subscription={{ submitting: true, pristine: true }}
          initialValues={entry}
          render={({ handleSubmit }): React.ReactNode => (
            <form onSubmit={handleSubmit}>
              <Grid container spacing={24}>
                <Grid item xs={12} sm={12} md={7}>
                  <Field
                    type="text"
                    name="title"
                    label="Заголовок"
                    fullWidth
                    component={FormFieldText}
                    validate={required}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={6} md="auto">
                  <Field
                    name="priority"
                    label="Приоритет"
                    fullWidth
                    component={FormFieldText}
                    type="number"
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={6} md="auto">
                  <Field
                    name="enabled"
                    type="checkbox"
                    label="Активна"
                    component={FormFieldSwitch as any}
                    disabled={isReadOnly}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Typography variant="caption" gutterBottom>Описание</Typography>
                  <Field
                    name="description"
                    component={FormFieldEditor as any}
                    options={{
                      plugins: 'code lists textcolor colorpicker',
                      height: 300,
                    }}
                    validate={required}
                    disabled={isReadOnly}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Grid container spacing={24}>
                    <Grid item>
                      <Button
                        color="default"
                        component={
                          (props: ButtonProps): React.ReactElement => <Link to={router.url.VACANCIES()} {...props} />
                        }
                      >
                        Назад
                      </Button>
                    </Grid>

                    {!isReadOnly && (
                      <Grid item className="m-margin_left-auto">
                        <Button
                          type="submit"
                          variant="contained"
                          color="primary"

                        >
                          {isNew ? 'Создать' : 'Сохранить'}
                        </Button>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </Paper>
    );
  }
}

export default VacancySettings;
