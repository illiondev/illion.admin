import { connect } from 'react-redux';

import { SoonBook } from 'types';

import { RootState } from 'lib';
import { authorAutocomplete } from 'lib/authors/actions';
import {
  soonRead, soonCreate, soonUpdate, soonDelete
} from 'lib/soon/actions';

import Soon from './Soon';

export interface SoonStoreProps {
  entries: SoonBook[];
}

export interface SoonDispatchProps {
  soonRead: typeof soonRead;
  soonCreate: typeof soonCreate;
  soonUpdate: typeof soonUpdate;
  soonDelete: typeof soonDelete;
  authorAutocomplete: typeof authorAutocomplete;
}

function mapStateToProps(state: RootState): SoonStoreProps {
  return {
    entries: state.soon.entries,
  };
}

export default connect<SoonStoreProps, SoonDispatchProps, {}, RootState>(mapStateToProps, {
  soonRead,
  soonCreate,
  soonUpdate,
  soonDelete,
  authorAutocomplete,
})(
  // @ts-ignore
  Soon
);
