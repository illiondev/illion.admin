import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import LinkIcon from '@material-ui/icons/Link';

import { PUBLIC_URL } from 'config';
import { getAuthorName, copyToClipboard } from 'utils';
import { required, requiredId } from 'utils/validators';
import { BOOK_THEMES } from 'enums/BookThemesEnum';

import FormFieldText from 'components/FormFieldText';
import FormFieldFile from 'components/FormFieldFile';
import FormFieldEditor from 'components/FormFieldEditor';
import FormFieldDate from 'components/FormFieldDate';
import FormFieldAutoComplete from 'components/FormFieldAutoComplete';
import FormFieldSelect from 'components/FormFieldSelect';
import router from 'services/RouterService';

class BookSettingsBasic extends Component {
  static propTypes = {
    isNew: PropTypes.bool,
    isReadOnlyRoute: PropTypes.bool.isRequired,
    entry: PropTypes.shape({
      title: PropTypes.string.isRequired,
      publication_date: PropTypes.string.isRequired,
      tagline: PropTypes.string.isRequired,
      preview: PropTypes.string.isRequired,
      theme_id: PropTypes.string.isRequired,
      priority: PropTypes.number,
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      author: PropTypes.shape({
        data: PropTypes.shape({
          id: PropTypes.number,
          name: PropTypes.string,
          surname: PropTypes.string,
        })
      }),
      cover: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        })
      }),
      coverSmall: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        })
      }),
      hardcover: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        })
      }),
    }),
    preview: PropTypes.shape({
      preview_hash: PropTypes.string,
      preview_hash_expire: PropTypes.number,
      bookId: PropTypes.number,
    }),
    authorAutocomplete: PropTypes.func.isRequired,
    bookCreate: PropTypes.func.isRequired,
    bookUpdate: PropTypes.func.isRequired,
    bookPreview: PropTypes.func.isRequired,
    snackbarShow: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isNew: false,
    preview: undefined,
    entry: {
      id: 'new',
      title: '',
      author: { data: {} },
      preview: '',
      publication_date: '',
      tagline: '',
      theme_id: '',
    },
  };


  handlePreview = () => this.props.bookPreview(this.props.entry.id);

  handleCopyPreviewLink = () => {
    copyToClipboard(`${PUBLIC_URL}/bpv/${this.props.preview.preview_hash}`);
    this.props.snackbarShow({ type: 'info', message: 'Ссылка скопирована в буфер' });
  };

  handleSubmit = (formData) => {
    const { isNew } = this.props;
    const {
      id, author, cover, coverSmall,
      title, preview, publication_date, tagline = '',
      author_id, theme_id, hardcover, priority
    } = formData;

    const data = {
      title,
      tagline,
      preview,
      publication_date,
      author_id: Number(author.id) || author_id,
      theme_id,
      priority,
    };

    if (id && id !== 'new') {
      data.id = id;
    }

    // если есть поле data, то в cover лежит родное значение, полученное с сервера
    if (!cover.data) {
      data.cover = cover;
    }

    // если есть поле data, то в coverSmall лежит родное значение, полученное с сервера
    if (!coverSmall.data) {
      data.coverSmall = coverSmall;
    }

    // если есть поле data, то в hardcover лежит родное значение, полученное с сервера
    if (hardcover && !hardcover.data) {
      data.hardcover = hardcover;
    }

    if (isNew) {
      this.props.bookCreate(data);
    } else {
      this.props.bookUpdate(data);
    }
  };

  render() {
    const {
      isNew, entry, isReadOnlyRoute, preview
    } = this.props;

    return (
      <Fragment>
        <Form
          onSubmit={this.handleSubmit}
          subscription={{ submitting: true, pristine: true }}
          initialValues={{
            ...entry,
            author: entry.author.data.id ? {
              id: entry.author.data.id,
              title: getAuthorName(entry.author),
            } : entry.author,
          }}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <Field
                    name="title"
                    label="Название*"
                    fullWidth
                    component={FormFieldText}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="tagline"
                    label="Слоган"
                    fullWidth
                    component={FormFieldText}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={3}>
                  <Field
                    autocompleteHandler={this.props.authorAutocomplete}
                    label="Автор*"
                    name="author"
                    component={FormFieldAutoComplete}
                    validate={requiredId}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={3}>
                  <Field
                    name="theme_id"
                    label="Стиль оформления*"
                    options={BOOK_THEMES}
                    fullWidth
                    component={FormFieldSelect}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={3}>
                  <Field
                    name="publication_date"
                    label="Дата издания*"
                    fullWidth
                    type="date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    component={FormFieldDate}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={3}>
                  <Field
                    name="priority"
                    label="Приоритет в сортировке"
                    fullWidth
                    component={FormFieldText}
                    type="number"
                    disabled={isReadOnlyRoute}
                    validate={required}
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <Field
                    name="coverSmall"
                    isImageFile
                    label="Обложка(список книг)*"
                    component={FormFieldFile}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <Field
                    name="cover"
                    isImageFile
                    label="Обложка(страница книги)*"
                    component={FormFieldFile}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <Field
                    name="hardcover"
                    isImageFile
                    label="Обложка мягкая(страница книги)"
                    component={FormFieldFile}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                <Grid item xs={12}>
                  <Field
                    name="preview"
                    label="Аннотация*"
                    component={FormFieldEditor}
                    options={{
                      plugins: 'code lists textcolor colorpicker',
                      height: 300,
                    }}
                    validate={required}
                    disabled={isReadOnlyRoute}
                  />
                </Grid>

                {!isReadOnlyRoute && (
                  <Grid item xs={12}>
                    <Grid container spacing={24}>
                      <Grid item>
                        <Button
                          color="default"
                          component={Link}
                          to={router.url.BOOKS()}
                        >
                          Назад
                        </Button>
                      </Grid>

                      {!isNew && (
                        <Grid item>
                          <Button
                            type="button"
                            variant="outlined"
                            onClick={this.handlePreview}
                          >
                            <VisibilityIcon className="m-margin_right-small" />
                            Предпросмотр
                          </Button>
                        </Grid>
                      )}

                      {preview && preview.bookId === entry.id && (
                        <Grid item>
                          <div className="b-book-editor__preview">
                            <Button
                              type="button"
                              variant="outlined"
                              onClick={this.handleCopyPreviewLink}
                            >
                              <LinkIcon />
                            </Button>
                            <Typography variant="caption" component="span" className="m-margin_left-small">
                              {`Ссылка действительна в течении ${preview.preview_hash_expire / 60} мин.`}
                            </Typography>
                          </div>
                        </Grid>
                      )}

                      <Grid item xs={12} className="m-margin_left-auto">
                        <Button
                          type="submit"
                          variant="contained"
                          color="primary"
                        >
                          {isNew ? 'Создать' : 'Сохранить'}
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                )}
              </Grid>
            </form>
          )}
        />
      </Fragment>
    );
  }
}

export default BookSettingsBasic;
