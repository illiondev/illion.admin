import { connect } from 'react-redux';

import { getBooksById } from 'lib/books/selectors';

import BookSettingsExtend from './BookSettingsExtend';

function mapStateToProps(state, ownProps) {
  return {
    entry: getBooksById(state)[ownProps.match.params.id],
  };
}

export default connect(mapStateToProps)(BookSettingsExtend);
