const NEW = 1;
const WAITING_FOR_PAYMENT = 2;
const PAYED = 3;
const CANCELED = 5;

export const PAYMENT_BILL_STATUS = {
  NEW, WAITING_FOR_PAYMENT, PAYED, CANCELED,
};

export const PAYMENT_BILL_STATUS_LIST = [
  {
    id: NEW,
    title: 'Новый',
  },
  {
    id: WAITING_FOR_PAYMENT,
    title: 'Ожидает оплаты',
  },
  {
    id: PAYED,
    title: 'Оплачен',
  },
  {
    id: CANCELED,
    title: 'Отменён',
  },
];

export const PAYMENT_BILL_STATUS_TITLES_BY_ID = PAYMENT_BILL_STATUS_LIST
  .reduce((byId, s) => ({ ...byId, [s.id]: s.title }), {});
