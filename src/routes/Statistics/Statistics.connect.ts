import { connect } from 'react-redux';

import { StatsBundleTotal, StatsBundle, StatsSummary } from 'types/stats';

import { RootState } from 'lib';

import { getStatistics } from 'lib/statistics/actions';
import { getTotalValues } from 'lib/statistics/selectors';

import Statistics from './Statistics';

export interface StatisticsStoreProps {
  total: StatsBundleTotal;
  entries: StatsBundle[];
  summary?: StatsSummary;
}

export interface StatisticsDispatchProps {
  getStatistics: typeof getStatistics;
}

function mapStateToProps(state: RootState): StatisticsStoreProps {
  return {
    total: getTotalValues(state),
    entries: state.statistics.entries,
    summary: state.statistics.summary,
  };
}

export default connect<StatisticsStoreProps, StatisticsDispatchProps, {}, RootState>(mapStateToProps, {
  getStatistics,
})(
  // @ts-ignore
  Statistics
);
