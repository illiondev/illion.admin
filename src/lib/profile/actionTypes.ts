enum ProfileActionTypes {
  PROFILE_ME_FETCH = 'PROFILE_ME_FETCH',
  PROFILE_ME = 'PROFILE_ME',
}

export default ProfileActionTypes;
