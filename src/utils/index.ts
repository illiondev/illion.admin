import { Author } from 'types';

export function formatPrice(amount: number, currency: string = 'RUB'): string {
  if (typeof Intl === 'undefined' || !Intl.NumberFormat) {
    return `${amount} ${currency}`;
  }

  const formatter = new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency,
    minimumFractionDigits: 0,
  });

  return formatter.format(amount);
}

export function filterQueryBuilder({
  page, sort_by, sort_dir, ...params
}: any, name: string = 'filter'): string {
  let query = Object.keys(params).map((key: string): string => `${name}[${key}]=${params[key]}`).join('&');

  if (page) {
    query += `&page=${page}`;
  }

  if (sort_by) {
    query += `&sort=${sort_dir === 'desc' ? '-' : ''}${sort_by}`;
  }

  return `?${query}`;
}

export function getCurrentDate(whitTime: boolean = false): string {
  const splitChar = whitTime ? '.' : 'T';

  return new Date().toISOString().split(splitChar)[0];
}

export function copyToClipboard(string: string): void {
  const el: HTMLInputElement = document.createElement('input');
  const selection: Selection|null = document.getSelection();
  let selected: Range|undefined;

  el.value = string;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.opacity = '0';

  document.body.appendChild(el);

  // если есть уже выделенный контент, сохраняем его для последующего восстановления
  if (selection) {
    selected = selection.rangeCount > 0 ? selection.getRangeAt(0) : undefined;
  }

  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);

  // восстанавливаем выделение
  if (selected && selection) {
    selection.removeAllRanges();
    selection.addRange(selected);
  }
}

export function getAuthorName(author: { data: Author }): string {
  if (!author || !author.data) return '';

  return `${author.data.name} ${author.data.surname}`;
}
