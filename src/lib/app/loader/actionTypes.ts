enum AppLoaderActionTypes {
  APP_LOADER_SHOW = 'APP_LOADER_SHOW',
  APP_LOADER_HIDE = 'APP_LOADER_HIDE',
}

export default AppLoaderActionTypes;
