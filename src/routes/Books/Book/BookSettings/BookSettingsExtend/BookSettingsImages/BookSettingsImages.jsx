import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

import { required } from 'utils/validators';

import FormFieldFile from 'components/FormFieldFile';

class BookSettingsImages extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      url: PropTypes.string,
    })).isRequired,
    bookId: PropTypes.number.isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    bookImageUpdate: PropTypes.func.isRequired,
    bookImageDelete: PropTypes.func.isRequired,
  }

  state = {
    modalIsOpen: false,
  }

  handleModalOpen = () => this.setState({ modalIsOpen: true })

  handleModalClose = () => this.setState({ modalIsOpen: false })

  handleDeleteEntry = entry => this.props.bookImageDelete({ id: entry.id, book_id: this.props.bookId })

  handleSubmitForm = (formData) => {
    const { bookId: book_id } = this.props;

    this.props.bookImageUpdate({
      category: 'photo',
      book_id,
      ...formData,
    });

    this.setState({ modalIsOpen: false });
  }

  render() {
    const { isReadOnly } = this.props;
    const { entry, modalIsOpen } = this.state;

    return (
      <div className="m-width-full">
        <Grid container spacing={24}>
          {this.props.entries.map(e => (
            <Grid item xs={6} md={3} key={e.id}>
              <Card className="m-relative">
                <Tooltip title="Просмотреть в новой вкладке" placement="top">
                  <IconButton
                    className="b-form-book-settings__photo-preview-action"
                    component="a"
                    href={e.url}
                    color="primary"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <VisibilityIcon />
                  </IconButton>
                </Tooltip>
                <CardMedia image={e.url} component="img" />
                {!isReadOnly && (
                  <CardActions>
                    <Button size="small" fullWidth color="secondary" onClick={() => this.handleDeleteEntry(e)}>
                      удалить
                    </Button>
                  </CardActions>
                )}
              </Card>
            </Grid>
          ))}
        </Grid>

        {!isReadOnly && (
          <Button onClick={this.handleModalOpen} fullWidth className="m-margin_top-normal">
            Добавить фото
          </Button>
        )}

        <Dialog onClose={this.handleModalClose} open={modalIsOpen} width="xs" fullWidth>
          <DialogTitle>Загрузка фото</DialogTitle>
          <Form
            onSubmit={this.handleSubmitForm}
            subscription={{ submitting: true, pristine: true }}
            initialValues={entry}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} className="m-width-full">
                <DialogContent>
                  <Grid container spacing={24}>
                    <Field
                      name="image"
                      label="Фотография"
                      component={FormFieldFile}
                      validate={required}
                      isImageFile
                    />
                  </Grid>
                </DialogContent>

                <DialogActions>
                  <Button onClick={this.handleModalClose} color="primary">
                    Отмена
                  </Button>
                  <Button type="submit" color="primary">
                    Сохранить
                  </Button>
                </DialogActions>
              </form>
            )}
          />
        </Dialog>
      </div>
    );
  }
}

export default BookSettingsImages;
