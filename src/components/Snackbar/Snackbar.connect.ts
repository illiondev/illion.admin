import { connect } from 'react-redux';

import { SNACKBAR_TYPES } from 'enums/SnakbarTypesEnums';

import { RootState } from 'lib';
import { snackbarHide } from 'lib/app/snackbar/actions';

import Snackbar from './Snackbar';

export interface SnackbarStoreProps {
  message: string;
  type: SNACKBAR_TYPES;
}

export interface SnackbarDispatchProps {
  snackbarHide: typeof snackbarHide;
}

function mapStateToProps(state: RootState): SnackbarStoreProps {
  return {
    message: state.app.snackbar.message,
    type: state.app.snackbar.type,
  };
}

export default connect<SnackbarStoreProps, SnackbarDispatchProps, {}, RootState>(mapStateToProps, {
  snackbarHide,
})(Snackbar);
