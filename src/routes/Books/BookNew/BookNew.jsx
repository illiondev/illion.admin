import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import BookSettingsBasic from 'routes/Books/Book/BookSettings/BookSettingsBasic';

class BookNew extends Component {
  static propTypes = {
    match: PropTypes.shape({}).isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  render() {
    return (
      <div className="b-page__content">
        <Paper className="b-paper">
          <Typography variant="h4" align="center">Новая книга</Typography>

          <BookSettingsBasic
            isNew
            match={this.props.match}
            isReadOnlyRoute={this.props.isReadOnlyRoute}
          />
        </Paper>
      </div>
    );
  }
}

export default BookNew;
