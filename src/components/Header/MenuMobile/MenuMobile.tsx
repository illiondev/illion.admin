import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';

import { MenuItem, MouseEventCallback } from 'types';

interface Props {
  items: MenuItem[];
}

interface State {
  anchorEl: EventTarget|null;
  submenu: string;
  isOpen: boolean;
}

class MenuMobile extends Component<Props, State> {
  public state: State = {
    anchorEl: null,
    submenu: '',
    isOpen: false,
  };

  private handleOpenSubmenu = (submenu: string): MouseEventCallback => (e): void => {
    e.stopPropagation();

    if (this.state.submenu === submenu) {
      this.handleCloseSubmenu();
    } else {
      this.setState({ anchorEl: e.currentTarget, submenu });
    }
  };

  private handleCloseSubmenu = (): void => this.setState({ anchorEl: null, submenu: '' });

  private handleOpen = (): void => this.setState({ isOpen: true });

  private handleClose = (): void => this.setState({ isOpen: false });

  private renderItem = (m: MenuItem): React.ReactNode => {
    const { anchorEl, submenu } = this.state;
    const isCurrent = Boolean(anchorEl) && submenu === m.title;

    return m.submenu ? (
      <Fragment key={m.title}>
        <ListItem
          button
          onClick={this.handleOpenSubmenu(m.title)}
          color="inherit"
          selected={isCurrent}
        >
          <ListItemText primary={m.title} />
          {isCurrent ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={isCurrent} timeout="auto" unmountOnExit>
          <List disablePadding>
            {m.submenu.map((sm): React.ReactNode => (
              <ListItem
                classes={{ root: 'b-header__drawer-submenu-item' }}
                button
                component={(props: ListItemProps): any => <Link to={sm.url} {...props} />}
                key={sm.url}
              >
                <ListItemText primary={sm.title} />
              </ListItem>
            ))}
          </List>
        </Collapse>
        <Divider />
      </Fragment>
    ) : (
      <ListItem
        button
        component={(props: ListItemProps): any => <Link to={m.url} {...props} />}
        key={m.url}
      >
        <ListItemText primary={m.title} />
      </ListItem>
    );
  };

  public render(): React.ReactNode {
    const { isOpen } = this.state;

    return (
      <Fragment>
        <IconButton onClick={this.handleOpen}>
          <MenuIcon fontSize="large" className="b-header__menu-icon" />
        </IconButton>
        <SwipeableDrawer
          open={isOpen}
          onClose={this.handleClose}
          onOpen={this.handleOpen}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.handleClose}
            onKeyDown={this.handleClose}
            className="b-header__menu-drawer"
          >
            <List>
              {this.props.items.map(this.renderItem)}
            </List>
          </div>
        </SwipeableDrawer>
      </Fragment>
    );
  }
}

export default MenuMobile;
