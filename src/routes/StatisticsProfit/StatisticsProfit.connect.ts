import { connect } from 'react-redux';

import { StatsProfit, StatsProfitTotal } from 'types/stats';

import { RootState } from 'lib';

import { statisticsProfit } from 'lib/statistics/actions';
import { getTotalProfitValues } from 'lib/statistics/selectors';

import StatisticsProfit from './StatisticsProfit';

export interface StatisticsProfitStoreProps {
  entries: StatsProfit[];
  total: StatsProfitTotal;
}

export interface StatisticsProfitDispatchProps {
  statisticsProfit: typeof statisticsProfit;
}

function mapStateToProps(state: RootState): StatisticsProfitStoreProps {
  return {
    entries: state.statistics.profit,
    total: getTotalProfitValues(state),
  };
}

export default connect<StatisticsProfitStoreProps, StatisticsProfitDispatchProps, {}, RootState>(mapStateToProps, {
  statisticsProfit,
})(
  // @ts-ignore
  StatisticsProfit
);
