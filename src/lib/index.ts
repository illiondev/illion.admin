import { combineReducers } from 'redux';

import app, { AppState } from './app';
import auth, { AuthState } from './auth/reducer';
import authors from './authors/reducer';
import books from './books/reducer';
import chapters from './chapters/reducer';
import chapterDraft from './chapterDraft/reducer';
import content from './content/reducer';
import profile, { ProfileState } from './profile/reducer';
import users from './users/reducer';
import statistics, { StatisticsState } from './statistics/reducer';
import orders from './orders/reducer';
import payouts, { PayoutsState } from './payouts/reducer';
import logs, { LogsState } from './logs/reducer';
import vacancies, { VacanciesState } from './vacancies/reducer';
import externalSales, { ExternalSalesState } from './externalSales/reducer';
import soon, { SoonState } from './soon/reducer';

export interface RootState {
  app: AppState;
  auth: AuthState;
  authors: any;
  books: any;
  chapters: any;
  chapterDraft: any;
  content: any;
  profile: ProfileState;
  users: any;
  statistics: StatisticsState;
  orders: any;
  payouts: PayoutsState;
  logs: LogsState;
  vacancies: VacanciesState;
  externalSales: ExternalSalesState;
  soon: SoonState;
}

export default combineReducers<RootState>({
  app,
  auth,
  authors,
  books,
  chapters,
  chapterDraft,
  content,
  profile,
  users,
  statistics,
  orders,
  payouts,
  logs,
  vacancies,
  externalSales,
  soon,
});
