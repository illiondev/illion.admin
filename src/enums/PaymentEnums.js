export const STATUS_NEW = 1;
export const STATUS_WAITING_FOR_PAYMENT = 2;
export const STATUS_PROCESSING = 3;
export const STATUS_COMPLETED = 4;
export const STATUS_CANCELED = 5;

export const PAYMENT_STATUS_LIST = [
  {
    id: STATUS_NEW,
    title: 'Новый',
  },
  {
    id: STATUS_WAITING_FOR_PAYMENT,
    title: 'Ожидает оплаты',
  },
  {
    id: STATUS_PROCESSING,
    title: 'Оплачен',
  },
  {
    id: STATUS_COMPLETED,
    title: 'Доставлен',
  },
  {
    id: STATUS_CANCELED,
    title: 'Отменён',
  },
];

export const PAYMENT_STATUS_TITLE_BY_ID = PAYMENT_STATUS_LIST.reduce((byId, s) => ({ ...byId, [s.id]: s.title }), {});
