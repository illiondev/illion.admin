import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TablePagination from '@material-ui/core/TablePagination';
import Avatar from '@material-ui/core/Avatar';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';

import router from 'services/RouterService';

class Authors extends Component {
  static propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      surname: PropTypes.string.isRequired,
    })).isRequired,
    pager: PropTypes.shape({}).isRequired,
    getAuthorList: PropTypes.func.isRequired,
    isReadOnlyRoute: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    this.props.getAuthorList();
  }

  handleChangePage = (e, page) => {
    this.props.getAuthorList({ page: page + 1 });
  }

  render() {
    const { entries, pager, isReadOnlyRoute } = this.props;

    return (
      <div className="b-page__content">
        {!isReadOnlyRoute && (
          <Tooltip title="Добавить автора" placement="top">
            <Fab
              color="primary"
              className="b-page__actions"
              component={Link}
              to={router.url.AUTHOR_NEW()}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}

        <Paper className="b-page__paper">
          <Typography variant="h5" className="b-page__paper-title">Авторы</Typography>
          {entries.length > 0 && (
            <List>
              {entries.map(author => (
                <ListItem
                  key={author.id}
                  role={undefined}
                  dense
                  button
                  component={Link}
                  to={router.url.AUTHOR(author.id)}
                >
                  {Boolean(author.photo) && (
                    <Avatar alt="author" src={author.photo.data.url} />
                  )}
                  <ListItemText primary={`${author.name} ${author.surname}`} />
                </ListItem>
              ))}
            </List>
          )}

          {pager.total_pages > 1 && (
            <TablePagination
              component="div"
              count={pager.total}
              rowsPerPage={pager.per_page}
              rowsPerPageOptions={[pager.per_page]}
              page={pager.current_page - 1}
              onChangePage={this.handleChangePage}
            />
          )}

        </Paper>
      </div>
    );
  }
}

export default Authors;
