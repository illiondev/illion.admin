import { connect } from 'react-redux';

import { bookImageUpdate } from 'lib/books/actions';

import BookSettingsOther from './BookSettingsOther';

export default connect(null, {
  bookImageUpdate,
})(BookSettingsOther);
