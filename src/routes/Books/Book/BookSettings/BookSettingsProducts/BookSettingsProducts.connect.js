import { connect } from 'react-redux';

import { getBooksById } from 'lib/books/selectors';

import BookSettingsProducts from './BookSettingsProducts';

function mapStateToProps(state, ownProps) {
  return {
    book: getBooksById(state)[ownProps.match.params.id],
  };
}

export default connect(mapStateToProps)(BookSettingsProducts);
