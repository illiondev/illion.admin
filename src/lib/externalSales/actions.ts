import api from 'services/ApiService';

import { ExternalSale, ExternalSalesFilterQuery, ListRes } from 'types';
import { Action, ActionThunk } from 'types/redux';

import { AuthLogout } from 'lib/auth/actions';

import types from './actionTypes';

type ExternalSalesReadSuccess = Action<types.EXTERNAL_SALES_READ, ListRes<ExternalSale>>;
type ExternalSaleCreateSuccess = Action<types.EXTERNAL_SALE_CREATE, ExternalSale>;
type ExternalSaleUpdateSuccess = Action<types.EXTERNAL_SALE_UPDATE, ExternalSale>;

export type ExternalSalesAllActions =
  ExternalSalesReadSuccess | ExternalSaleCreateSuccess | ExternalSaleUpdateSuccess | AuthLogout;

function externalSalesReadSuccess(payload: ListRes<ExternalSale>): ExternalSalesReadSuccess {
  return {
    type: types.EXTERNAL_SALES_READ,
    payload,
  };
}

export function externalSalesRead(query: ExternalSalesFilterQuery = {}): ActionThunk {
  return (dispatch): Promise<void> => api.externalSalesRead(query).then((res): void => {
    dispatch(externalSalesReadSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
    }));
  });
}

function externalSaleCreateSuccess(payload: ExternalSale): ExternalSaleCreateSuccess {
  return {
    type: types.EXTERNAL_SALE_CREATE,
    payload,
  };
}

export function externalSaleCreate(data: ExternalSale): ActionThunk {
  return (dispatch): Promise<void> => api.externalSaleCreate(data).then((res): void => {
    dispatch(externalSaleCreateSuccess(res.data.data));
  });
}

function externalSaleUpdateSuccess(payload: ExternalSale): ExternalSaleUpdateSuccess {
  return {
    type: types.EXTERNAL_SALE_UPDATE,
    payload,
  };
}

export function externalSaleUpdate(data: ExternalSale): ActionThunk {
  return (dispatch): Promise<void> => api.externalSaleUpdate(data).then((res): void => {
    dispatch(externalSaleUpdateSuccess(res.data.data));
  });
}
