import { connect } from 'react-redux';

import { bookBundleProductCreate, bookBundleProductDelete, productAutocomplete } from 'lib/books/actions';

import BundleIncludes from './BundleIncludes';

export default connect(null, {
  productAutocomplete,
  bookBundleProductCreate,
  bookBundleProductDelete,
})(BundleIncludes);
