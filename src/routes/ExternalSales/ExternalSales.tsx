import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import WaitIcon from '@material-ui/icons/AccessTime';
import DoneIcon from '@material-ui/icons/Done';

import { formatPrice } from 'utils';
import { BOOK_TYPES_TITLE_BY_ID } from 'enums/BookTypesEnum';

import AutoComplete from 'components/AutoComplete';

import {
  ProtectedRoute, ExternalSale, AutocompleteEntry, ExternalSalesFilterQuery
} from 'types';
import FormExternalSale from './FormExternalSale';
import { ExternalSalesDispatchProps, ExternalSalesStoreProps } from './ExternalSales.connect';

type Props = ExternalSalesStoreProps & ExternalSalesDispatchProps & ProtectedRoute;

interface State {
  modalIsOpen: boolean;
  entry?: ExternalSale;
  currentBundle?: AutocompleteEntry;
  entriesProp?: ExternalSale[];
}

class ExternalSales extends Component<Props, State> {
  public static getDerivedStateFromProps(nextProps: Props, prevState: State): State|null {
    if (nextProps.entries !== prevState.entriesProp) {
      return {
        modalIsOpen: false,
        entry: undefined,
        entriesProp: nextProps.entries,
      };
    }

    return null;
  }

  public state: State = {
    modalIsOpen: false,
    entry: undefined,
    currentBundle: undefined,
    entriesProp: undefined,
  };

  public componentDidMount(): void {
    this.props.externalSalesRead();
  }

  private handleChangePage = (e: any, page: number): void => {
    const { currentBundle } = this.state;
    const req: ExternalSalesFilterQuery = { page: page + 1 };

    if (currentBundle) {
      req.bundle_id = currentBundle.id as number;
    }

    this.props.externalSalesRead(req);
  };

  private handleModalClose = (): void => this.setState({ modalIsOpen: false, entry: undefined });

  private handleEditEntry = (entry: ExternalSale): void => this.setState({ entry, modalIsOpen: true });

  private handleBundleAutocomplete = (query: string): void => {
    this.props.bundleAutocomplete(query, this.state.modalIsOpen ? 'bundle' : 'bundle_filter');
  };

  private handleChooseBundle = (currentBundle: AutocompleteEntry): void => {
    this.setState({ currentBundle });
    this.props.externalSalesRead({ page: 1, bundle_id: currentBundle.id } as ExternalSalesFilterQuery);
  };

  private handleReset = (): void => {
    this.setState({ currentBundle: undefined });
    this.props.externalSalesRead({ page: 1 });
  };

  private handleCreate = (): void => {
    this.setState({
      entry: {} as ExternalSale,
      modalIsOpen: true
    });
  };

  private handleUpdateEntry = (data: ExternalSale): void => {
    if (data.id) {
      this.props.externalSaleUpdate(data);
    } else {
      this.props.externalSaleCreate(data);
    }
  };

  public render(): React.ReactNode {
    const { entries, pager, isReadOnlyRoute } = this.props;
    const { modalIsOpen, entry, currentBundle } = this.state;

    return (
      <div className="b-page__content">
        <Paper className="b-page__paper">
          <Typography variant="headline" className="b-page__paper-title">Внешние продажи</Typography>

          <Paper elevation={0} className="b-page__paper">
            <Grid container spacing={24}>
              <Grid item xs={12} sm={8}>
                <AutoComplete
                  onChoose={this.handleChooseBundle}
                  autocompleteHandler={this.handleBundleAutocomplete}
                  name="bundle_filter"
                  label="Название комплекта"
                  current={currentBundle}
                />
              </Grid>
              {Boolean(currentBundle) && (
                <Grid item>
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={this.handleReset}
                    className="m-margin_left-normal"
                  >
                    Сбросить
                  </Button>
                </Grid>
              )}
            </Grid>
          </Paper>

          <div className="b-table-responsive">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell padding="none" />
                  <TableCell>Товар</TableCell>
                  <TableCell>Количество</TableCell>
                  <TableCell>Цена (за шт.)</TableCell>
                  <TableCell>Добавил</TableCell>
                  <TableCell align="right">Дата продажи</TableCell>
                  <TableCell align="right">Комментарий</TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>

              <TableBody>
                {entries.map((e): React.ReactNode => (
                  <TableRow key={e.id} hover>
                    <Tooltip title={e.is_paid ? 'Оплачено' : 'Ожидает оплаты'} placement="top">
                      <TableCell padding="none">
                        <div className="m-margin_left-small">
                          {e.is_paid ? <DoneIcon color="primary" /> : <WaitIcon color="error" /> }
                        </div>
                      </TableCell>
                    </Tooltip>

                    <TableCell>{`${e.title} (${BOOK_TYPES_TITLE_BY_ID[e.type]})`}</TableCell>
                    <TableCell>{e.quantity}</TableCell>
                    <TableCell>{formatPrice(e.price)}</TableCell>
                    <TableCell>{e.moderator_email}</TableCell>
                    <TableCell align="right">{new Date(e.sold_at).toLocaleDateString()}</TableCell>

                    <TableCell align="right">{e.comment}</TableCell>
                    <TableCell>
                      {!isReadOnlyRoute && (
                        <Tooltip title="Редактировать">
                          <IconButton onClick={(): void => this.handleEditEntry(e)}>
                            <EditIcon />
                          </IconButton>
                        </Tooltip>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>

          {pager.total_pages > 1 && (
            <TablePagination
              component="div"
              count={pager.total}
              rowsPerPage={pager.per_page}
              rowsPerPageOptions={[pager.per_page]}
              page={pager.current_page - 1}
              onChangePage={this.handleChangePage}
            />
          )}
        </Paper>

        <FormExternalSale
          onClose={this.handleModalClose}
          onSave={this.handleUpdateEntry}
          onBundleAutocomplete={this.handleBundleAutocomplete}
          open={modalIsOpen}
          entry={entry}
        />

        {!isReadOnlyRoute && (
          <Tooltip title="Добавить продажу">
            <Fab
              color="primary"
              className="b-page__actions"
              onClick={this.handleCreate}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        )}
      </div>
    );
  }
}

export default ExternalSales;
