import {
  Api, ITokensStorage, ApiRequestConfig
} from 'illion-api';
import { AxiosError, AxiosResponse } from 'axios';

import { API_SERVER } from 'config';
import { SNACKBAR_TYPES } from 'enums/SnakbarTypesEnums';

import ApiResources from 'services/ApiResources';
import ApiTokensStorage from 'services/ApiTokensStorage';
import reduxStore from 'services/StoreService';
import sentry from 'services/SentryService';

import { loaderHide, loaderShow } from 'lib/app/loader/actions';
import { snackbarShow } from 'lib/app/snackbar/actions';
import { profileMe } from 'lib/profile/actions';

const store = reduxStore.get();

export class ApiService extends ApiResources {
  private tokenStorage: ITokensStorage;

  public constructor() {
    const tokenStorage = new ApiTokensStorage();
    const api = new Api(
      {
        baseURL: API_SERVER,
        timeout: 10 * 1000,
      },
      {
        onAccessTokenExpired: (): void => {
          tokenStorage.clearTokens();
        },
        onRequestStart: (): void => this.loaderShow(),
        onRequestFinish: (): void => this.loaderHide(),
        extractResponsePayloadData: (response: AxiosResponse<any>): any => response.data.data,
        authEndpoints: {
          login: 'auth/login',
          register: 'auth/register',
          refresh: 'auth/refresh',
          logout: 'auth/logout',
        }
      },
      tokenStorage
    );

    super(api);

    this.api = api;
    this.tokenStorage = tokenStorage;

    this.api.getAxiosInstance().interceptors.request.use(
      (config: ApiRequestConfig): ApiRequestConfig => {
        this.loaderShow();

        return config;
      },
      (error): Promise<any> => {
        this.loaderHide();
        this.handleError(error);

        return Promise.reject(error);
      }
    );

    this.api.getAxiosInstance().interceptors.response.use(
      (response: AxiosResponse): AxiosResponse => {
        this.loaderHide();

        return response;
      },
      (error: AxiosError): Promise<AxiosError> => {
        this.loaderHide();
        this.handleError(error);

        return Promise.reject(error);
      }
    );
  }

  private loaderShow(): void {
    store.dispatch(loaderShow());
  }

  private loaderHide(): void {
    store.dispatch(loaderHide());
  }

  public clearToken(): void {
    this.tokenStorage.clearTokens();
  }

  private handleError(e: AxiosError): void {
    const error = e.response && e.response.data ? e.response.data : e;
    const message = this.getErrorMessage(error);

    // обновляем права юзера, если была ошибка прав доступа
    if (e.response && e.response.status === 403 && message === 'User does not have the right permissions.') {
      // @ts-ignore
      store.dispatch(profileMe());
    }

    sentry.setFingerprints([message]);

    store.dispatch(snackbarShow({
      type: SNACKBAR_TYPES.ERROR,
      message,
    }));
  }

  private getErrorMessage(error: any): string {
    if (error.data && error.data.errors) {
      return Object.values(error.data.errors)
        .reduce((mem: string[], e: any): string[] => [...mem, ...e], [])
        .map((e: any): string => (
          typeof e === 'string' ? e : e.message || ''
        ))
        .join(' ');
    }

    if (error.message) return error.message;

    return 'Server error';
  }
}

const api = new ApiService();

export default api;
