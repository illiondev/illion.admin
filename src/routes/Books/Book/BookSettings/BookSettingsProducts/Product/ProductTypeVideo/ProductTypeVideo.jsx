import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';
import FormFieldEditor from 'components/FormFieldEditor';

class ProductTypeVideo extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Fragment>
        <Grid item xs={12}>
          <Field
            name="video"
            label="Ссылка на видео"
            validate={required}
            fullWidth
            component={FormFieldText}
            disabled={isReadOnly}
          />
        </Grid>
        <Grid item xs={12}>
          <Field
            name="description"
            label="Описание"
            fullWidth
            component={FormFieldEditor}
            disabled={isReadOnly}
            options={{ height: 200 }}
          />
        </Grid>
      </Fragment>
    );
  }
}

export default ProductTypeVideo;
