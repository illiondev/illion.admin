import { connect } from 'react-redux';

import { bookAdvantagesCreate, bookAdvantagesUpdate, bookAdvantagesDelete } from 'lib/books/actions';

import BookSettingsAdvantages from './BookSettingsAdvantages';

export default connect(null, {
  bookAdvantagesCreate,
  bookAdvantagesUpdate,
  bookAdvantagesDelete,
})(BookSettingsAdvantages);
