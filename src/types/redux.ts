import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { Action as ReduxAction } from 'redux';

import { RootState } from 'lib';

export interface Action<R = string, T = any> extends ReduxAction<R> {
  type: R;
  payload: T;
}

export type ActionThunk<R = void> = ThunkAction<Promise<R>, RootState, {}, Action>;

export interface ThunkDispatch extends ThunkDispatch<RootState, {}, Action> {
  <T extends Action>(action: T): T;
  <R>(asyncAction: ThunkAction<R, RootState, {}, Action>): R;
}

