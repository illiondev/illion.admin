import api from 'services/ApiService';

import * as types from './actionTypes';

function getAnnouncementSuccess(payload) {
  return {
    type: types.ANNOUNCEMENT_GET,
    payload,
  };
}

export function announcementRead() {
  return dispatch => api.announcementRead().then(res => dispatch(getAnnouncementSuccess(res.data.data)));
}

export function announcementCreate(data) {
  return dispatch => api.announcementCreate(data).then(res => dispatch(getAnnouncementSuccess(res.data.data)));
}

export function announcementUpdate(data) {
  return dispatch => api.announcementUpdate(data).then(res => dispatch(getAnnouncementSuccess(res.data.data)));
}
