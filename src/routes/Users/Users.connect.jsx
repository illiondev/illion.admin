import { connect } from 'react-redux';

import {
  getUsers, userModeratorDelete, userUpdate, userModeratorCreate, userCommentCreate
} from 'lib/users/actions';
import { getUsersGroupedPermissions } from 'lib/users/selectors';
import { getProfileId } from 'lib/profile/selectors';

import Users from './Users';

function mapStateToProps(state) {
  return {
    userId: getProfileId(state),
    entries: getUsersGroupedPermissions(state),
    pager: state.users.pager,
  };
}

export default connect(mapStateToProps, {
  getUsers,
  userUpdate,
  userModeratorCreate,
  userModeratorDelete,
  userCommentCreate,
})(Users);
