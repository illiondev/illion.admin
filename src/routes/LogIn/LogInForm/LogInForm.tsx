import React, { Component } from 'react';
import { Field, Form } from 'react-final-form';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';

import { LoginReq } from 'types';

import { composeValidators, emailFormat, required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';
import FormFieldCheckbox from 'components/FormFieldCheckbox';

interface Props {
  onSave(req: LoginReq): void;
}

class LogInForm extends Component<Props> {
  private handleSubmit = (values: object): void => {
    this.props.onSave(values as LoginReq);
  };

  public render(): React.ReactNode {
    return (
      <Form
        onSubmit={this.handleSubmit}
        subscription={{ submitting: true, pristine: true }}
        initialValues={{ remember_me: false }}
        render={({ handleSubmit }): React.ReactNode => (
          <form onSubmit={handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <Field
                label="Email"
                name="email"
                type="email"
                autoFocus
                component={FormFieldText}
                validate={composeValidators(required, emailFormat)}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <Field
                name="password"
                label="Пароль"
                type="password"
                component={FormFieldText}
                validate={required}
                fullWidth
              />
            </FormControl>

            <Field
              name="remember_me"
              label="Запомнить меня"
              type="checkbox"
              component={FormFieldCheckbox as any}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className="b-auth__form-submit"
            >
              Войти
            </Button>
          </form>
        )}
      />
    );
  }
}

export default LogInForm;
