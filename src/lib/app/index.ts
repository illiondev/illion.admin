import { combineReducers } from 'redux';

import autocomplete, { AppAutocompleteState } from './autocomplete/reducer';
import loader, { AppLoaderState } from './loader/reducer';
import snackbar, { AppSnackbarState } from './snackbar/reducer';


export interface AppState {
  autocomplete: AppAutocompleteState;
  loader: AppLoaderState;
  snackbar: AppSnackbarState;
}

const appReducer = combineReducers<AppState>({
  autocomplete,
  loader,
  snackbar,
});


export default appReducer;
