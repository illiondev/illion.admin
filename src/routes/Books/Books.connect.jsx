import { connect } from 'react-redux';

import { getBookList } from 'lib/books/actions';

import Books from './Books';

function mapStateToProps(state) {
  return {
    entries: state.books.entries,
    pager: state.books.pager,
  };
}

export default connect(mapStateToProps, {
  getBookList,
})(Books);
