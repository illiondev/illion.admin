import { connect } from 'react-redux';

import { RootState } from 'lib';
import { Pager } from 'types';
import { Log, LogClass, LogsFilterQuery } from 'types/logs';

import { getLogs, getLogClasses } from 'lib/logs/actions';

import Logs from './Logs';

export interface LogsStoreProps {
  readonly entries: Log[];
  readonly pager: Pager;
  readonly query: LogsFilterQuery;
  readonly logClasses: LogClass[];
}

export interface LogsDispatchProps {
  getLogs: typeof getLogs;
  getLogClasses: typeof getLogClasses;
}

function mapStateToProps(state: RootState): LogsStoreProps {
  return {
    entries: state.logs.entries,
    pager: state.logs.pager,
    query: state.logs.query,
    logClasses: state.logs.classes,
  };
}

export default connect<LogsStoreProps, LogsDispatchProps, {}, RootState>(mapStateToProps, {
  getLogs,
  getLogClasses,
})(
  // @ts-ignore
  Logs
);
