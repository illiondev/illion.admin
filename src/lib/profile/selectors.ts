import { createSelector } from 'reselect';

import { UserProfile, MenuItem } from 'types';
import { RootState } from 'lib';

import { MENU } from 'enums/MenuEnums';
import { PERMISSION_PATHS } from 'enums/PermissionsEnums';

interface EditPermissions { [key: string]: string }

export const getProfile = (state: RootState): UserProfile|undefined => state.profile.me;
export const getProfileIsFetching = (state: RootState): boolean => state.profile.isFetching;

export const getRoutesAccess = createSelector(getProfile, (profile): string[] => {
  if (!profile || !profile.permissions) return [];

  const permissions = profile.permissions.data.reduce((mem, p): EditPermissions => ({
    ...mem,
    [`${p.name.replace(/(read|edit) /, '')}`]: 1
  }), {});

  return Object.keys(permissions);
});

export const getRoutesEditAccess = createSelector(getProfile, (profile): string[] => {
  if (!profile || !profile.permissions) return [];

  const permissions: EditPermissions = profile.permissions.data
    .filter((p): boolean => p.name.includes('edit'))
    .reduce((mem, p): EditPermissions => ({
      ...mem,
      [`${p.name.replace(/edit /, '')}`]: 1
    }), {});

  return Object.keys(permissions);
});

export const getProfileIsLoaded = createSelector(
  getProfile,
  getProfileIsFetching,
  (profile, profileIsFetching): boolean => Boolean(profile && !profileIsFetching)
);

export const getProfileName = createSelector(
  getProfile,
  (profile): string|undefined => (profile ? profile.email : undefined)
);

export const getProfileId = createSelector(
  getProfile,
  (profile): number|undefined => (profile ? profile.id : undefined)
);

export const getUserMenuItems = createSelector(getRoutesAccess, (permissions): MenuItem[] => (
  MENU.reduce<MenuItem[]>((mem, m): MenuItem[] => {
    // обычный пункт меню и попадает под доступы
    if (m.url && permissions.includes(PERMISSION_PATHS[m.url])) {
      return [...mem, m];
    }

    // это обычный пункт который не попал под доступы
    // или это подменю, у которого ни один из пунктов не доступен
    if (!m.submenu || !m.submenu.some((sm): boolean => permissions.includes(PERMISSION_PATHS[sm.url]))) {
      return mem;
    }

    return [
      ...mem,
      {
        ...m,
        submenu: m.submenu.filter((sm): boolean => permissions.includes(PERMISSION_PATHS[sm.url])),
      }
    ];
  }, [])
));
