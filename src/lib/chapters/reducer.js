import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  entries: [],
  bookId: null,
};

const chapters = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CHAPTER_INIT:
      return {
        ...state,
        entries: payload.entries,
        bookId: payload.bookId,
      };

    case types.CHAPTER_CREATE:
      return {
        ...state,
        entries: [...state.entries, payload.entry],
      };

    case types.CHAPTER_UPDATE:
      return {
        ...state,
        entries: state.entries.map(entry => (entry.id !== payload.entry.id ? entry : payload.entry)),
      };

    case types.CHAPTER_DELETE:
      return {
        ...state,
        entries: state.entries.filter(entry => entry.id !== payload.entry.id),
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default chapters;
