enum LogsActionTypes {
  LOGS_LIST = 'LOGS_LIST',
  LOG_CLASSES = 'LOG_CLASSES',
}

export default LogsActionTypes;
