import { connect } from 'react-redux';

import { bookChapterCreate, bookChapterUpdate, bookChapterDelete } from 'lib/chapters/actions';
import { chapterDraftCreate, chapterDraftUpdate, chapterDraftDelete } from 'lib/chapterDraft/actions';
import { getBookChaptersById } from 'lib/chapters/selectors';

import BookChapter from './BookChapter';

function mapStateToProps(state, ownProps) {
  return {
    entry: getBookChaptersById(state)[ownProps.match.params.chapter],
    chapterDraft: Number(ownProps.match.params.chapter) === state.chapterDraft.id ? state.chapterDraft : undefined,
  };
}

export default connect(mapStateToProps, {
  bookChapterCreate,
  bookChapterUpdate,
  bookChapterDelete,
  chapterDraftCreate,
  chapterDraftUpdate,
  chapterDraftDelete,
})(BookChapter);
