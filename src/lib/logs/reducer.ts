import { Pager } from 'types';
import { Log, LogClass, LogsFilterQuery } from 'types/logs';

import authTypes from 'lib/auth/actionTypes';

import types from './actionTypes';
import { LogsAllActions } from './actions';

export interface LogsState {
  readonly entries: Log[];
  readonly classes: LogClass[];
  readonly pager: Pager;
  readonly query: LogsFilterQuery;
}

const initialState: LogsState = {
  entries: [],
  classes: [],
  pager: {} as Pager,
  query: {} as LogsFilterQuery,
};

const logs = (state = initialState, action: LogsAllActions): LogsState => {
  switch (action.type) {
    case types.LOGS_LIST:
      return {
        ...state,
        entries: action.payload.entries,
        pager: action.payload.pager,
        query: action.payload.query,
      };

    case types.LOG_CLASSES:
      return {
        ...state,
        classes: action.payload,
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default logs;
