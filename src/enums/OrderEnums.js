const SHIPPING_CATEGORY_COURIER = 'to-door';
const SHIPPING_CATEGORY_POST = 'post-office';
const SHIPPING_CATEGORY_PVZ = 'delivery-point';


export const SHIPPING_CATEGORIES_LIST = [
  { id: SHIPPING_CATEGORY_COURIER, title: 'Курьер' },
  { id: SHIPPING_CATEGORY_POST, title: 'Почта' },
  { id: SHIPPING_CATEGORY_PVZ, title: 'ПВЗ' },
];

export const SHIPPING_CATEGORIES_TITLE_BY_ID = SHIPPING_CATEGORIES_LIST.reduce((byId, s) => ({
  ...byId, [s.id]: s.title
}), {});

export const CUSTOMER_FORM_FIELDS = [
  'surname', 'name', 'patronimic', 'email', 'phone', 'postal_code', 'country', 'region', 'settlement', 'address',
  'address2', 'id', 'comment'
];

const SHIPTOR_STATUS_DONE = 1;
const SHIPTOR_STATUS_ERROR = 2;

export const SHIPTOR_STATUS_LIST = [
  { id: SHIPTOR_STATUS_DONE, title: 'Приняты' },
  { id: SHIPTOR_STATUS_ERROR, title: 'Ошибка' },
];
