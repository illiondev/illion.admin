import { createSelector } from 'reselect';

export const getUsers = state => state.users.entries;

export const getUsersGroupedPermissions = createSelector(getUsers, users => users.map(user => ({
  ...user,
  read: user.permissions.data.filter(p => p.name.includes('read')).map(p => p.name.replace(/read /, '')),
  edit: user.permissions.data.filter(p => p.name.includes('edit')).map(p => p.name.replace(/edit /, '')),
  permissions: user.permissions.data.map(p => p.name),
})));
