import api from 'services/ApiService';

import * as types from './actionTypes';

function getOrdersSuccess(payload) {
  return {
    type: types.ORDERS_LIST,
    payload,
  };
}

export function getOrders(query = {}) {
  return dispatch => api.orders(query).then((res) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { page, ...restParams } = query;

    dispatch(getOrdersSuccess({
      entries: res.data.data || [],
      pager: res.data.meta.pagination || {},
      query: restParams,
    }));
  });
}

function getOrderSuccess(payload) {
  return {
    type: types.ORDER_ITEM,
    payload,
  };
}

export function getOrder(id) {
  return dispatch => api.order(id).then(res => dispatch(getOrderSuccess(res.data.data)));
}

function orderUpdateSuccess(payload) {
  return {
    type: types.ORDER_UPDATED,
    payload,
  };
}

export function orderUpdate(data) {
  return dispatch => api.orderUpdate(data).then(res => dispatch(orderUpdateSuccess(res.data.data)));
}

export function orderPay(orderId) {
  return dispatch => api.orderPay(orderId).then(res => dispatch(orderUpdateSuccess(res.data.data)));
}

function orderBillCreateSuccess(payload) {
  return {
    type: types.ORDER_BILL_CREATE,
    payload,
  };
}

export function orderBillCreate(data) {
  return dispatch => api.orderBillCreate(data).then(res => dispatch(orderBillCreateSuccess(res.data.data)));
}


export function ordersSpreadsheetDrop() {
  return {
    type: types.ORDER_EXPORT_URL_DROP,
  };
}

function ordersSpreadsheetSuccess(payload) {
  return {
    type: types.ORDER_EXPORT_URL,
    payload,
  };
}

export function ordersSpreadsheet(data) {
  return dispatch => api.ordersSpreadsheet(data).then(res => dispatch(ordersSpreadsheetSuccess(res.data.data)));
}
