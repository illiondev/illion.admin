import { createSelector } from 'reselect';

import { RootState } from 'lib';
import {
  StatsBundle, StatsBundleTotal, StatsProfit, StatsProfitTotal
} from 'types/stats';

export const getEntries = (state: RootState): StatsBundle[] => state.statistics.entries;
export const getProfit = (state: RootState): StatsProfit[] => state.statistics.profit;

export const getTotalValues = createSelector(
  getEntries,
  (entries: StatsBundle[]): StatsBundleTotal => entries.reduce(
    (sum: StatsBundleTotal, entry: StatsBundle): StatsBundleTotal => ({
      ordered: sum.ordered + Number(entry.ordered),
      paid: sum.paid + Number(entry.paid),
      not_paid: sum.not_paid + Number(entry.not_paid),
      returned: sum.returned + Number(entry.returned),
      external_sales_sum: sum.external_sales_sum + Number(entry.external_sales_sum),
      bills_sales: sum.bills_sales + Number(entry.bills_sales),
      bills_sales_sum: sum.bills_sales_sum + Number(entry.bills_sales_sum),
      sum: sum.sum + Number(entry.sum),
      delivery_sum: sum.delivery_sum + Number(entry.delivery_sum),
    }), {
      ordered: 0,
      paid: 0,
      not_paid: 0,
      returned: 0,
      external_sales_sum: 0,
      bills_sales: 0,
      bills_sales_sum: 0,
      sum: 0,
      delivery_sum: 0,
    }
  )
);

export const getTotalProfitValues = createSelector(
  getProfit,
  (entries: StatsProfit[]): StatsProfitTotal => entries.reduce(
    (sum: StatsProfitTotal, entry: StatsProfit): StatsProfitTotal => ({
      authors_profit: sum.authors_profit + Number(entry.authors_profit),
      house_profit: sum.house_profit + Number(entry.house_profit),
    }), {
      authors_profit: 0,
      house_profit: 0,
    }
  )
);
