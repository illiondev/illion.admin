const PRICE_FIELD_NAMES = [
  'price_web_rub', 'price_web_eur', 'price_web_uah', 'price_mob_rub', 'price_mob_eur', 'price_mob_uah'
];

const PRICE_TYPES = {
  web: 1,
  mob: 2,
};

const PRICE_TYPES_BY_ID = {
  1: 'web',
  2: 'mob',
};

export function transformFormPricesToBundlePrices(formData) {
  return PRICE_FIELD_NAMES.map((name) => {
    if (typeof formData[name] !== 'undefined') {
      const [, type, currency] = name.split('_');
      return {
        type: PRICE_TYPES[type],
        currency,
        amount: Number(formData[name])
      };
    }

    return null;
  }).filter(p => p);
}

export function transformBundlePricesToFormPrices(prices) {
  const fields = {};

  if (prices) {
    prices.data.forEach((p) => {
      fields[`price_${PRICE_TYPES_BY_ID[p.type]}_${p.currency}`] = p.amount;
    });
  }

  return fields;
}

export function getBundleType(products) {
  const mainProduct = products.filter(p => !p.is_surprise)[0] || {};

  return mainProduct.type;
}
