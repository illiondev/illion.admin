import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcardTwoTone';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import ProductTypeIcon from 'components/ProductTypeIcon';

class BundleIncludesItem extends Component {
  static propTypes = {
    product: PropTypes.shape({
      id: PropTypes.number.isRequired,
      type: PropTypes.number.isRequired,
      price: PropTypes.number.isRequired,
      authors_percent: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      is_surprise: PropTypes.bool.isRequired,
    }).isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
  };

  render() {
    const {
      product: {
        type, title, is_surprise, price, authors_percent, id
      }, product, isReadOnly
    } = this.props;

    return (
      <TableRow>
        <TableCell padding="none"><ProductTypeIcon type={type} /></TableCell>
        <TableCell>{`${title} (#${id})`}</TableCell>
        <TableCell align="right">{price}</TableCell>
        <TableCell align="right">{authors_percent}</TableCell>
        <TableCell align="right">{is_surprise ? <CardGiftcardIcon color="secondary" /> : null}</TableCell>

        {!isReadOnly && (
          <TableCell align="right">
            <Tooltip title="Удалить" placement="top">
              <IconButton
                onClick={() => this.props.onDelete(product.id)}
                color="secondary"
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Настройки" placement="top">
              <IconButton
                className="m-margin_left-normal"
                onClick={() => this.props.onEdit(product)}
              >
                <EditIcon />
              </IconButton>
            </Tooltip>
          </TableCell>
        )}
      </TableRow>
    );
  }
}

export default BundleIncludesItem;
