import { connect } from 'react-redux';

import { RootState } from 'lib';
import { Pager, ExternalSale } from 'types';

import { bundleAutocomplete } from 'lib/books/actions';
import { externalSalesRead, externalSaleCreate, externalSaleUpdate } from 'lib/externalSales/actions';

import ExternalSales from './ExternalSales';

export interface ExternalSalesStoreProps {
  readonly entries: ExternalSale[];
  readonly pager: Pager;
}

export interface ExternalSalesDispatchProps {
  externalSalesRead: typeof externalSalesRead;
  externalSaleCreate: typeof externalSaleCreate;
  externalSaleUpdate: typeof externalSaleUpdate;
  bundleAutocomplete: typeof bundleAutocomplete;
}

function mapStateToProps(state: RootState): ExternalSalesStoreProps {
  return {
    entries: state.externalSales.entries,
    pager: state.externalSales.pager,
  };
}

export default connect<ExternalSalesStoreProps, ExternalSalesDispatchProps, {}, RootState>(mapStateToProps, {
  externalSalesRead,
  externalSaleCreate,
  externalSaleUpdate,
  bundleAutocomplete,
})(
  // @ts-ignore
  ExternalSales
);
