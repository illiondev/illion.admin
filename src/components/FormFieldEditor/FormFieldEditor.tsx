import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';

import ContentEditor from 'components/ContentEditor';

export interface FormFieldEditorProps extends FieldRenderProps<any> {
  label: string;
}

class FormFieldEditor extends Component<FormFieldEditorProps> {
  public render(): React.ReactNode {
    const {
      input,
      meta,
      label,
      ...rest
    } = this.props;

    return (
      <div className="m-relative">
        <Typography
          variant="caption"
          color={meta.error && meta.touched ? 'error' : 'textSecondary'}
          gutterBottom
        >
          {label}
        </Typography>
        <ContentEditor
          {...rest}
          content={input.value}
          onChange={(value: any): void => input.onChange(value)}
        />
        {Boolean(meta.error && meta.touched) && (
          <FormHelperText className="b-input__helper-text" error>{meta.error}</FormHelperText>
        )}
      </div>
    );
  }
}

export default FormFieldEditor;
