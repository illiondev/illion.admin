import { connect } from 'react-redux';

import { Vacancy } from 'types';

import { RootState } from 'lib';
import { vacanciesRead } from 'lib/vacancies/actions';

import Vacancies from './Vacancies';

export interface VacanciesStoreProps {
  entries: Vacancy[];
}

export interface VacanciesDispatchProps {
  vacanciesRead: typeof vacanciesRead;
}

function mapStateToProps(state: RootState): VacanciesStoreProps {
  return {
    entries: state.vacancies.entries,
  };
}

export default connect<VacanciesStoreProps, VacanciesDispatchProps, {}, RootState>(mapStateToProps, { vacanciesRead })(
  // @ts-ignore
  Vacancies
);
