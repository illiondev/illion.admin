import { connect } from 'react-redux';

import { AutocompleteEntry } from 'types';

import { RootState } from 'lib';

import { appAutocompleteDrop } from 'lib/app/autocomplete/actions';

import AutoComplete, { AutoCompleteProps } from './AutoComplete';

export interface AutoCompleteStoreProps {
  entries?: AutocompleteEntry[];
  isFetching: boolean;
}

export interface AutoCompleteDispatchProps {
  appAutocompleteDrop: typeof appAutocompleteDrop;
}

function mapStateToProps(state: RootState, ownProps: AutoCompleteProps): AutoCompleteStoreProps {
  const isActive = state.app.autocomplete.type === ownProps.name;

  return {
    entries: isActive ? state.app.autocomplete.entries : undefined,
    isFetching: isActive ? state.app.autocomplete.isFetching : false,
  };
}

export default connect<AutoCompleteStoreProps, AutoCompleteDispatchProps, AutoCompleteProps, RootState>(
  mapStateToProps, {
    appAutocompleteDrop
  }
)(
  AutoComplete
);
