import authTypes from 'lib/auth/actionTypes';

import * as types from './actionTypes';

const initialState = {
  entries: [],
  pager: {},
  entry: undefined,
  isFetchingEntry: false,
};

const authors = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.AUTHOR_LIST:
      return {
        ...state,
        entries: payload.entries,
        pager: payload.pager,
      };

    case types.AUTHOR_CREATE:
    case types.AUTHOR_UPDATE:
      return {
        ...state,
        entry: payload,
      };

    case types.AUTHOR_READ:
      return {
        ...state,
        entry: payload,
        isFetchingEntry: false,
      };

    case types.AUTHOR_ENTRY_FETCH:
      return {
        ...state,
        isFetchingEntry: payload,
      };

    case authTypes.LOG_OUT:
      return initialState;

    default:
      return state;
  }
};

export default authors;
