import { connect } from 'react-redux';

import { RootState } from 'lib';
import { authForgot } from 'lib/auth/actions';

import Restore from './Restore';

export interface RestoreDispatchProps {
  authForgot: typeof authForgot;
}


export default connect<{}, RestoreDispatchProps, {}, RootState>(null, { authForgot })(
  // @ts-ignore
  Restore
);
