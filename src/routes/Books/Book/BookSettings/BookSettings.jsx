import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Switch, Redirect } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import router from 'services/RouterService';

import RoutePrivate from 'components/RoutePrivate';

import BookSettingsBasic from './BookSettingsBasic';
import BookSettingsExtend from './BookSettingsExtend';
import BookSettingsProducts from './BookSettingsProducts';
import Product from './BookSettingsProducts/Product';
import BookSettingsBundles from './BookSettingsBundles';
import Bundle from './BookSettingsBundles/Bundle';

import './BookSettings.scss';

class BookSettings extends Component {
  static propTypes = {
    isNew: PropTypes.bool,
    entry: PropTypes.shape({
      title: PropTypes.string.isRequired,
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      author: PropTypes.shape({
        data: PropTypes.shape({
          id: PropTypes.number,
          name: PropTypes.string,
          surname: PropTypes.string,
        })
      }),
      cover: PropTypes.shape({
        data: PropTypes.shape({
          url: PropTypes.string,
        })
      }),
    }),
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      url: PropTypes.string.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    isNew: false,
    entry: {
      id: 'new',
      title: '',
      author: { data: {} },
      products: { data: [] },
      reviews: { data: [] },
      features: { data: [] },
      advantages: { data: [] },
      photos: { data: [] },
      quotes: { data: [] },
      preview: '',
      publication_date: '',
    },
  };

  render() {
    const {
      isNew, entry, match: { url }, location: { pathname }
    } = this.props;
    const currentTab = pathname.replace(`${url}/`, '').split('/')[0] || 'basic';

    return (
      <Paper className="b-paper">
        {!isNew && (
          <Typography variant="caption" align="center">{entry.title}</Typography>
        )}
        <Typography variant="h4" align="center">
          {isNew ? 'Новая книга' : 'Настройки книги'}
        </Typography>

        <Tabs
          value={currentTab}
          onChange={this.handleSwitchTab}
          className="m-margin_bottom-medium"
          variant="scrollable"
        >
          <Tab value="basic" label="Основное" component={Link} to={`${url}/basic`} />

          {!isNew && [
            <Tab
              value="extend"
              key="extend"
              label="Оформление страницы"
              component={Link}
              to={`${url}/extend`}
            />,
            <Tab value="products" key="products" label="Товары" component={Link} to={`${url}/products`} />,
            <Tab value="bundles" key="bundles" label="Комплекты" component={Link} to={`${url}/bundles`} />,
          ]}
        </Tabs>
        <Switch>
          <RoutePrivate path={router.path.BOOK_EDIT_BASIC} component={BookSettingsBasic} />
          <RoutePrivate path={router.path.BOOK_EDIT_EXTEND} component={BookSettingsExtend} />
          <RoutePrivate path={router.path.BOOK_EDIT_PRODUCTS} exact component={BookSettingsProducts} />
          <RoutePrivate path={router.path.BOOK_EDIT_BUNDLES} exact component={BookSettingsBundles} />
          <RoutePrivate path={router.path.BOOK_PRODUCT_NEW} exact component={Product} isNew />
          <RoutePrivate path={router.path.BOOK_EDIT_PRODUCT} component={Product} />
          <RoutePrivate path={router.path.BOOK_EDIT_BUNDLE} component={Bundle} />

          <Redirect to={router.path.BOOK_EDIT_BASIC} />
        </Switch>
      </Paper>
    );
  }
}

export default BookSettings;
