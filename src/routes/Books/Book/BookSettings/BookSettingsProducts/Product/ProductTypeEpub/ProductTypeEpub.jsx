import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import Grid from '@material-ui/core/Grid';

import { required } from 'utils/validators';

import FormFieldText from 'components/FormFieldText';
import FormFieldFile from 'components/FormFieldFile';

class ProductTypeEpub extends Component {
  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    onDelete: PropTypes.func.isRequired,
  };

  render() {
    const { isReadOnly } = this.props;

    return (
      <Fragment>
        <Grid item sm={12} md={2}>
          <Field
            name="article"
            label="Артикул книги"
            fullWidth
            component={FormFieldText}
            disabled={isReadOnly}
          />
        </Grid>
        <Grid item xs={12} sm={4} md={3}>
          <Field
            name="epub"
            accept=".epub"
            label="Файл книги в формате ePub*"
            component={FormFieldFile}
            validate={required}
            showPreview={false}
            onDeleteFile={() => this.props.onDelete('epub')}
            disabled={isReadOnly}
          />
        </Grid>
        <Grid item xs={12} sm={4} md={3}>
          <Field
            name="pdf"
            accept=".pdf"
            label="Файл книги в формате PDF"
            component={FormFieldFile}
            validate={required}
            showPreview={false}
            onDeleteFile={() => this.props.onDelete('pdf')}
            disabled={isReadOnly}
          />
        </Grid>
        <Grid item xs={12} sm={4} md={3}>
          <Field
            name="epub_preview"
            accept=".epub"
            label="Превью книги в формате ePub*"
            component={FormFieldFile}
            validate={required}
            showPreview={false}
            onDeleteFile={() => this.props.onDelete('preview')}
            disabled={isReadOnly}
          />
        </Grid>
      </Fragment>
    );
  }
}

export default ProductTypeEpub;
