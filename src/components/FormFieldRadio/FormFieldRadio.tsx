import React, { Component } from 'react';
import { FieldRenderProps } from 'react-final-form';

import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import './FormFieldRadio.scss';

export interface Props extends FieldRenderProps<any> {
  label: string;
}

class FormFieldRadio extends Component<Props> {
  public render(): React.ReactNode {
    const {
      input: {
        name, onChange, value, checked, ...restInput
      },
      label,
      meta,
      ...rest
    } = this.props;

    return (
      <FormControlLabel
        control={(
          <Radio
            {...rest}
            name={name}
            inputProps={restInput}
            onChange={onChange}
            checked={Boolean(checked)}
            value={String(value)}
          />
        )}
        label={label}
        classes={{
          label: meta.error && meta.touched ? 'b-input-radio__error' : ''
        }}
      />
    );
  }
}

export default FormFieldRadio;
