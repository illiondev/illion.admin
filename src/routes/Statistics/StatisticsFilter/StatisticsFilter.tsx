import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import { getCurrentDate } from 'utils';
import { StatsFilter } from 'types/stats';

import DatePicker from 'components/DatePicker';

interface Props {
  onSubmit(query?: StatsFilter): void;
}

type State = StatsFilter;

class StatisticsFilter extends Component<Props, State> {
  public state: State = {
    date_from: '',
    date_to: getCurrentDate(),
  };

  public componentDidMount(): void {
    window.addEventListener('keyup', this.handleKeyUp);
  }

  public componentWillUnmount(): void {
    window.removeEventListener('keyup', this.handleKeyUp);
  }

  private handleKeyUp = (e: KeyboardEvent): void => {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  };

  private handleChangeFilter = (e: React.ChangeEvent<HTMLInputElement>): void => {
    // @ts-ignore
    this.setState({ [e.target.name]: e.target.value });
  };

  private handleSubmit = (): void => {
    const { date_from, date_to } = this.state;

    if (date_from && date_to) {
      this.props.onSubmit({ date_from, date_to });
    }
  };

  private handleReset = (): void => {
    this.setState({
      date_from: '',
      date_to: getCurrentDate(),
    });

    this.props.onSubmit();
  };

  public render(): React.ReactNode {
    const { date_from, date_to } = this.state;

    return (
      <Grid container spacing={24} className="b-paper">
        <Grid item xs={12} sm={6} md={3}>
          <DatePicker
            name="date_from"
            value={date_from}
            onChange={this.handleChangeFilter}
            label="C"
            type="date"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              max: date_to || getCurrentDate(),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <DatePicker
            name="date_to"
            value={date_to}
            onChange={this.handleChangeFilter}
            label="По"
            type="date"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              min: date_from,
              max: getCurrentDate(),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
            disabled={!(date_from && date_to)}
          >
            Показать
          </Button>
          {(date_from && date_to) && (
            <Button
              variant="outlined"
              color="secondary"
              onClick={this.handleReset}
              className="m-margin_left-normal"
            >
              Сбросить
            </Button>
          )}
        </Grid>
      </Grid>
    );
  }
}

export default StatisticsFilter;
