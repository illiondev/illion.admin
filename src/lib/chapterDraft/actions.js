import * as types from './actionTypes';

export function chapterDraftCreate(payload) {
  return {
    type: types.CHAPTER_DRAFT_CREATE,
    payload,
  };
}

export function chapterDraftUpdate(payload) {
  return {
    type: types.CHAPTER_DRAFT_UPDATE,
    payload,
  };
}

export function chapterDraftDelete(payload) {
  return {
    type: types.CHAPTER_DRAFT_DELETE,
    payload,
  };
}
