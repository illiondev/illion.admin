import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import router from 'services/RouterService';

import LogInForm from './LogInForm';

import { LogInDispatchProps } from './LogIn.connect';

import './LogIn.scss';

class LogIn extends Component<LogInDispatchProps> {
  public render(): React.ReactNode {
    return (
      <div className="b-auth">
        <Paper className="b-auth__paper">
          <Avatar className="b-auth__avatar">
            <LockIcon />
          </Avatar>
          <Typography variant="h5">Вход</Typography>

          <LogInForm onSave={this.props.authLogin} />

          <Typography variant="body1" align="center" className="m-margin_top-normal">
            <Link to={router.url.RESTORE()}>Забыли пароль?</Link>
          </Typography>
        </Paper>
      </div>
    );
  }
}

export default LogIn;
