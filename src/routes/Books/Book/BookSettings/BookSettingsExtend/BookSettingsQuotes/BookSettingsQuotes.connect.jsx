import { connect } from 'react-redux';

import { bookQuotesCreate, bookQuotesUpdate, bookQuotesDelete } from 'lib/books/actions';

import BookSettingsQuotes from './BookSettingsQuotes';

export default connect(null, {
  bookQuotesCreate,
  bookQuotesUpdate,
  bookQuotesDelete,
})(BookSettingsQuotes);
