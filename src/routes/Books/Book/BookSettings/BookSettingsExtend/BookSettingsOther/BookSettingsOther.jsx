import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form } from 'react-final-form';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { required } from 'utils/validators';

import FormFieldFile from 'components/FormFieldFile';

class BookSettingsOther extends Component {
  static propTypes = {
    entry: PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    }).isRequired,
    isReadOnly: PropTypes.bool.isRequired,
    bookImageUpdate: PropTypes.func.isRequired,
  };

  form = null;

  handleUpdateFragment = ({ fragment }) => {
    const { id: book_id } = this.props.entry;

    this.props.bookImageUpdate({
      category: 'fragment',
      book_id,
      image: fragment,
    });
  };

  handleSave = () => {
    this.form.dispatchEvent(new Event('submit', { cancelable: true }));
  }

  render() {
    const { entry: { fragment }, isReadOnly } = this.props;

    return (
      <div className="m-width-full m-margin_top-medium">
        <Typography variant="h6" gutterBottom>Отрывок книги</Typography>
        <Form
          onSubmit={this.handleUpdateFragment}
          initialValues={{ fragment }}
          render={({ handleSubmit }) => (
            <form
              onSubmit={handleSubmit}
              ref={(el) => { this.form = el; }}
              className="m-width-full m-margin_bottom-medium"
            >
              <Grid container spacing={24}>
                <Grid item xs={12} md={4}>
                  <Field
                    name="fragment"
                    label="Отрывок книги в формате PDF"
                    accept=".pdf"
                    component={FormFieldFile}
                    validate={required}
                    showPreview={false}
                    disabled={isReadOnly}
                    onUpdateFile={this.handleSave}
                  />
                </Grid>
              </Grid>
            </form>
          )}
        />
      </div>
    );
  }
}

export default BookSettingsOther;
